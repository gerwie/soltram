
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `access_control`
--

CREATE TABLE `access_control` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `acl_name` varchar(20) NOT NULL,
  `can_view` tinyint(1) NOT NULL,
  `can_change` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
