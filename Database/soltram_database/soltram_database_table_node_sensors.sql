
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `node_sensors`
--

CREATE TABLE `node_sensors` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `sensor_id` int(2) NOT NULL,
  `name` varchar(20) NOT NULL,
  `type` int(3) NOT NULL,
  `mode` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
