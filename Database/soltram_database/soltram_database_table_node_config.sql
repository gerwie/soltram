
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `node_config`
--

CREATE TABLE `node_config` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL DEFAULT '1',
  `hor_offset` float NOT NULL DEFAULT '0',
  `ver_offset` float NOT NULL DEFAULT '0',
  `mqtt_topic` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_interval` int(8) NOT NULL DEFAULT '1000'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `node_config`
--

INSERT INTO `node_config` (`id`, `node_id`, `field_id`, `hor_offset`, `ver_offset`, `mqtt_topic`, `report_interval`) VALUES
(1, 1, 1, 0, 0, NULL, 1000),
(2, 3, 1, 0, 0, NULL, 1000),
(3, 4, 1, 0, 0, NULL, 1000),
(4, 5, 1, 0, 0, NULL, 1000);
