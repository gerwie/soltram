
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `central_log`
--

CREATE TABLE `central_log` (
  `id` int(11) NOT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `status_code` int(8) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
