
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gateway_message_log`
--

CREATE TABLE `gateway_message_log` (
  `id` int(11) NOT NULL,
  `node_id` int(8) NOT NULL,
  `message` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
