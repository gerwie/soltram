
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `energy_log`
--

CREATE TABLE `energy_log` (
  `id` int(11) NOT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `node_id` int(11) NOT NULL,
  `node_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `output` float NOT NULL,
  `acc_output` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
