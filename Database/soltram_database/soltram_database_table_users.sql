
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `pass_hash` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
