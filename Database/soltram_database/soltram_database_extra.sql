
--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `access_control`
--
ALTER TABLE `access_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `central_log`
--
ALTER TABLE `central_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `dummy_fields`
--
ALTER TABLE `dummy_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `dummy_nodes`
--
ALTER TABLE `dummy_nodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `dummy_node_config`
--
ALTER TABLE `dummy_node_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `energy_log`
--
ALTER TABLE `energy_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `gateway_message_log`
--
ALTER TABLE `gateway_message_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `nodes`
--
ALTER TABLE `nodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexen voor tabel `node_config`
--
ALTER TABLE `node_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `node_log`
--
ALTER TABLE `node_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `node_sensors`
--
ALTER TABLE `node_sensors`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `access_control`
--
ALTER TABLE `access_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `central_log`
--
ALTER TABLE `central_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `dummy_fields`
--
ALTER TABLE `dummy_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `dummy_nodes`
--
ALTER TABLE `dummy_nodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `dummy_node_config`
--
ALTER TABLE `dummy_node_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `energy_log`
--
ALTER TABLE `energy_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `gateway_message_log`
--
ALTER TABLE `gateway_message_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `nodes`
--
ALTER TABLE `nodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `node_config`
--
ALTER TABLE `node_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `node_log`
--
ALTER TABLE `node_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `node_sensors`
--
ALTER TABLE `node_sensors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;