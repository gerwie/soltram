
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `node_log`
--

CREATE TABLE `node_log` (
  `id` int(11) NOT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `node_id` int(11) NOT NULL,
  `status_code` int(8) NOT NULL DEFAULT '0',
  `hor_axis` float NOT NULL,
  `ver_axis` float NOT NULL,
  `node_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
