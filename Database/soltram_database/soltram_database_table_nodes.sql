
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `nodes`
--

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL,
  `node_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `online_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `nodes`
--

INSERT INTO `nodes` (`id`, `node_name`, `latitude`, `longitude`, `address`, `online_since`) VALUES
(4, 'Node 1', NULL, NULL, '5526146531270527', '2017-04-09 19:17:16'),
(5, 'Node 2', NULL, NULL, '5526146531270576', '2017-04-09 19:18:13');
