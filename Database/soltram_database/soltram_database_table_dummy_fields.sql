
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `dummy_fields`
--

CREATE TABLE `dummy_fields` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `dummy_fields`
--

INSERT INTO `dummy_fields` (`id`, `name`, `latitude`, `longitude`) VALUES
(1, '01', 51.9879, 5.95138),
(2, '02', 51.9879, 5.95138);
