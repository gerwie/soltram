/**
 * Energy log record listing
 *
 * Structure:
 * energyLogs
 * +---Object:Reports
 *     |---Object
 *     |   |---Number:Samples
 *     |   |---Number:Yield
 *     |   |---Boolean:WasUpdated
 *     +--- ...
 */
var energyLogs = {
    "Reports" : {}
};

/**
 * Avg filter for yields
 */
var YieldAvg = {
    "Size" : 25,
    "Yields" : [],
    "Mean" : 0,
    "Empty" : true,
    "Add" : function (y) {
        if (this.Yields.length >= this.Size) {
            if (this.Yields.length > 0) {
                this.Mean -= this.Yields[0] / this.Size;
            }
            this.Yields.shift();
        }
        this.Mean += y / this.Size;
        this.Yields.push(y);
    },
    "Set" : function (y) {
        for (var i=0;i<this.Size;i++) {
            this.Yields[i] = y;
        }
        this.Mean = y;
        this.Empty = false;
    }
};

/**
 * Sets up the dynamic page elements
 */
function pageSetup() {

}

/**
 * Updates the fields on the page
 */
function pageUpdate(yield) {
    updateChart(yield);
    document.getElementById("currentHarvest").innerHTML = Math.round(yield*1000)/1000;
    yieldToday += (yield/3600);
    document.getElementById("dailyHarvest").innerHTML = Math.round(yieldToday*1000)/1000;
}

/**
 * Page refresh loop
 */
function loop() {
    var totalYield = 0;
    for (var x in energyLogs.Reports) {
        if(energyLogs.Reports[x].WasUpdated) {
            totalYield += (energyLogs.Reports[x].Yield / energyLogs.Reports[x].Samples);
            //console.log(energyLogs.Reports[x].Yield + ", " + energyLogs.Reports[x].Samples);
            energyLogs.Reports[x].Yield = 0;
            energyLogs.Reports[x].Samples = 0;
            energyLogs.Reports[x].WasUpdated = false;
        }
    }
    if (YieldAvg.Empty) {
        YieldAvg.Set(totalYield);
    } else {
        YieldAvg.Add(totalYield);
    }

    pageUpdate(YieldAvg.Mean);
}

/**
 * Test update function
 */
function testUpdate() {
    var r = Math.floor(Math.random()*100);
    pageUpdate(r);
}

/**
 * MQTT
 */
var MQTT_SERVER_NAME = "mqtt.soltram.info";
var MQTT_SERVER_PORT = 8083;
var MQTT_CLIENT_NAME = "micromesh-web" + Math.floor(Math.random() * 1000);
var MQTT_ROOT_TOPIC = "SOLTRAM";
var mqttClient;

function mqttSetup(host     = MQTT_SERVER_NAME,
                   port     = MQTT_SERVER_PORT,
                   clientID = MQTT_CLIENT_NAME)
{
    // Debug
	console.log("Note from mqttSetup() : Setting up mqttClient...");

    // Create the client
	mqttClient = new Paho.MQTT.Client(host, Number(port), clientID);

	// Set callback handlers
	mqttClient.onConnectionLost = onConnectionLost;
	mqttClient.onMessageArrived = onMessageArrived;

	// Connect the client
    try {
        mqttClient.connect({
            onSuccess:onConnect,
            onFailure:onConnectFail,
            invocationContext:mqttClient
        });
    }
    catch (e) {
        console.log("Error in function mqttSetup() : Error when invoking Paho.MQTT.Client.connect() - " + e);
    }

    // Do some dirty additions to the client object
    mqttClient.currentSubscriptions = [];
    mqttClient.currentBaseTopic = "";
}

function mqttShutdown() {
    // Try to disconnent
    try {
        if (mqttClient.hasOwnProperty("disconnect")) {
            mqttClient.disconnect();
        } else {
            throw "MQTT client not initialised.";
        }
    }
    catch (e) {
        console.log("Note in function mqttShutdown() : Unable to disconnect - " + e);
    }

    delete mqttClient;
}

// called when the client connects
function onConnect(respObj) {
    // Object:respObj
    // +--- invocationContext

    console.log("MQTT Client connected.");
    mqttClient.subscribe(MQTT_ROOT_TOPIC+"/#");

}

function onConnectFail(respObj) {
    console.log("MQTT Client failed to connect.");
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("mqttClient Disconnected: " + responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
  // Debug
  //console.log("Note from function onMessageArrived() : Received message, Topic: " + message.destinationName+ ", Message: " +message.payloadString);
  //console.log(message);
    if(message.destinationName.indexOf("Current_Yield") > -1) {
        var n = message.destinationName.indexOf("Node");
        var nodeName;
        if(n > -1) {
            nodeName = message.destinationName.substr(n).split("/")[0];
            //console.log("Got a report from " + nodeName + ", " + Number(message.payloadString));

            if (energyLogs.Reports.hasOwnProperty(nodeName)) {
                energyLogs.Reports[nodeName].Samples++;
                energyLogs.Reports[nodeName].Yield += Number(message.payloadString);
                energyLogs.Reports[nodeName].WasUpdated = true;
            } else {
                energyLogs.Reports[nodeName] = {
                    "Samples" : 1,
                    "Yield" : Number(message.payloadString),
                    "WasUpdated" : true
                };
            }

        }
    }
}

function onSubscribe (respObj) {
    // Object:respObj
    // |--- invocationContext
    // +--- gratedQos
    console.log("Succesffully Subscribed.");
}

function onUnsubscribe (respObj) {
    // Object:respObj
    // |--- invocationContext
    // +--- errorCode

}

var yieldToday = 0;

$( document ).ready(function() {
	//document.documentElement.requestFullScreen();
    console.log( "ready!" );
	setupChart();
    mqttSetup();
	setInterval(loop, 1000);
});

var chartData = {
    labels: [],
    datasets: [{
                label:              "Yield",
                fillColor:          'rgba(00,51,204,0.6)',
                strokeColor:        "#0033cc",
                pointColor:         "#0033cc",
                pointStrokeColor:   "#FFF",
                data: []
    }],
	maxSize: 60,
    append: function(label, data)
    {
		if(this.count() >= this.maxSize)
		{
			this.labels.shift();
			this.datasets[0].data.shift();
		}
        this.labels.push(label);
        this.datasets[0].data.push(data);
    },
    clear: function()
    {
        this.labels.length = 0;
        this.datasets[0].data.length = 0;
    },
    count: function()
    {
        return this.datasets[0].data.length;
    }
}

var todayChart;

function setupChart()
{
	//document.getElementById("myChart").width = document.getElementById("chartDiv").width;
	//document.getElementById("myChart").height = document.getElementById("chartDiv").height;
	todayChart = new Chart(document.getElementById("myChart").getContext("2d")).Line(chartData, {animation: false, bezierCurveTension : (1/4)});
	//updateChart();
}

function updateChart(i)
{
	if(updateChart.counter == undefined){
		updateChart.counter = 0;
	}
	var curTime = new Date();
	var currentHarvest = document.getElementById("currentHarvest").innerHTML;
    var h = (curTime.getHours() < 10) ? "0" + curTime.getHours() : curTime.getHours();
    var m = (curTime.getMinutes() < 10) ? "0" + curTime.getMinutes() : curTime.getMinutes();
    var s = (curTime.getSeconds() < 10) ? "0" + curTime.getSeconds() : curTime.getSeconds();
	todayChart.addData([i], h + ":" + m + ":" + s );
	if(updateChart.counter++ > 12){
		todayChart.removeData();
	}
	todayChart.update();
}
