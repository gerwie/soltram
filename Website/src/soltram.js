/* SOLTRAM Specific */
// Global SOLTRAM Object
if (typeof SOLTRAM === "undefined") {
    SOLTRAM = {};
}

// SOLTRAM::NodeTree
if (typeof SOLTRAM.NodeTree === "undefined") {
    SOLTRAM.NodeTree = function () {};
}

// SOLTRAM::NodeField
if (typeof SOLTRAM.NodeField === "undefined") {
    SOLTRAM.NodeField = function (name, lat, lon) {
        if (false == String(name)) {
            console.log("Error in constructor of SOLTRAM::NodeField : Invalid name.");
            return -1;
        }
        if (false == Number(lat)) {
            console.log("Error in constructor of SOLTRAM::NodeField : Invalid Latitude.");
            return -1;
        }
        if (false == Number(lon)) {
            console.log("Error in constructor of SOLTRAM::NodeField : Invalid longitude.");
            return -1;
        }
        
        var data = {
            "AddNode" : function (node) {
                if (false == node instanceof SOLTRAM.Node) {
                    console.log("Error in SOLTRAM::NodeField::AddNode() : Node is not an instance of SOLTRAM::Node.");
                    return -1;
                }
                this.Nodes.push(node);
            },
            "GetNodeByIndex" : function (index) {
                if (0 == this.Nodes.length) {
                    console.log("Error in SOLTRAM::NodeField::GetNodeByIndex() : Field does not have any nodes.");
                    return -1;
                } else {
                    if (index > this.Nodes.length) {
                        console.log("Error in SOLTRAM::NodeField::GetNodeByIndex() : Index larger than node count.");
                        return -1;
                    } else {
                        return this.Nodes[index];
                    }
                }
            },
            "GetNodeByName" : function (name) {
                if (0 == this.Nodes.legnth) {
                    console.log("Error in SOLTRAM::NodeField::GetNodeByName() : Field does not have any nodes.");
                    return -1;
                }
                for (var i=0;i<this.Nodes.length;i++) {
                    if (name == this.Nodes[i].Name) {
                        return this.Nodes[i];
                    }
                }
                return -1;
            },
            "Name" : name,
            "Position" : {
                "Latitude" : lat,
                "longitude" : lon
            },
            "Nodes" : []
        };
        
        return data;
    }
}

// SOLTRAM::Node
if (typeof SOLTRAM.Node === "undefined") {
    SOLTRAM.Node = function (name, lat, lon, field) {
        if (false == String(name)) {
            console.log("Error in constructor of SOLTRAM::Node : Invalid name.");
            return -1;
        }
        if (false == Number(lat)) {
            console.log("Error in constructor of SOLTRAM::Node : Invalid Latitude.");
            return -1;
        }
        if (false == Number(lon)) {
            console.log("Error in constructor of SOLTRAM::Node : Invalid longitude.");
            return -1;
        }
        if (false == field instanceof SOLTRAM.NodeField) {
            console.log("Error in constructor of SOLTRAM::Node : field is not an instance of SOLTRAM::NodeField");
            return -1;
        }
        
        var data = {
            "GetParentField" : function () { return this.ParentField; },
            "Name" : name,
            "Position" : {
                "Latitude" : lat,
                "longitude" : lon
            },
            "Orientations" : {
                "Current" : {
                    "Horizontal" : 0.00,
                    "Vertical" : 0.00
                },
                "Calculated" : {
                    "Horizontal" : 0.00,
                    "Vertical" : 0.00
                },
            },
            "ParentField" : field
        };
        
        return data;
    };
}

// SOLTRAM::MQTT
if (typeof SOLTRAM.MQTT === "undefined") {
    // State    Description
    // -1       Not initialized
    // 0        Ready
    
    SOLTRAM.MQTT = function () {
        var data = {
            "Client" : undefined,
            "Host" : undefined,
            "Port" : undefined,
            "ClientID" : undefined,
            "RootTopic" : undefined,
            "State" : -1,
            "Setup" : function (host, port, ID) {
                this.Client = new Paho.MQTT.Client(host, Number(port), ID);
                this.Client.onConnectionLost = this.Handlers.ConnectionLost;
                this.Client.onMessageArrived = this.Handlers.MessageReceived;
                this.Client.onMessageDelivered = this.Handlers.MessageSent;
                console.log(
                    this.Logging.Prefix.Note + " from " + this.Logging.ObjectName +
                    " | MQTT Client setup complete."
                );
            },
            "Shutdown" : function () {
                this.Disconnect();
                delete this.Client;
            },
            "Connect" : function () {
                console.log(
                    this.Logging.Prefix.Note + " from " + this.Logging.ObjectName +
                    " | Connecting to " + this.Client.host + "..."
                );
                try {
                    this.Client.connect({
                        "invocationContext" : this,
                        "onSuccess" : this.Handlers.ConnectSuccess,
                        "onFailure" : this.Handlers.ConnectFailure
                    });
                }
                catch (e) {
                    console.log(
                        this.Logging.Prefix.Error + " in function " + this.Logging.ObjectName + "::Connect()" +
                        " | Could not establish connection: " + e
                    );
                }
            },
            "Disconnect" : function () {
                this.Client.disconnect();
            },
            "Handlers" : {
                "ConnectSuccess" : function (responseObject) {
                    var _this = responseObject.invocationContext;
                    console.log(
                        _this.Logging.Prefix.Note + " from " + _this.Logging.ObjectName +
                        " | Connected to: " + _this.Client.host + "."
                    );
                },
                "ConnectFailure" : function (responseObject) {
                    var _this = responceObject.invocationContext;
                    console.log(
                        _this.Logging.Prefix.Error + " in " + _this.Logging.ObjectName + 
                        " | Connection failed: (" + responseObject.errorCode + ") " + responseObject.errorMessage + "."
                    );
                },
                "ConnectionLost" : function (responseObject) {
                    console.log(
                        "[ERROR]" + " in " + "SOLTRAM::MQTT" + 
                        " | Connection lost: (" + responseObject.errorCode + ") " + responseObject.errorMessage + "."
                    );
                },
                "SubscribeSuccess" : function () {},
                "SubscribeFailure" : function () {},
                "UnsubscribeSuccess" : function () {},
                "UnsubscribeFailure" : function () {},
                "MessageReceived" : function () {},
                "MessageSent" : function () {}
            },
            "MessageParsers" : {
                
            },
            "Commands" : {
                "NodeSetDate" : function () {},
                "NodeSetTime" : function () {},
                "NodeSetGPS" : function () {},
                "NodeSetHarvestInterval" : function () {},
                "NodeSetTemperatureInterval" : function () {},
                "NodeSetOrientationInterval" : function () {},
                "NodeGetCurrentHarvest" : function () {},
                "NodeGetCurrentOrientation" : function () {},
                "NodeGetCalculatedOrientation" : function () {},
                "NodeGetSoltrackError" : function () {},
                "NodeGetGPS" : function () {},
                "NodeGetTemperature" : function () {},
                "NodeGetTime" : function () {},
                "NodeGetDate" : function () {}
            },
            "Logging" : {
                "Prefix" : {
                    "Note" : "[NOTE]",
                    "Warning" : "[WARNING]",
                    "Error" : "[ERROR]"
                },
                "ObjectName" : "SOLTRAM::MQTT"
            }
        };
        
        return data;
    };
}