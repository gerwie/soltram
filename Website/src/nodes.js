/* Globals */

/* Page script */

// AJAX Event handlers (Debug)
//$(document).ajaxComplete ( function (e, xhr, opts) {console.log("AJAX Completed. "+xhr.responseText);} ) ;
$(document).ajaxError ( function (e, xhr, opts, err) {console.log("AJAX Failed: "+err);} );

// Function fillPage
// Sets the page fields to the given values
function fillPage (fillerObj = {} ) {
    // fillerObj structure:
    // Object:root
    // |--- String:Name
    // |--- Object:Orientations
    // |    |--- Object:Current
    // |    |    |--- Number:Horizontal
    // |    |    +--- Number:Vertical
    // |    |--- Object:Calculated
    // |    |    |--- Number:Horizontal
    // |    |    +--- Number:Vertical
    // |    +--- Object:Error
    // |         |--- Number:Horizontal
    // |         +--- Number:Vertical
    // |--- Number:Temperature
    // |--- Object:Position
    // |    |--- Boolean:SetMapMarker
    // |    |--- Number:Latitude
    // |    +--- Number:longitude
    // |--- CurrentYield:Number
    // +--- ConfigData:Object
    //      |--- String:Date
    //      |--- String:Time
    //      |--- Number:Latitude
    //      |--- Number:longitude
    //      |--- Number:YieldUpdateInterval
    //      |--- Number:OrientationUpdateInterval
    //      +--- Number:TemperatureUpdateInterval

    // Check for Name
    if (fillerObj.hasOwnProperty("Name")) {
        document.getElementById("nodeName").innerHTML = fillerObj.Name;
    }

    // Check for Orientations
    if (fillerObj.hasOwnProperty("Orientations")) {
        // Object has Orientations
        // Check for Current Orientations
        if (fillerObj.Orientations.hasOwnProperty("Current")) {
            // There is a Current Orientation
            // Check for Current Horizontal Orientation
            if (fillerObj.Orientations.Current.hasOwnProperty("Horizontal")) {
                document.getElementById("curori-hor").innerHTML = "Horizontal: " + fillerObj.Orientations.Current.Horizontal + "&deg;";
            }

            if (fillerObj.Orientations.Current.hasOwnProperty("Vertical")) {
               document.getElementById("curori-ver").innerHTML = "Vertical: " + fillerObj.Orientations.Current.Vertical + "&deg;";
            }
        }

        // Check for Calculated Orientations
        if (fillerObj.Orientations.hasOwnProperty("Calculated")) {
            if (fillerObj.Orientations.Calculated.hasOwnProperty("Horizontal")) {
                document.getElementById("solori-hor").innerHTML = "Horizontal: " + fillerObj.Orientations.Calculated.Horizontal + "&deg;";
            }

            if (fillerObj.Orientations.Calculated.hasOwnProperty("Vertical")) {
                document.getElementById("solori-ver").innerHTML = "Vertical: " + fillerObj.Orientations.Calculated.Vertical + "&deg;";
            }
        }

        // Check for Orientation Errors
        if (fillerObj.Orientations.hasOwnProperty("Error")) {
            if (fillerObj.Orientations.Error.hasOwnProperty("Horizontal")) {
                document.getElementById("solerr-hor").innerHTML = "Horizontal: " + Math.round(fillerObj.Orientations.Error.Horizontal*1000)/1000 + "&deg;";
            }

            if (fillerObj.Orientations.Error.hasOwnProperty("Vertical")) {
                document.getElementById("solerr-ver").innerHTML = "Vertical: " + Math.round(fillerObj.Orientations.Error.Vertical*1000)/1000 + "&deg;";
            }
        }
    }

    // Check for temperature
    if (fillerObj.hasOwnProperty("Temperature")) {
        // There is a temperature
        document.getElementById("curtmp").innerHTML = fillerObj.Temperature + "&deg; Celcius";
    }

    // Check for position (GPS)
    if (fillerObj.hasOwnProperty("Position")) {
        // There are positions
        if (fillerObj.Position.hasOwnProperty("Latitude") &&
            fillerObj.Position.hasOwnProperty("longitude")) {
            // There is a Latitude
            document.getElementById("gpslat").innerHTML = fillerObj.Position.Latitude + "&deg;";
            document.getElementById("gpslong").innerHTML = fillerObj.Position.longitude + "&deg;";
            if (fillerObj.Position.hasOwnProperty("SetMapMarker")) {
                if (fillerObj.Position.SetMapMarker) {
                    setMarkerToMap(fillerObj.Position.Latitude, fillerObj.Position.longitude);
                }
            }

        }
    }

    // Check for Yield
    if (fillerObj.hasOwnProperty("CurrentYield")) {
        document.getElementById("currentHarvest").innerHTML = fillerObj.CurrentYield;
    }

    // Check for config settings
    if (fillerObj.hasOwnProperty("ConfigData")) {
        if (fillerObj.ConfigData.hasOwnProperty("Date")) {
            document.getElementById("date-set-value").value = fillerObj.ConfigData.Date;
        }

        if (fillerObj.ConfigData.hasOwnProperty("Time")) {
            document.getElementById("time-set-value").value = fillerObj.ConfigData.Time;
        }

        if (fillerObj.ConfigData.hasOwnProperty("Latitude")) {
            document.getElementById("lat-set-value").value = fillerObj.ConfigData.Latitude;
        }

        if (fillerObj.ConfigData.hasOwnProperty("longitude")) {
            document.getElementById("lon-set-value").value = fillerObj.ConfigData.longitude;
        }

        if (fillerObj.ConfigData.hasOwnProperty("YieldUpdateInterval")) {
            document.getElementById("yield-int-set-value").value = fillerObj.ConfigData.YieldUpdateInterval;
        }

        if (fillerObj.ConfigData.hasOwnProperty("OrientationUpdateInterval")) {
            document.getElementById("ori-int-set-value").value = fillerObj.ConfigData.OrientationUpdateInterval;
        }

        if (fillerObj.ConfigData.hasOwnProperty("TemperatureUpdateInterval")) {
            document.getElementById("temp-int-set-value").value = fillerObj.ConfigData.TemperatureUpdateInterval;
        }
    }
}

// Clear the page fields
function clearPage () {
    fillPage(
    {
        "Orientations" : {
            "Current" : {
                "Horizontal" : 0.00,
                "Vertical" : 0.00
            },
            "Calculated" : {
                "Horizontal" : 0.00,
                "Vertical" : 0.00
            },
            "Error" : {
                "Horizontal" : 0.00,
                "Vertical" : 0.00
            }
        },
        "Temperature" : 0.00,
        "Position" : {
            "SetMapMarker" : false,
            "Latitude" : 0.00,
            "longitude" : 0.00
        },
        "ConfigData" : {
            "Date" : "000000",
            "Time" : "000000",
            "Latitude" : 0.00,
            "longitude" : 0.00,
            "YieldUpdateInterval" : 1000,
            "OrientationUpdateInterval" : 1000,
            "TemperatureUpdateInterval" : 1000
        }
    }
    );
}

function fillConfigForm(nodeObj) {
    var curDate = new Date();
    var dString = ((curDate.getUTCDate() < 10) ? "0" + curDate.getUTCDate() : curDate.getUTCDate()) + "-" + (((curDate.getUTCMonth() + 1) < 10) ? "0" + (curDate.getUTCMonth() + 1) : (curDate.getUTCMonth() + 1)) + "-" + ((Number(String(curDate.getUTCFullYear()).substr(-2)) < 10) ? "0" + String(curDate.getUTCFullYear()).substr(-2) : String(curDate.getUTCFullYear()).substr(-2));
    var tString = ((curDate.getUTCHours() < 10) ? "0" + curDate.getUTCHours() : curDate.getUTCHours()) + ":" + ((curDate.getUTCMinutes() < 10) ? "0" + curDate.getUTCMinutes() : curDate.getUTCMinutes()) + ":" + ((curDate.getUTCSeconds() < 10) ? "0" + curDate.getUTCSeconds() : curDate.getUTCSeconds());
    var data = {
        "ConfigData" : {
            "Date" : dString,
            "Time" : tString,
            "Latitude" : nodeObj.StaticData.latitude,
            "longitude" : nodeObj.StaticData.longitude,
            "YieldUpdateInterval" : 1000,
            "OrientationUpdateInterval" : 1000,
            "TemperatureUpdateInterval" : 1000
        }
    };
    fillPage(data);
}

function updateTimeDateConfig() {
    var curDate = new Date();
    var dString = ((curDate.getUTCDate() < 10) ? "0" + curDate.getUTCDate() : curDate.getUTCDate()) + "-" + (((curDate.getUTCMonth() + 1) < 10) ? "0" + (curDate.getUTCMonth() + 1) : (curDate.getUTCMonth() + 1)) + "-" + ((Number(String(curDate.getUTCFullYear()).substr(-2)) < 10) ? "0" + String(curDate.getUTCFullYear()).substr(-2) : String(curDate.getUTCFullYear()).substr(-2));
    var tString = ((curDate.getUTCHours() < 10) ? "0" + curDate.getUTCHours() : curDate.getUTCHours()) + ":" + ((curDate.getUTCMinutes() < 10) ? "0" + curDate.getUTCMinutes() : curDate.getUTCMinutes()) + ":" + ((curDate.getUTCSeconds() < 10) ? "0" + curDate.getUTCSeconds() : curDate.getUTCSeconds());
    var data = {
        "ConfigData" : {
            "Date" : dString,
            "Time" : tString
        }
    };
    fillPage(data);
}

function setNodeConfig(nodeObj, mqttClientObj = mqttClient) {
    if (nodeObj == undefined) {
        console.log("Error in function setNodeConfig() : nodeObj is undefined.");
        return -1;
    }

    if (false == (mqttClientObj instanceof Paho.MQTT.Client)) {
        console.log("Error in function setNodeConfig() : mqttClientObj is not an instance of Paho.MQTT.Client.");
        return -2;
    }

    var setDate = document.getElementById("date-set-check").checked;
    var setTime = document.getElementById("time-set-check").checked;
    var setLon = document.getElementById("lon-set-check").checked;
    var setLat = document.getElementById("lat-set-check").checked;
    var setYieldInt = document.getElementById("yield-int-set-check").checked;
    var setOriInt = document.getElementById("ori-int-set-check").checked;
    var setTempInt = document.getElementById("temp-int-set-check").checked;

    var dateValue = document.getElementById("date-set-value").value;
    var timeValue = document.getElementById("time-set-value").value;
    var lonValue = document.getElementById("lon-set-value").value;
    var latValue = document.getElementById("lat-set-value").value;
    var yieldIntValue = document.getElementById("yield-int-set-value").value;
    var oriIntValue = document.getElementById("ori-int-set-value").value;
    var tempIntValue = document.getElementById("temp-int-set-value").value;

    var topic = "SOLTRAM/" + nodeObj.StaticData.field + "/Command/"+nodeObj.StaticData.text;

    if (setDate) {
        //console.log("setNodeConfig() : Sending date is disabled!");
        var d = parseDate(dateValue);
        if (d !== false) {
            var msg = new Paho.MQTT.Message ("set cudate "+d+"\0");
            msg.destinationName = topic;
            mqttClientObj.send(msg);
            console.log("setNodeConfig() : Sending " + msg.destinationName + " -> " + msg.payloadString + ".");
        } else {
            console.log("setNodeConfig() : Invalid date!");
        }
    }

    if (setTime) {
        //console.log("setNodeConfig() : Sending time is disabled!");
        var t = parseTime(timeValue);
        if (t !== false) {
            var msg = new Paho.MQTT.Message ("set cutime "+t+"\0");
            msg.destinationName = topic;
            mqttClientObj.send(msg);
            console.log("setNodeConfig() : Sending " + msg.destinationName + " -> " + msg.payloadString + ".");
        } else {
            console.log("setNodeConfig() : Invalid time!");
        }
    }

    if (setLon || setLat) {
        var msg = new Paho.MQTT.Message ("set curgps "+latValue+" "+lonValue+"\0");
        msg.destinationName = topic;
        mqttClientObj.send(msg);
        console.log("setNodeConfig() : Sending " + msg.destinationName + " -> " + msg.payloadString + ".");
    }

    if (setYieldInt) {
        var msg = new Paho.MQTT.Message ("set curhar "+yieldIntValue+"\0");
        msg.destinationName = topic;
        mqttClientObj.send(msg);
        console.log("setNodeConfig() : Sending " + msg.destinationName + " -> " + msg.payloadString + ".");
    }

    if (setOriInt) {
        var msg = new Paho.MQTT.Message ("set curori "+oriIntValue+"\0");
        msg.destinationName = topic;
        mqttClientObj.send(msg);
        console.log("setNodeConfig() : Sending " + msg.destinationName + " -> " + msg.payloadString + ".");
    }

    if (setTempInt) {
        var msg = new Paho.MQTT.Message ("set curtmp "+tempIntValue+"\0");
        msg.destinationName = topic;
        mqttClientObj.send(msg);
        console.log("setNodeConfig() : Sending " + msg.destinationName + " -> " + msg.payloadString + ".");
    }
    //console.log("Warning in function setNodeConfig() : Publishing new config is disabled (outcommented).");

    //console.log("Warning in function setNodeConfig() : Function invoked but is not complete.");
    return 0;
}

var allowedDateChars = "0123456789-";
function checkDateInput (e) {
    if (e.defaultPrevented) {return;}
    if (allowedDateChars.indexOf(e.key) <= -1 && e.key.length == 1) {
        e.preventDefault();
    }
}

function parseDate (inputstr) {
    // If length = 6, expect the following;
    // The string is formatted as DDMMYY
    // Else if length = 8;
    // The string is formatted as DD:MM:YY
    // Else if length = 10;
    // The string is formatted as DD:MM:YYYY
    // Else this is not a valid date string

    // Check if the string contains bad characters, and if it's between 6 and 10 chars long
    var l = inputstr.length;
    if (l >= 6 && l <= 10) {
        for (var i = 0;i < l;i++) {
            if (allowedDateChars.indexOf(inputstr[i]) == -1) {
                return false;
            }
        }
    } else {
        return false;
    }

    switch (inputstr.length) {
        case 6:
            return inputstr;
        break;
        case 8:
            if (inputstr[2] == '-' && inputstr[5] == '-') {
                var split = inputstr.split("-");
                return (split[0]+split[1]+split[2]);
            } else {
                return false;
            }
        break;
        case 10:
            if (inputstr[2] == '-' && inputstr[5] == '-') {
                var split = inputstr.split("-");
                return (split[0]+split[1]+split[2].substr(0,2));
            } else {
                return false;
            }
        break;
        default:
            return false;
    }

    return false;
}

var allowedTimeChars = "0123456789:";
function checkTimeInput (e) {
    if (e.defaultPrevented) {return;}
    if (allowedTimeChars.indexOf(e.key) <= -1 && e.key.length == 1) {
        e.preventDefault();
    }
}

function parseTime (inputstr) {
    // If length = 6, expect the following;
    // The string is formatted as DDMMYY
    // Else if length = 8;
    // The string is formatted as DD:MM:YY
    // Else if length = 10;
    // The string is formatted as DD:MM:YYYY
    // Else this is not a valid date string

    // Check if the string contains bad characters, and if it's between 6 and 10 chars long
    var l = inputstr.length;
    if (l >= 6 && l <= 8) {
        for (var i = 0;i < l;i++) {
            if (allowedTimeChars.indexOf(inputstr[i]) == -1) {
                return false;
            }
        }
    } else {
        return false;
    }

    switch (inputstr.length) {
        case 6:
            return inputstr;
        break;
        case 8:
            if (inputstr[2] == ':' && inputstr[5] == ':') {
                var split = inputstr.split(":");
                return (split[0]+split[1]+split[2]);
            } else {
                return false;
            }
        break;
        default:
            return false;
    }

    return false;
}

// Execute this script when the page is done loading
var configMenuUpdateInterval = undefined;
var chartUpdateInterval = undefined;

$( document ).ready( function() {
    mqttSetup();

	tree = createTree();
    getTree("placeholder",tree ,true);

	// Maps
	google.maps.event.addDomListener(window, 'load', initialize);
	google.maps.event.addDomListener(window, 'resize', centerMap);

	// yieldChart
	setupChart();
	//setInterval(updateChart, 1000);
    configMenuUpdateInterval = setInterval(updateTimeDateConfig,500);
});

// Execute this script when the page is left
$( window ).unload( function () {
    if (typeof configMenuUpdateInterval === "number") {
        clearInterval(configMenuUpdateInterval);
        configMenuUpdateInterval = undefined;
    }
    mqttShutdown();
});

/* Tree */
var tree;
var currentNodeView = {
    "StaticData" : {},
    "DynamicData" : {
        "Latitude" : 0.00,
        "longitude" : 0.00,
        "CurrentOrientation" : {
            "Horizontal" : 0.00,
            "Vertical" : 0.00
        },
        "CalculatedOrientation" : {
            "Horizontal" : 0.00,
            "Vertical" : 0.00
        },
        "OrientationError" : {
            "Horizontal" : 0.00,
            "Vertical" : 0.00
        },
        "Temperature" : 0.00
    },
    "Type" : undefined,
    "FieldData" : {
        // This contains the collection of node reports
        // in the following format:
        // "Node xxxxxx" : {
        //      Samples : Number,
        //      Yield : Number,
        //      WasUpdated : Boolean
        //  }
    }
};

function renderTree (treeObj,elementID = "#tree") {
    $(elementID).treeview({
        data: treeObj,
        onNodeSelected: function(event, node) {
            nodeSelected(event, node);
            //document.getElementById("nodeName").innerHTML = node.text;
            //setMarkerToMap(node.latitude, node.longitude);
        }
    });
}

function getTree(sourceURI, treeObj, USE_DATABASE = false) {
  // Some logic to retrieve, or generate tree structure
  // Fetch fields and nodes from Database through AJAX
  // (dummy file)
  if (false == String(sourceURI)) {
      console.log("ERROR in function getTree(): Source URI could not be coerced into a string.");
      return -1;
  }
  if (USE_DATABASE) {
      $.getJSON ("request.php?q=dummyNodeTree", function (data, status, xhr) {
          var log = ""; // Log string
          var fc = 0; // Field count
          var nc = 0; // Node count

          // Walk through each field
          for (var Field in data.Results.Fields) {
              var currentField = data.Results.Fields[Field];
              var fld = createField(currentField.Name, currentField.Lon, currentField.Lat);

              // Walk through each node in this field
              for (var Node in currentField.Nodes) {
                  var currentNode = currentField.Nodes[Node];
                  ++nc;
                  addNode (fld, currentNode.Name, currentNode.Lon, currentNode.Lat);
              }

              ++fc;
              treeObj.push(fld);
          }
          log = "Found "+nc+" nodes in "+fc+" fields.";
          console.log(log);
          renderTree(treeObj);
      });
  } else {
      $.getJSON ("dummytree.json", function (data, status, xhr) {
          var log = ""; // Log string
          var fc = 0; // Field count
          var nc = 0; // Node count

          // Walk through each field
          for (var Field in data.Fields) {
              var currentField = data.Fields[Field];
              var fld = createField(currentField.Name, currentField.Lon, currentField.Lat);

              // Walk through each node in this field
              for (var Node in currentField.Nodes) {
                  var currentNode = currentField.Nodes[Node];
                  ++nc;
                  addNode (fld, currentNode.Name, currentNode.Lon, currentNode.Lat);
              }

              ++fc;
              treeObj.push(fld);
          }
          log = "Found "+nc+" nodes in "+fc+" fields.";
          console.log(log);
          renderTree(treeObj);
      });
  }
  return 0;
}

function createTree() {
	var data = [];
	return data;
}

function createField(id, latitude, longitude) {
	var data = {
		text: "Field" + " " + id,
		id: id,
		latitude: latitude,
		longitude: longitude,
		nodes: []
	};

	return data;
}

function addNode(datacontainer, id, latitude, longitude) {
	datacontainer.nodes.push({
		text: "Node" + " " + id,
        field: datacontainer.text,
		id: id,
		latitude: latitude,
		longitude: longitude
		});
}

function nodeSelected(event, node) {
    if (typeof chartUpdateInterval === "number") {
        window.clearInterval(chartUpdateInterval);
        chartUpdateInterval = undefined;
    }
    currentNodeView.FieldData = {};
    todayChart.clear();
    clearPage();
    fillPage({
        "Name" : node.text,
        "Position" : {
            "SetMapMarker" : true,
            "Latitude" : node.latitude,
            "longitude" : node.longitude
        }
    });
    fillConfigForm(currentNodeView);

    currentNodeView.StaticData = node;
    currentNodeView.Type = (node.text.indexOf("Field") > -1) ? "Field" : "Node";
    unsubscribeFromAll(mqttClient);
    subscribeToNode(node, mqttClient);
    if (currentNodeView.Type == "Field") {
        chartUpdateInterval = setInterval(updateChart,1000);
    }
}


/* Maps */
var googleMap;
var mapCenter;
var mapMarker;

function initialize() {
  var mapProp = {
    center:new google.maps.LatLng(51.9851034,5.8987296),
    zoom:5,
    mapTypeId:google.maps.MapTypeId.TERRAIN
  };
  mapCenter = mapProp.center;
  googleMap=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}

function setMarkerToMap(latitude, longitude) {
	console.log("Setting new marker!");
	mapCenter=new google.maps.LatLng(longitude, latitude);
	mapMarker=new google.maps.Marker({ position:mapCenter });
	googleMap.setCenter(mapCenter);
	googleMap.setZoom(14);
	mapMarker.setMap(googleMap);
}

function clearMarkerToMap () {
    mapMarker.setMap(null);
}

function centerMap() {
	googleMap.setCenter(mapCenter);
}


/* Chart */
var chartData = {
    labels: [],
    datasets: [{
                fillColor:          'rgba(00,51,204,0.6)',
                strokeColor:        "#0033cc",
                pointColor:         "#0033cc",
                pointStrokeColor:   "#FFF",
                data: []
    }]
}

var todayChart;

function setupChart()
{
	//document.getElementById("myChart").width = document.getElementById("chartDiv").width;
	//document.getElementById("myChart").height = document.getElementById("chartDiv").height;
	//todayChart = new Chart(document.getElementById("yieldChart").getContext("2d")).Line(chartData, {animation: false});
    todayChart = new Chart(document.getElementById("yieldChart").getContext("2d")).Line(chartData, {animation: false, bezierCurveTension : (1/4)});
	updateChart();
}

function updateChart()
{
    if(updateChart.counter == undefined){
		updateChart.counter = 0;
	}
	var curTime = new Date();
    var h = (curTime.getHours() < 10) ? "0" + curTime.getHours() : curTime.getHours();
    var m = (curTime.getMinutes() < 10) ? "0" + curTime.getMinutes() : curTime.getMinutes();
    var s = (curTime.getSeconds() < 10) ? "0" + curTime.getSeconds() : curTime.getSeconds();

    switch (currentNodeView.Type) {
        case "Node":
            console.log("updateChart() : Displaying Node Yield");
            var currentHarvest = document.getElementById("currentHarvest").innerHTML;
            todayChart.addData([currentHarvest], h + ":" + m + ":" + s );
            break;
        case "Field":
            console.log("updateChart() : Displaying Field Yield");
            var FieldYield = 0;
            for(var x in currentNodeView.FieldData) {
                if (currentNodeView.FieldData[x].WasUpdated) {
                    FieldYield += currentNodeView.FieldData[x].Yield / currentNodeView.FieldData[x].Samples;
                    currentNodeView.FieldData[x].WasUpdated = false;
                }
            }
            document.getElementById("currentHarvest").innerHTML = FieldYield;
            todayChart.addData([FieldYield], h + ":" + m + ":" + s );
            break;
        default:
            console.log("updateChart() : Unknown display type, ignoring");
    }
	if(updateChart.counter++ > 12){
		todayChart.removeData();
	}
	todayChart.update();

    /*
	if(updateChart.counter == undefined){
		updateChart.counter = 0;
	}
	var curTime = new Date();
	var currentHarvest = document.getElementById("currentHarvest").innerHTML;
	todayChart.addData([currentHarvest], curTime.getHours() + ":" + curTime.getMinutes() );
	if(updateChart.counter++ > 12){
		todayChart.removeData();
	}
	todayChart.update();
    */
}


/* MQTT */
var MQTT_SERVER_NAME = "mqtt.soltram.info";
var MQTT_SERVER_PORT = 8083;
var MQTT_CLIENT_NAME = "micromesh-web" + Math.floor(Math.random() * 1000);
var MQTT_ROOT_TOPIC = "SOLTRAM";
var mqttClient;

function mqttSetup(host     = MQTT_SERVER_NAME,
                   port     = MQTT_SERVER_PORT,
                   clientID = MQTT_CLIENT_NAME)
{
    // Debug
	console.log("Note from mqttSetup() : Setting up mqttClient...");

    // Create the client
	mqttClient = new Paho.MQTT.Client(host, Number(port), clientID);

	// Set callback handlers
	mqttClient.onConnectionLost = onConnectionLost;
	mqttClient.onMessageArrived = onMessageArrived;

	// Connect the client
    try {
        mqttClient.connect({
            onSuccess:onConnect,
            onFailure:onConnectFail,
            invocationContext:mqttClient
        });
    }
    catch (e) {
        console.log("Error in function mqttSetup() : Error when invoking Paho.MQTT.Client.connect() - " + e);
    }

    // Do some dirty additions to the client object
    mqttClient.currentSubscriptions = [];
    mqttClient.currentBaseTopic = "";
}

function mqttShutdown() {
    // Try to disconnent
    try {
        if (mqttClient.hasOwnProperty("disconnect")) {
            mqttClient.disconnect();
        } else {
            throw "MQTT client not initialised.";
        }
    }
    catch (e) {
        console.log("Note in function mqttShutdown() : Unable to disconnect - " + e);
    }

    delete mqttClient;
}

// called when the client connects
function onConnect(respObj) {
    // Object:respObj
    // +--- invocationContext

    console.log("MQTT Client connected.");


}

function onConnectFail(respObj) {
    console.log("MQTT Client failed to connect.");
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("mqttClient Disconnected: " + responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
  // Debug
  console.log("Note from function onMessageArrived() : Received message, Topic: " + message.destinationName+ ", Message: " +message.payloadString);
  //console.log(message);

    if (currentNodeView.Type === "Node") {
        // Received Yield
        if(message.destinationName.indexOf("Current_Yield") > -1) {
            document.getElementById("currentHarvest").innerHTML = message.payloadString;
            if(message.payloadString != "unknown"){
                updateChart();
            }
        }

        // Received GPS coords
        if(message.destinationName.indexOf("GPS") > -1) {
            var stringArray = message.payloadString.split(" ");
            //setMarkerToMap(stringArray[0], stringArray[1]);
            //document.getElementById("gpslat").innerHTML = "Latitude: " + stringArray[0] + "&deg";
            //document.getElementById("gpslong").innerHTML = "longitude: " + stringArray[1] + "&deg";
            currentNodeView.DynamicData.Latitude = Number(stringArray[0]);
            currentNodeView.DynamicData.longitude = Number(stringArray[1]);
        }

        // Received current orientation
        if(message.destinationName.indexOf("Current_Orientation") > -1) {
            // document.getElementById("curori-hor").innerHTML = "Horizontal: " + message.payloadString + "&deg";
            // document.getElementById("curori-ver").innerHTML = "Vertical: " + message.payloadString + "&deg";
            try {
                var stringArray = message.payloadString.split(" ");
            }
            catch (e) {
                console.log("Error in function onMessageArrived() : Failed to fetch message string - " + e);
                return;
            }

            if (stringArray.length == 1) {
                currentNodeView.DynamicData.CurrentOrientation.Horizontal = ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00);
                currentNodeView.DynamicData.CurrentOrientation.Vertical = ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00);
                fillPage({
                    "Orientations" : {
                        "Current" : {
                            "Horizontal" : ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00),
                            "Vertical" : ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00)
                        },
                        "Error" : {
                            "Horizontal" : 0,
                            "Vertical" : 0
                        }
                    }
                });
            } else if (stringArray.length > 1) {
                currentNodeView.DynamicData.CurrentOrientation.Horizontal = ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00);
                currentNodeView.DynamicData.CurrentOrientation.Vertical = ((Number(stringArray[1]) !== NaN)?Number(stringArray[1]):0.00);
                fillPage({
                    "Orientations" : {
                        "Current" : {
                            "Horizontal" : ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00),
                            "Vertical" : ((Number(stringArray[1]) !== NaN)?Number(stringArray[1]):0.00)
                        },
                        "Error" : {
                            "Horizontal" : currentNodeView.DynamicData.CalculatedOrientation.Horizontal - currentNodeView.DynamicData.CurrentOrientation.Horizontal,
                            "Vertical" : currentNodeView.DynamicData.CalculatedOrientation.Vertical - currentNodeView.DynamicData.CurrentOrientation.Vertical
                        }
                    }
                });
            } else {
                console.log("Error in function onMessageArrived() : Message payload unusable. (" + message.payloadString + ").");
                return;
            }
        }
        if(message.destinationName.indexOf("Calculated_Orientation") > -1) {
            document.getElementById("solori-hor").innerHTML = "Horizontal: " + message.payloadString + "&deg";
            document.getElementById("solori-ver").innerHTML = "Vertical: " + message.payloadString + "&deg";
        // document.getElementById("solerr-hor").innerHTML = "Horizontal: " + abs(document.getElementById("curori-hor").innerHTML.value - document.getElementById("solori-hor").innerHTML.value) + "&deg";
        // document.getElementById("solerr-ver").innerHTML = "Vertical: " + abs(document.getElementById("curori-ver").innerHTML.value - document.getElementById("solori-hor").innerHTML.value) + "&deg";
        try {
                var stringArray = message.payloadString.split(" ");
            }
            catch (e) {
                console.log("Error in function onMessageArrived() : Failed to fetch message string - " + e);
                return;
            }

            if (stringArray.length == 1) {
                currentNodeView.DynamicData.CurrentOrientation.Horizontal = ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00);
                currentNodeView.DynamicData.CurrentOrientation.Vertical = ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00);
                fillPage({
                    "Orientations" : {
                        "Calculated" : {
                            "Horizontal" : ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00),
                            "Vertical" : ((Number(stringArray[1]) !== NaN)?Number(stringArray[1]):0.00)
                        },
                        "Error" : {
                            "Horizontal" : 0,
                            "Vertical" : 0
                        }
                    }
                });
            } else if (stringArray.length > 1) {
                currentNodeView.DynamicData.CalculatedOrientation.Horizontal = ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00);
                currentNodeView.DynamicData.CalculatedOrientation.Vertical = ((Number(stringArray[1]) !== NaN)?Number(stringArray[1]):0.00);
                fillPage({
                    "Orientations" : {
                        "Calculated" : {
                            "Horizontal" : ((Number(stringArray[0]) !== NaN)?Number(stringArray[0]):0.00),
                            "Vertical" : ((Number(stringArray[1]) !== NaN)?Number(stringArray[1]):0.00)
                        },
                        "Error" : {
                            "Horizontal" : currentNodeView.DynamicData.CalculatedOrientation.Horizontal - currentNodeView.DynamicData.CurrentOrientation.Horizontal,
                            "Vertical" : currentNodeView.DynamicData.CalculatedOrientation.Vertical - currentNodeView.DynamicData.CurrentOrientation.Vertical
                        }
                    }
                });
            } else {
                console.log("Error in function onMessageArrived() : Message payload unusable. (" + message.payloadString + ").");
                return;
            }
        }
        if(message.destinationName.indexOf("Current_Temperature") > -1) {
            document.getElementById("curtmp").innerHTML = message.payloadString + "&deg Celcius";
            currentNodeView.DynamicData.Temperature = Number(message.payloadString);
        }
    }
    if (currentNodeView.Type === "Field") {
        if(message.destinationName.indexOf("Current_Yield") > -1) {
            var n = message.destinationName.indexOf("Node");
            var nodeName;
            if(n > -1) {
                nodeName = message.destinationName.substr(n).split("/")[0];
                console.log("Got a report from " + nodeName);
                if (currentNodeView.FieldData.hasOwnProperty(nodeName)) {
                    currentNodeView.FieldData[nodeName].Samples++;
                    currentNodeView.FieldData[nodeName].Yield += Number(message.payloadString);
                    currentNodeView.FieldData[nodeName].WasUpdated = true;
                } else {
                    currentNodeView.FieldData[nodeName] = {
                        "Samples" : 1,
                        "Yield" : Number(message.payloadString),
                        "WasUpdated" : true
                    };
                }
            }
            /*
            document.getElementById("currentHarvest").innerHTML = message.payloadString;
            if(message.payloadString != "unknown"){
                updateChart();
            }
            */
        }
    }
}

function onSubscribe (respObj) {
    // Object:respObj
    // |--- invocationContext
    // +--- gratedQos
    console.log("Succesffully Subscribed.");
}

function onUnsubscribe (respObj) {
    // Object:respObj
    // |--- invocationContext
    // +--- errorCode

}

function subscribeToNode () {
    var dummy;
	var node =	$('#tree').treeview('getSelected', dummy);
	var mqttBaseTopic;

	if(node[0].text.indexOf("Field") > -1) {
		mqttBaseTopic = "SOLTRAM/" + node[0].text + "/#"
	} else {
		var parentNode = $('#tree').treeview('getParent', node[0]);
		//mqttBaseTopic = "SOLTRAM/" + parentNode.text + "/" + node[0].text + "/";
        mqttBaseTopic = "SOLTRAM/" + parentNode.text + "/" + node[0].text + "/#";
	}

    var subscOpts = {"onSuccess":onSubscribe};

    /*
	mqttClient.subscribe(mqttBaseTopic + "Current_Yield", subscOpts);
    mqttClient.currentSubscriptions.push(mqttBaseTopic + "Current_Yield");
	mqttClient.subscribe(mqttBaseTopic + "Current_Orientation", subscOpts);
    mqttClient.currentSubscriptions.push(mqttBaseTopic + "Current_Orientation");
	mqttClient.subscribe(mqttBaseTopic + "Calculated_Orientation", subscOpts);
    mqttClient.currentSubscriptions.push(mqttBaseTopic + "Calculated_Orientation");
	mqttClient.subscribe(mqttBaseTopic + "Current_Temperature", subscOpts);
    mqttClient.currentSubscriptions.push(mqttBaseTopic + "Current_Temperature");
	mqttClient.subscribe(mqttBaseTopic + "GPS", subscOpts);
    mqttClient.currentSubscriptions.push(mqttBaseTopic + "GPS");
	mqttClient.subscribe(mqttBaseTopic + "Soltrack_Error", subscOpts);
    mqttClient.currentSubscriptions.push(mqttBaseTopic + "Soltrack_Error");
    */
    mqttClient.subscribe(mqttBaseTopic);
    mqttClient.currentSubscriptions.push(mqttBaseTopic);

	console.log("Subscribing to " + MQTT_SERVER_NAME + " topic(base): " + mqttBaseTopic);
}

function unsubscribeFromAll () {
    console.log(mqttClient);
    var l = mqttClient.currentSubscriptions.length;
    for (var i = 0;i<l;i++) {
        var t = mqttClient.currentSubscriptions.shift();
        console.log((i+1) + "/" + l + ": " + "Unsubscribing from "+t);
        mqttClient.unsubscribe(t);
    }
}
