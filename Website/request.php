<?php


    $mysql_host = "soltram.info";
    $mysql_port = "3306";
    $mysql_user = "soltram";
    $mysql_pass = "Soltram-1617";
    $mysql_data = "soltram_database";
    $mysql_link;

    function connect () {
        global $mysql_host, $mysql_port, $mysql_user, $mysql_pass, $mysql_data, $mysql_link;
        $mysql_link = new mysqli($mysql_host.":".$mysql_port,$mysql_user,$mysql_pass,$mysql_data);
        if ($mysql_link->connect_errno) {
            print "{
                \"ErrorCode\" : 101,
                \"ErrorMessage\" : \"Failed to connect to database: Code ".$mysql_link->errno.", ".$mysql_link->error."\"
            }";
            exit;
        }
    }

    function disconnect () {
        global $mysql_link;
        $mysql_link->close();
    }

    class ResponseObject{
        public $StatusCode = 0;
        public $ErrorCode = 0;
        public $ErrorMessage = "";
        public $Results = [];
        public function SetError($ErrorObject) {
            if($ErrorObject instanceof ErrorState) {
                $this->ErrorCode = $ErrorObject->ErrorCode;
                $this->ErrorMessage = $ErrorObject->ErrorMessage;
            } else {
                $this->ErrorCode = 999;
                $this->ErrorMessage = "An internal error occured.";
            }
        }
        public function ToJSON() {
            $resp = "{\"StatusCode\" : $this->StatusCode, \"ErrorCode\" : $this->ErrorCode, \"ErrorMessage\" : \"$this->ErrorMessage\"";
            if(count($this->Results) > 0) {
                $resp += ", \"Results\" : {}";
            }
            $resp += "}";
            return $resp;
        }
    }

    class ErrorState{
        public $ErrorCode;
        public $ErrorMessage;
        function __construct($code,$message) {
            $this->ErrorCode = $code;
            $this->ErrorMessage = $message;
        }
    }

    switch ($_GET["q"]) {
        case "nodeTree":
            fetchNodeTree("fields","nodes","node_config");
            break;
        case "dummyNodeTree":
            fetchNodeTree("dummy_fields","dummy_nodes","dummy_node_config");
            break;
        case "centralLog":
            $start = 0;
            $limit = 30;
            if ($_GET["start"]) {$start = $_GET["start"];}
            if ($_GET["limit"]) {$limit = $_GET["limit"];}
            fetchCentralLog($start, $limit);
            break;
        case "messageLog":
            $start = 0;
            $limit = 30;
            if ($_GET["start"]) {$start = $_GET["start"];}
            if ($_GET["limit"]) {$limit = $_GET["limit"];}
            fetchMessageLog($start, $limit);
            break;
        default:
            print "{\"ErrorCode\" : 1, \"ErrorMessage\" : \"Invalid Request\"}";
            break;
    }

    /*
    function fetchNodeTree() {
        print "{\"ErrorCode\" : 2, \"ErrorMessage\" : \"Not yet Implemented\"}";
    }
    */

    function fetchNodeTree($field_table, $node_table, $nodeconf_table) {
        global $mysql_host, $mysql_port, $mysql_user, $mysql_pass, $mysql_data, $mysql_link;
        /*
        $field_table = "dummy_fields";
        $node_table = "dummy_nodes";
        $nodeconf_table = "dummy_node_config";
        */
        $mysqlcon = mysql_connect($mysql_host.":".$mysql_port,$mysql_user,$mysql_pass);
        //$mysqlcon = mysql_connect("hostdoesnotexist.com","JohnDoe","0000");

        if (mysql_errno($mysqlcon)) {
            // Something went wrong with connecting
            print "{
                \"ErrorCode\" : 101,
                \"ErrorMessage\" : \"Failed to connect to database: Code ".mysql_errno($mysqlcon).", ".mysql_error($mysqlcon)."\"
            }";
        } else {
            // Connection successful, fetch the node fields
            $fieldQuery = "SELECT `id`, `name`,`latitude`,`longitude` FROM `$mysql_data`.`$field_table`";
            if ($fieldResult = mysql_query($fieldQuery, $mysqlcon)) {

                print "{
                      \"ErrorCode\" : -1,
                      \"ErrorMessage\" : \"This is not implemented yet.\",
                      \"Results\" : {
                      \"Fields\" : [\n";
                $fieldCount = mysql_num_rows($fieldResult);
                $fc = 1;
                while ($row = mysql_fetch_array($fieldResult,MYSQL_NUM)) {
                    // For each field: ...
                    print "{\"Name\":\"".$row[1]."\",\"Lat\":".$row[2].",\"Lon\":".$row[3].", \"Nodes\":[\n";

                    // Check the configuration table for nodes in this field
                    $nodeconfQuery = "SELECT `node_id` FROM `$mysql_data`.`$nodeconf_table` WHERE `field_id`=$row[0]";
                    if ($nodeconfResult = mysql_query($nodeconfQuery,$mysqlcon)) {
                        $nodeCount = mysql_num_rows($nodeconfResult);
                        $nc = 1;
                        while ($nodeconfRow = mysql_fetch_array($nodeconfResult,MYSQL_NUM)) {
                            // There are nodes assigned to this field, fetch them
                            $nodeQuery = "SELECT `node_name`, `latitude`, `longitude` FROM `$mysql_data`.`$node_table` WHERE `id` = $nodeconfRow[0]";
                            $nodeResult = mysql_query($nodeQuery,$mysqlcon);
                            if (mysql_num_rows($nodeResult) > 0) {
                                // The node_id exists, this should only be one row
                                $nodeRow = mysql_fetch_array($nodeResult,MYSQL_NUM);
                                print "{\"Name\":\"".$nodeRow[0]."\",\"Lat\":".$nodeRow[1].",\"Lon\":".$nodeRow[2]."}";
                            } else {
                                print "{\"Name\":\"$row[0]\",\"Lat\":0,\"Lon\":0}";
                            }
                            if ($nc != $nodeCount) {print ",";} // Put a comma after it if it's not the last one
                            print "\n";
                            ++$nc;
                        }
                    }
                    print "]}";
                    if ($fc != $fieldCount) {print ",";} // Put a comma after it if it's not the last one
                    print "\n";
                    ++$fc;
                }

                print "]}}";
            } else {
                print "{
                    \"ErrorCode\" : 201,
                    \"ErrorMessage\" : \"Error when making query '$fieldQuery': ".mysql_error($mysqlcon)."\"
                }";
            }
        }

        mysql_close($mysqlcon);
    }

    function fetchCentralLog($start, $limit) {
        connect();
        global $mysql_link, $mysql_data;
        $table = "central_log";
        $query = "SELECT `time`,`status_code`,`message` FROM `$mysql_data`.`$table` LIMIT $limit OFFSET $start";
        $result = $mysql_link->query($query);
        if (!$result) {
            print "{\"ErrorCode\" : 302, \"ErrorMessage\" : \"Error in query, code $mysql_link->errno, $mysql_link->error.\"}";
        } else {
            print "{\"ErrorCode\" : 303, \"ErrorMessage\" : \"Query Successful\"";

            print ", \"Query\" : \"$query\"";

            if($result->num_rows > 0) {
                print ", \"Results\" : {\"Logs\" : [";
                $rc = 1;
                    while($row = $result->fetch_row()) {
                        print "{\"Time\" : \"$row[0]\", \"StatusCode\" : $row[1], \"Message\" : \"$row[2]\"}";
                        if($rc != $result->num_rows) {print ",";}
                        ++$rc;
                    }
                print "]}";
            }

            print "}";
            /*
            $rsp = new ResponseObject;
            $rsp->ErrorCode = 301;
            $rsp->ErrorMessage = "This function is not complete!";
            print $rsp->ToJSON();
            */
        }
        $result->free();
        disconnect();
    }

    function fetchMessageLog($start, $limit) {
        connect();
        global $mysql_link, $mysql_data;
        $table = "gateway_message_log";
        $query = "SELECT `time`,`node_id`,`message` FROM `$mysql_data`.`$table` LIMIT $limit OFFSET $start";
        $result = $mysql_link->query($query);
        if (!$result) {
            print "{\"ErrorCode\" : 402, \"ErrorMessage\" : \"Error in query, code $mysql_link->errno, $mysql_link->error.\"}";
        } else {
            print "{\"ErrorCode\" : 403, \"ErrorMessage\" : \"Query Successful\"";

            print ", \"Query\" : \"$query\"";

            if($result->num_rows > 0) {
                print ", \"Results\" : {\"Logs\" : [";
                $rc = 1;
                    while($row = $result->fetch_row()) {
                        $nodeQuery = "SELECT `node_name` FROM `$mysql_data`.`nodes` WHERE `id`=$row[1]";
                        $nodeResult = $mysql_link->query($nodeQuery);
                        if($nodeResult->num_rows > 0) {
                            $nodeID = $nodeResult->fetch_row()[0];
                        } else {
                            $nodeID = "($row[1])";
                        }
                        print "{\"Time\" : \"$row[0]\", \"Node\" : \"$nodeID\", \"Message\" : \"$row[2]\"}";
                        if($rc != $result->num_rows) {print ",";}
                        ++$rc;
                    }
                print "]}";
            }

            print "}";
            /*
            $rsp = new ResponseObject;
            $rsp->ErrorCode = 301;
            $rsp->ErrorMessage = "This function is not complete!";
            print $rsp->ToJSON();
            */
        }
        $result->free();
        disconnect();
    }

?>
