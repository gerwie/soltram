var MQTT_SERVER_NAME = "mqtt.soltram.info"

var tree;

var googleMap;
var mapCenter;
var mapMarker;

$( document ).ready(function() {
	tree = createTree();
	var field1 = createField("1", 51.9851034, 5.8987296);
	addNode(field1, "000002", 51.994333, 5.9776939);
	tree.push(field1);

	$('#tree').treeview({
		data: tree,
		onNodeSelected: function(event, node) {
			nodeSelected(event, node);
			//document.getElementById("nodeName").innerHTML = node.text;
			//setMarkerToMap(node.latitude, node.longitude);
		}
	});

	// Maps
	google.maps.event.addDomListener(window, 'load', initialize);
	google.maps.event.addDomListener(window, 'resize', centerMap);

	// yieldChart
	setupChart();
	//setInterval(updateChart, 1000);
});

/* Tree */
function getTree() {
  // Some logic to retrieve, or generate tree structure
  var data = [
  {
    text: "Field",
	id: "1",
	latitude: 51.9851034,
	longitude: 5.8987296,
    nodes: [
      {
        text: "Node",
		id: "000225",
		latitude: 51.994333,
		longitude: 5.9776939
      },
      {
        text: "Node",
		id: "001222",
		latitude: 51.9868795,
		longitude: 5.9462289
      }
    ]
  }
];

  return data;
}

function createTree() {
	var data = [];
	return data;
}

function createField(id, latitude, longitude) {
	var data = {
		text: "Field" + " " + id,
		id: id,
		latitude: latitude,
		longitude: longitude,
		nodes: []
	};

	return data;
}

function addNode(datacontainer, id, latitude, longitude) {
	datacontainer.nodes.push({
		text: "Node" + " " + id,
		id: id,
		latitude: latitude,
		longitude: longitude
		});
}

function nodeSelected(event, node) {
	document.getElementById("nodeName").innerHTML = node.text;
	setMarkerToMap(node.latitude, node.longitude);
	document.getElementById("gpslat").innerHTML = node.latitude;
	document.getElementById("gpslong").innerHTML = node.longitude;
	mqttSetup();
}


/* Maps */
function initialize() {
  var mapProp = {
    center:new google.maps.LatLng(51.9851034,5.8987296),
    zoom:5,
    mapTypeId:google.maps.MapTypeId.TERRAIN
  };
  mapCenter = mapProp.center;
  googleMap=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}

function setMarkerToMap(latitude, longitude) {
	console.log("Setting new marker!");
	mapCenter=new google.maps.LatLng(latitude, longitude);
	mapMarker=new google.maps.Marker({ position:mapCenter });
	googleMap.setCenter(mapCenter);
	googleMap.setZoom(14);
	mapMarker.setMap(googleMap);
}

function centerMap() {
	googleMap.setCenter(mapCenter);
}

/* Chart */
var chartData = {
    labels: [],
    datasets: [{
                fillColor:          'rgba(00,51,204,0.6)',
                strokeColor:        "#0033cc",
                pointColor:         "#0033cc",
                pointStrokeColor:   "#FFF",
                data: []
    }]
}

var todayChart;

function setupChart()
{
	//document.getElementById("myChart").width = document.getElementById("chartDiv").width;
	//document.getElementById("myChart").height = document.getElementById("chartDiv").height;
	todayChart = new Chart(document.getElementById("yieldChart").getContext("2d")).Line(chartData, {animation: false});
	updateChart();
}

function updateChart()
{
	if(updateChart.counter == undefined){
		updateChart.counter = 0;
	}
	var curTime = new Date();
	var currentHarvest = document.getElementById("currentHarvest").innerHTML;
	todayChart.addData([currentHarvest], curTime.getHours() + ":" + curTime.getMinutes() );
	if(updateChart.counter++ > 12){
		todayChart.removeData();
	}
	todayChart.update();
}

/* MQTT */
var mqttClient;

function mqttSetup()
{
	console.log("Setting up mqttClient");
	mqttClient = new Paho.MQTT.Client(MQTT_SERVER_NAME, Number(8083), "webapplication");

	// set callback handlers
	mqttClient.onConnectionLost = onConnectionLost;
	mqttClient.onMessageArrived = onMessageArrived;

	// connect the client
	mqttClient.connect({onSuccess:onConnect});
}

// called when the client connects
function onConnect() {
	var dummy;
	var node =	$('#tree').treeview('getSelected', dummy);
	var mqttBaseTopic;

	if(node[0].text.indexOf("Field") > -1) {
		mqttBaseTopic = "SOLTRAM/" + node[0].text + "/"
	} else {
		var parentNode = $('#tree').treeview('getParent', node[0]);
		mqttBaseTopic = "SOLTRAM/" + parentNode.text + "/" + node[0].text + "/";
	}

	mqttClient.subscribe(mqttBaseTopic + "Current_Yield");
	mqttClient.subscribe(mqttBaseTopic + "Current_Orientation");
	mqttClient.subscribe(mqttBaseTopic + "Calculated_Orientation");
	mqttClient.subscribe(mqttBaseTopic + "Current_Temperature");
	mqttClient.subscribe(mqttBaseTopic + "GPS");
	mqttClient.subscribe(mqttBaseTopic + "Soltrack_Error");

	console.log("Subscribing to topic(base): " + mqttBaseTopic);
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("mqttClient Disconnected");
  }
}

// called when a message arrives
function onMessageArrived(message) {
  console.log("Message arrived. Topic: " + message.destinationName+ ", Message: " +message.payloadString);
  console.log(message);

  if(message.destinationName.indexOf("Current_Yield") > -1) {
	  document.getElementById("currentHarvest").innerHTML = message.payloadString;
	  if(message.payloadString != "unknown"){
		  updateChart();
	  }
  }
  if(message.destinationName.indexOf("GPS") > -1) {
	  var stringArray = message.payloadString.split(" ");
	  setMarkerToMap(stringArray[0], stringArray[1]);
	  document.getElementById("gpslat").innerHTML = stringArray[0];
	  document.getElementById("gpslong").innerHTML = stringArray[1];
  }
  if(message.destinationName.indexOf("Current_Orientation") > -1) {
	  document.getElementById("curori").innerHTML = message.payloadString;
  }
  if(message.destinationName.indexOf("Calculated_Orientation") > -1) {
	  document.getElementById("solori").innerHTML = message.payloadString;
	  document.getElementById("solerr").innerHTML = abs(document.getElementById("curori").innerHTML.value - document.getElementById("solori").innerHTML.value);
  }
  if(message.destinationName.indexOf("Current_Temperature") > -1) {
	  document.getElementById("curtmp").innerHTML = message.payloadString;
  }
  if(message.destinationName.indexOf("Soltrack_Error") > -1) {
	  document.getElementById("curori").innerHTML = message.payloadString;
  }

}
