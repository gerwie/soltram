TEMPLATE = app
CONFIG += console c++11 thread
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -L/usr/local/mysql/lib -lmysqlclient
LIBS += -L/usr/mysql-connector-c -lmysqlcppconn -lmosquittopp

INCLUDEPATH +=  -I/usr/include \
                -I/usr/local/include \
                -I/usr/local/include/cppconn \
                -I/usr/include/mysql \
                -I/usr/local/mysql/include \
                -I/usr/local/boost_1_62_0 \


SOURCES += main.cpp \
    libs/database/database.cpp \
    libs/gateway_mysql/gateway_mysql.cpp \
    libs/xbee_dm/xbee_dm.cpp \
    libs/uart/uart.cpp \
    libs/mqtt_client/mqtt_client.cpp \
    libs/gateway_class/gateway_class.cpp \
    libs/gateway_message/gateway_message.cpp \
    libs/string_utils/string_utils.cpp

HEADERS += \
    app_info.h \
    config.h \
    libs/database/database.h \
    libs/gateway_mysql/gateway_mysql.h \
    libs/xbee_dm/xbee_dm.h \
    libs/uart/uart.h \
    libs/mqtt_client/mqtt_client.h \
    libs/gateway_class/gateway_class.h \
    libs/gateway_message/gateway_message.h \
    libs/string_utils/string_utils.h \
    main.h



QMAKE_LFLAGS +=
