#ifndef GATEWAYMYSQL_H
#define GATEWAYMYSQL_H

#include <mysql/mysql.h>
#include <string.h>
#include <iostream>

class GatewayMYSQL
{
public:
    GatewayMYSQL(std::string host, int port, std::string user, std::string password, std::string database);
    virtual ~GatewayMYSQL();

    MYSQL_RES* performQuery(std::string query);

private:
    MYSQL* connect(void);
    MYSQL* sqlConnection;

    std::string host, user, password, database;
    int port;

};

#endif // GATEWAYMYSQL_H
