#include "gateway_mysql.h"

GatewayMYSQL::GatewayMYSQL(std::string host, int port, std::string user, std::string password, std::string database)
    : host(host),
      user(user),
      password(password),
      database(database),
      port(port)
{
    sqlConnection = mysql_init(nullptr);
    if(!connect()) {
        std::cout << "[GatewayMYSQL]   GatewayMYSQL failed to connect! Error: " << mysql_error(sqlConnection) << std::endl;
    } else {
        std::cout << "[GatewayMYSQL]   GatewayMYSQL connected!" << std::endl;
    }

}

GatewayMYSQL::~GatewayMYSQL() {

}

MYSQL_RES* GatewayMYSQL::performQuery(std::string query) {
    // send the query to the database
    if (mysql_query(sqlConnection, query.c_str()))
    {
      std::cout << "[GatewayMYSQL]   Query error: " << mysql_error(sqlConnection) << std::endl;
      return nullptr;
    }

    return mysql_store_result(sqlConnection);
}

MYSQL* GatewayMYSQL::connect(void) {
    return mysql_real_connect(sqlConnection, host.c_str(), user.c_str(), password.c_str(), database.c_str(),
            port, nullptr, 0);
}
