#ifndef STRINGUTILS_H
#define STRINGUTILS_H


#include <cctype>
#include <string>
#include <vector>
#include <locale>

int isForwardSlash(int);
std::vector<std::string> split(const std::string&, int (int) = std::isspace);


#endif // STRINGUTILS_H
