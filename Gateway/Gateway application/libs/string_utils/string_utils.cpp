#include "string_utils.h"
#include <iostream>
#include <algorithm>

int isForwardSlash (int c) {
    return (c == '/');
}

std::vector<std::string> split (const std::string& str, int delim(int)) {
  std::vector<std::string> result;
  auto e = end(str);
  auto i = begin(str);

  while (i != e)
  {
    i = find_if_not(i, e, delim);
    if (i != e)
    {
      auto j = find_if(i, e, delim);
      result.push_back(std::string(i, j));
      i = j;
    }
  }

  return result;
}
