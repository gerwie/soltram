#include "xbee_dm.h"
#include <iostream>
#include <iomanip>

XBeeTxMessage::XBeeTxMessage(char * message, uint8_t * sourceAddr, uint8_t * destinationAddr)
    : XBeePacketSize(0),
      bValid(true)
{
    unsigned int checksum = 0;
    sMessage = (char*) malloc(strlen(message)+1);

    if(message != NULL)
        strcpy(sMessage, message);
    if(sourceAddr != NULL)
        memcpy(srcAddr, sourceAddr, ADDRESS_SIZE);
    if(destinationAddr != NULL)
        memcpy(destAddr, destinationAddr, ADDRESS_SIZE);


    unsigned int packetSize = 10 + ADDRESS_SIZE + strlen(sMessage);

    XBeePacket = (uint8_t*) malloc(packetSize);
        memset(XBeePacket, 0, packetSize);

    XBeePacket[0] = XBEE_START_DELIMITER;
    XBeePacket[1] = (unsigned char) (0xFF & ((packetSize-4) >> 8));
    XBeePacket[2] = (unsigned char) (0xFF & (packetSize-4));
    XBeePacket[3] = XBEE_TX_FRAME_TYPE;
    XBeePacket[4] = XBEE_DEFAULT_TX_FRAME_ID;
    memcpy(&XBeePacket[5], destAddr, ADDRESS_SIZE);
    XBeePacket[13] = (char) 0xff;
    XBeePacket[14] = (char) 0xfe;
    XBeePacket[15] = 0x00;
    XBeePacket[16] = 0x00;
    memcpy(&XBeePacket[17], sMessage, strlen(sMessage));


    for(unsigned int i = 3; i < packetSize; i++) {
        checksum += XBeePacket[i];
    }

    XBeePacket[packetSize-1] = 0xff - checksum;

    XBeePacketSize = packetSize;
}

XBeeTxMessage::XBeeTxMessage(uint8_t * receivedXBeePacket, unsigned int uiPacketLength)
    : XBeePacketSize(uiPacketLength),
      bValid(true)
{
    XBeePacket = (uint8_t*) malloc(XBeePacketSize);
        memset(XBeePacket, 0, XBeePacketSize);
    memcpy(XBeePacket, receivedXBeePacket, XBeePacketSize);

    if(XBeePacket[0] != XBEE_START_DELIMITER) {
        bValid = false;
        return;
    }
    unsigned int checksum = 0;
    for(unsigned int i = 3; i < XBeePacketSize; i++) {
        checksum += XBeePacket[i];
    }
    checksum &= 0xFF;
    if(checksum != 0xFF) {
        bValid = false;
        return;
    }

    memcpy(srcAddr, &XBeePacket[5], ADDRESS_SIZE);

    unsigned int messageSize = (((XBeePacket[1] << 8) | (XBeePacket[2])) - 14);
    sMessage = (char*) malloc(messageSize+1);
    memcpy(sMessage, (const char*) &XBeePacket[17], messageSize);

}

XBeeTxMessage::XBeeTxMessage(const XBeeTxMessage &cpInstance)
{
    memcpy((void*) srcAddr, (void*) cpInstance.srcAddr, ADDRESS_SIZE);
    memcpy((void*) destAddr, (void*) cpInstance.destAddr, ADDRESS_SIZE);

    sMessage = (char*) malloc(strlen(cpInstance.sMessage));
    strcpy(sMessage, cpInstance.sMessage);

    XBeePacket = (uint8_t*) malloc(cpInstance.XBeePacketSize);
    memcpy(XBeePacket, cpInstance.XBeePacket, cpInstance.XBeePacketSize);

    XBeePacketSize = cpInstance.XBeePacketSize;
    bValid = cpInstance.bValid;
}

XBeeTxMessage::~XBeeTxMessage() {
    free(sMessage);
    free(XBeePacket);
}

XBeeRxMessage::XBeeRxMessage(uint8_t * receivedXBeePacket, uint16_t uiPacketLength)
    : XBeePacketSize(uiPacketLength),
        bValid(true)
{
    XBeePacket = (uint8_t*) malloc(XBeePacketSize);
    sMessage = (char*) malloc(XBeePacketSize - 15);

    memset(XBeePacket, 0, XBeePacketSize);
    memcpy((void*) XBeePacket, (void*) receivedXBeePacket, XBeePacketSize);

    if(XBeePacket[0] != XBEE_START_DELIMITER) {
        bValid = false;
        return;
    }

    if(XBeePacket[3] != XBEE_RX_FRAME_TYPE) {
        bValid = false;
        return;
    }

    unsigned int checksum = 0;
    for(unsigned int i = 3; i < XBeePacketSize; i++) {
        checksum += XBeePacket[i];
    }
    checksum &= 0xFF;

    if(checksum != 0xFF) {
        bValid = false;
        return;
    }

    memcpy(srcAddr, &XBeePacket[4], ADDRESS_SIZE);
    receiveOptions = XBeePacket[14];

    //strcpy(sMessage, (const char*) &XBeePacket[15]);
    memcpy(sMessage, &XBeePacket[15], XBeePacketSize - 16);
    sMessage[XBeePacketSize - 16] = '\0';	// XBee message does not contain NULL character.
}

XBeeRxMessage::XBeeRxMessage(const XBeeRxMessage &cpInstance)
{
    memcpy((void*) srcAddr, (void*) cpInstance.srcAddr, ADDRESS_SIZE);

    sMessage = (char*) malloc(strlen(cpInstance.sMessage));
    strcpy(sMessage, cpInstance.sMessage);

    receiveOptions = cpInstance.receiveOptions;

    XBeePacketSize = cpInstance.XBeePacketSize;
    XBeePacket = (uint8_t*) malloc(XBeePacketSize);
    memcpy((void *) XBeePacket, (void*) cpInstance.XBeePacket, cpInstance.XBeePacketSize);

    bValid = cpInstance.bValid;
}

XBeeRxMessage::~XBeeRxMessage() {
    free(sMessage);
    free(XBeePacket);
}
