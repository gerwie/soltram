#ifndef GATEWAY_H
#define GATEWAY_H


#include <iostream>
#include <stdio.h>
#include <vector>
#include <locale>
#include <map>
#include <string>
#include <boost/config.hpp>
#include <boost/bimap.hpp>
#include <boost/lexical_cast.hpp>
#include "time.h"

#include "libs/database/database.h"
#include "config.h"
#include "libs/gateway_message/gateway_message.h"
#include "libs/string_utils/string_utils.h"
#include "libs/xbee_dm/xbee_dm.h"

namespace Gateway {

class Gateway {
    public:
    Gateway (std::string&);
    ~Gateway ();

    XBeeTxMessage ParseMQTTMessage (MQTTMessage&);
    MQTTMessage ParseXBeeRxMessage (const XBeeRxMessage&);
    int handleRegisterCommand(const XBeeRxMessage&);
    XBeeTxMessage createRegisterAckMessage (unsigned int nodeId);
    XBeeTxMessage createTimeSyncMessage (unsigned int nodeId);
    XBeeTxMessage createDateSyncMessage (unsigned int nodeId);

    std::string convertCommandToMqttTopic(std::string cmd);
    std::string MQTT_Topic;

    private:
    std::string convertXBeeAddrToNodeId(std::vector<uint8_t> &xbeeAddr);
    unsigned int convertXBeeAddrToNodeIdUI(std::vector<uint8_t> &xbeeAddr);
    std::vector<uint8_t> convertNodeIdToXBeeAddr(unsigned int nodeId);
    int storeDataInDatabase(unsigned int nodeId, std::string sCommand, std::string sData);

    /* Map with XBEE addresses and Node ID's */
    typedef boost::bimap<unsigned int, std::vector<uint8_t>> nodeMap;
    typedef nodeMap::value_type nodeMapInfo;
    nodeMap registeredNodes;

    Database *dbConnection;
};

}


#endif // GATEWAY_H
