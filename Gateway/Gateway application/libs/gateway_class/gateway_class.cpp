#include "gateway_class.h"


namespace Gateway {
// Gateway class
Gateway::Gateway (std::string& top) :
    MQTT_Topic(std::string(top))
{
    dbConnection = new Database();
}

Gateway::~Gateway() {
}

XBeeTxMessage Gateway::ParseMQTTMessage (MQTTMessage& msg) {
    uint8_t dummy[XBEE_ADDRESS_SIZE];
        memset(dummy,0,XBEE_ADDRESS_SIZE);
    std::size_t strPos = msg.getTopic().find(std::string("Node"));
    std::string nodeId = msg.getTopic().substr(strPos + 5, 6);
    std::cout << "[GATEWAY] Node ID from received MQTT message: " << std::stoi(nodeId) << std::endl;
    std::cout << "[GATEWAY] Corresponding addr: ";
    std::vector<uint8_t> xbeeAddr = convertNodeIdToXBeeAddr((unsigned int) std::stoi(nodeId));
    if(xbeeAddr.size() != ADDRESS_SIZE) {
        std::cout << "Node ID not present in gateway!" << std::endl;
        XBeeTxMessage invalidMsg("\0", dummy, dummy );
        invalidMsg.bValid = false;
        return invalidMsg;
    }
    for(int i = 0; i < ADDRESS_SIZE; i++) {
            std::cout << std::hex << (unsigned int) xbeeAddr.at(i);
        }
    std::cout << std::dec << std::endl;

    std::string dbLogString("MQTT -> NODE: " + msg.getMessage());
    dbConnection->logGatewayMessage(convertXBeeAddrToNodeIdUI(xbeeAddr), dbLogString);

    uint8_t dst [8] {0x00, 0x13, 0xA2, 0x00, 0x40, 0xF9, 0x1F, 0xB0};

    return XBeeTxMessage((char*) msg.getMessage().c_str(), dummy, &xbeeAddr[0]);

}

MQTTMessage Gateway::ParseXBeeRxMessage (const XBeeRxMessage& msg) {
    std::string str (msg.sMessage);
    std::vector<std::string> partedString (split(str));
    std::string rxMessageData(partedString[1]);

    std::cout << "message [" << std::hex << msg.sMessage << std::dec << "]" << std::dec << std::endl;
    std::string dbLogString("NODE -> MQTT: ");
    dbLogString += msg.sMessage;


    if(partedString.size() > 2) {
        rxMessageData += std::string(' ' + partedString[2]);
    }
    /*
    for(auto i: partedString) {
        std::cout << i << " , " ;
    }*/

    std::vector<uint8_t> tempVec;
    for(int i = 0; i < ADDRESS_SIZE; i++) {
        tempVec.push_back(msg.srcAddr[i]);
    }
    std::cout << "[GATEWAY] Message from Node " << convertXBeeAddrToNodeId(tempVec) << std::endl;

    storeDataInDatabase(convertXBeeAddrToNodeIdUI(tempVec), partedString[0], rxMessageData);
    dbConnection->logGatewayMessage(convertXBeeAddrToNodeIdUI(tempVec), dbLogString);

    return MQTTMessage((MQTT_Topic + "/" + "Node " +
                        convertXBeeAddrToNodeId(tempVec) + "/" +
                        convertCommandToMqttTopic(partedString[0])), rxMessageData);
}

int Gateway::handleRegisterCommand(const XBeeRxMessage& rxMessage) {
    std::vector<std::string> partedString(split(std::string(rxMessage.sMessage)));
    if(partedString[0].compare("register") == 0) {
        std::vector<uint8_t> tempVec;
        for(int i = 0; i < ADDRESS_SIZE; i++) {
            tempVec.push_back(rxMessage.srcAddr[i]);
        }
        auto it = registeredNodes.right.find(tempVec);
        if(it != registeredNodes.right.end()) {
            std::cout << "[GATEWAY] XBeeAddr already exists! Overwriting Node ID!" << std::endl;
            registeredNodes.right.replace_data(it, std::stoi(partedString[1]));
            std::cout << "[GATEWAY] Registered Node " << partedString[1] << std::endl;
            dbConnection->addNode(std::stoi(partedString[1]), tempVec);
            return std::stoi(partedString[1]);
        }
        std::cout << "[GATEWAY] Registered Node " << partedString[1] << std::endl;

        registeredNodes.insert(nodeMapInfo(std::stoi(partedString[1]), tempVec));

        dbConnection->addNode(std::stoi(partedString[1]), tempVec);

        return std::stoi(partedString[1]);
    }
    return 0;
}

XBeeTxMessage Gateway::createRegisterAckMessage (unsigned int nodeId) {
    std::cout << "[GATEWAY] Creating ACK Register message for XBeeAddr: ";
    std::vector<uint8_t> xbeeAddr = convertNodeIdToXBeeAddr(nodeId);
    for(int i = 0; i < ADDRESS_SIZE; i++) {
            std::cout << std::hex << (unsigned int) xbeeAddr.at(i);
        }
    std::cout << std::dec << std::endl;

    uint8_t dummy[XBEE_ADDRESS_SIZE];
    memset(dummy,0,XBEE_ADDRESS_SIZE);

    std::string ackMessage("ack regist " + std::to_string(nodeId) + '\n');

    std::cout << "[GATEWAY] Ack message: " << ackMessage << std::endl;

    return XBeeTxMessage((char*) ackMessage.c_str(), dummy, &xbeeAddr[0]);
}

std::string Gateway::convertCommandToMqttTopic(std::string cmd) {
    if(cmd.compare("curhar") == 0) {
        return std::string("Current_Yield");
    }
    if(cmd.compare("curori") == 0) {
        return std::string("Current_Orientation");
    }
    if(cmd.compare("solori") == 0) {
        return std::string("Calculated_Orientation");
    }
    if(cmd.compare("curtmp") == 0) {
        return std::string("Current_Temperature");
    }
    if(cmd.compare("curgps") == 0) {
        return std::string("GPS");
    }
    if(cmd.compare("solerr") == 0) {
        return std::string("Soltrack_Error");
    }
    if(cmd.compare("cudate") == 0) {
        return std::string("Date");
    }
    if(cmd.compare("cutime") == 0) {
        return std::string("Time");
    }

    return std::string("");
}

std::string Gateway::convertXBeeAddrToNodeId(std::vector<uint8_t> &xbeeAddr) {
    char str[7] = "\0";
    sprintf(str, "%06d", registeredNodes.right.find(xbeeAddr)->second);
    return std::string(str);
}

std::vector<uint8_t> Gateway::convertNodeIdToXBeeAddr(unsigned int nodeId) {
    return registeredNodes.left.find(nodeId)->second;
}

unsigned int Gateway::convertXBeeAddrToNodeIdUI(std::vector<uint8_t> &xbeeAddr)
{
    return registeredNodes.right.find(xbeeAddr)->second;
}

XBeeTxMessage Gateway::createTimeSyncMessage(unsigned int nodeId) {
    std::vector<uint8_t> xbeeAddr (convertNodeIdToXBeeAddr(nodeId));
    time_t epoch = time(NULL);
    struct tm *tm = localtime(&epoch);

    uint8_t dummy[XBEE_ADDRESS_SIZE];
    memset(dummy,0,XBEE_ADDRESS_SIZE);

    std::string timeMessage("set cudate ");
    char tmp[2];
    sprintf(tmp, "%02d", tm->tm_hour - 2);
    timeMessage += std::string(tmp);

    sprintf(tmp, "%02d", tm->tm_min);
    timeMessage += std::string(tmp);

    sprintf(tmp, "%02d", tm->tm_sec);
    timeMessage += std::string(tmp);

    timeMessage += std::string("\n");
    std::cout << "Set Cutime message: " << timeMessage << std::endl;

    return XBeeTxMessage((char*) timeMessage.c_str(), dummy, &xbeeAddr[0]);
}

XBeeTxMessage Gateway::createDateSyncMessage(unsigned int nodeId) {
    std::vector<uint8_t> xbeeAddr (convertNodeIdToXBeeAddr(nodeId));
    time_t epoch = time(NULL);
    struct tm *tm = localtime(&epoch);

    uint8_t dummy[XBEE_ADDRESS_SIZE];
    memset(dummy,0,XBEE_ADDRESS_SIZE);

    std::string timeMessage("set cudate ");
    char tmp[2];
    sprintf(tmp, "%02d", tm->tm_mday);
    timeMessage += std::string(tmp);

    sprintf(tmp, "%02d", tm->tm_mon + 1);
    timeMessage += std::string(tmp);

    sprintf(tmp, "%02d", tm->tm_year + 1900 - 2000);
    timeMessage += std::string(tmp);

    timeMessage += std::string("\n");
    std::cout << "Set cudate message: " << timeMessage << std::endl;

    return XBeeTxMessage((char*) timeMessage.c_str(), dummy, &xbeeAddr[0]);
}

int Gateway::storeDataInDatabase(unsigned int nodeId, std::string sCommand, std::string sData) {
        if(sCommand.compare("curhar") == 0) {
            dbConnection->updateCurrentHarvest(nodeId, boost::lexical_cast<float>(sData));
            return 0;
        }
        if(sCommand.compare("curori") == 0) {
            std::vector<std::string> partedString (split(sData));
            dbConnection->addCurrentOrientation(nodeId, boost::lexical_cast<float>(partedString[0]),
                    boost::lexical_cast<float>(partedString[1]));
            return 0;
        }
        if(sCommand.compare("solori") == 0) {
            std::vector<std::string> partedString (split(sData));
            dbConnection->addSoltrackOrientation(nodeId, boost::lexical_cast<float>(partedString[0]),
                    boost::lexical_cast<float>(partedString[1]));
            return 0;
        }
        if(sCommand.compare("curtmp") == 0) {
            return 0;
        }
        if(sCommand.compare("curgps") == 0) {
            std::vector<std::string> partedString (split(sData));
            dbConnection->updateCurrentGPS(nodeId, boost::lexical_cast<float>(partedString[0]),
                    boost::lexical_cast<float>(partedString[1]));
            return 0;
        }
        if(sCommand.compare("solerr") == 0) {
            std::vector<std::string> partedString (split(sData));
            dbConnection->addSoltrackError(nodeId, boost::lexical_cast<float>(partedString[0]),
                    boost::lexical_cast<float>(partedString[1]));
            return 0;
        }
        if(sCommand.compare("cudate") == 0) {
            return 0;
        }
        if(sCommand.compare("cutime") == 0) {
            return 0;
        }

        return 0;
}

}
