#ifndef DATABASE_H
#define DATABASE_H

#include <stdio.h>
#include <iostream>
#include <vector>
#include <string.h>

#include "../gateway_mysql/gateway_mysql.h"
#include "app_info.h"

class Database {
public:
    Database();
    virtual ~Database();

    int addNode(unsigned int NodeID, std::vector<uint8_t> xbeeAddr);
    int addNodeToField(unsigned int databaseNodeID, unsigned int field);
    int removeNode(std::vector<uint8_t> xbeeAddr);
    int updateCurrentHarvest(unsigned int NodeID, float currentHarvest);
    int updateCurrentGPS(unsigned int NodeID, float Latitude, float longitude);
    int addCurrentOrientation(unsigned int NodeID, float horizontalOrientation, float verticalOrientation);
    int addSoltrackOrientation(unsigned int NodeID, float horizontalOrientation, float verticalOrientation);
    int addSoltrackError(unsigned int NodeID, float horizontalOrientation, float verticalOrientation);
    int logGatewayMessage(unsigned int NodeID, std::string logMessage);
    int getNodeID(unsigned int nodeNameId);
    int getNodeID(std::vector<uint8_t> xbeeAddress);


private:
    GatewayMYSQL *sqlConnection;
};

#endif // DATABASE_H
