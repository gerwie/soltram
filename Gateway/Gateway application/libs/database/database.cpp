#include "database.h"
#include <boost/lexical_cast.hpp>
#include <mysql/mysql.h>

Database::Database() {
    // TODO Auto-generated constructor stub
    std::cout << "Initializing MySQL connection" << std::endl;
    sqlConnection = new GatewayMYSQL(DATABASE_HOST, DATABASE_PORT, DATABASE_USER,
                                DATABASE_PASS, DATABASE_NAME);
//    sqlConnection->performQuery(std::string("DELETE FROM `nodes`;"));

//    std::vector<uint8_t> tempvekkie {0x00, 0x13, 0xA2, 0x00, 0x40, 0xF9, 0x1F, 0xB0};
//    addNode(8, tempvekkie);
//    updateCurrentGPS(8, 82.7778, 5.8886);
//    addCurrentOrientation(8, 223.5, 55.6);
//    addSoltrackOrientation(8, 2.2, 553.2);
//    addSoltrackError(8, 3.1, 5.6);
//    updateCurrentHarvest(8, 888.1);

    //removeNode(tempvekkie);
}

Database::~Database() {
    // TODO Auto-generated destructor stub
    delete sqlConnection;
}

int Database::addNode(unsigned int NodeID, std::vector<uint8_t> xbeeAddr) {
    uint64_t uiXBeeAddress = 0;
    auto iVecIndex = xbeeAddr.begin();
    for(int i = 56; i >= 0; i -= 8) {
        uiXBeeAddress |= (uint64_t) *iVecIndex++ << i;
    }

    if(int nodeDatabaseID = getNodeID(xbeeAddr) == -1) {	// Insert new node
        std::cout << "Inserting node in database" << std::endl;
        std::string sNodeName {"'Node " + boost::lexical_cast<std::string>(NodeID) + "'"};

        std::cout << "Node name: "<< sNodeName  <<"xbeeAdress: " << boost::lexical_cast<std::string>(uiXBeeAddress) << std::endl;

        std::string sQuery("INSERT INTO `nodes` (`node_name`, `latitude`, `longitude`, `address`) VALUES(");
        sQuery += sNodeName;
        sQuery += std::string(",NULL,NULL,");
        sQuery += boost::lexical_cast<std::string>(uiXBeeAddress);
        sQuery += std::string(");");

        std::cout << "add new node query:" << std::endl
                  << sQuery << std::endl;

        sqlConnection->performQuery(sQuery);
    } else {	// Update node name
        std::cout << "Updating node in database" << std::endl;
        std::string sNodeName {"'Node " + boost::lexical_cast<std::string>(NodeID) + "'"};

        std::string sQuery("UPDATE `nodes` SET `node_name`=" + sNodeName + " WHERE `id`='" +
                boost::lexical_cast<std::string>(nodeDatabaseID) + "'");

        sqlConnection->performQuery(sQuery);
    }
    addNodeToField(getNodeID(NodeID), 1);

    return 0;
}

int Database::removeNode(std::vector<uint8_t> xbeeAddr) {
    uint64_t uiXBeeAddress = 0;
    auto iVecIndex = xbeeAddr.begin();
    for(int i = 56; i >= 0; i -= 8) {
        uiXBeeAddress |= (uint64_t) *iVecIndex++ << i;
    }
    sqlConnection->performQuery(std::string("DELETE FROM `nodes` WHERE `address`='" +
                                boost::lexical_cast<std::string>(uiXBeeAddress) + "'"));
    return 0;
}

int Database::updateCurrentHarvest(unsigned int NodeID, float currentHarvest) {
    std::string sDatabaseNodeID(boost::lexical_cast<std::string>(getNodeID(NodeID)));
    std::string sCurrentHarvest(boost::lexical_cast<std::string>(currentHarvest));

    std::string sQuery("INSERT INTO energy_log (`node_id`, `output`) VALUES('" + sDatabaseNodeID + "','" +
                        sCurrentHarvest + "')");
    sqlConnection->performQuery(sQuery);
    return 0;
}

int Database::updateCurrentGPS(unsigned int NodeID, float Latitude,
        float longitude) {
    int iDatabaseNodeID = getNodeID(NodeID);
    if(iDatabaseNodeID != -1) {
        std::string sLatitude("`latitude`='" + boost::lexical_cast<std::string>(Latitude) + "'");
        std::string slongitude("`longitude`='" + boost::lexical_cast<std::string>(longitude) + "'");

        std::string sQuery("UPDATE `nodes` SET " + sLatitude + "," + slongitude + " WHERE `id`='" + boost::lexical_cast<std::string>(iDatabaseNodeID) + "'");

        sqlConnection->performQuery(sQuery);
        std::cout << "Updating GPS for node " << iDatabaseNodeID << std::endl;
        return 1;
    }
    std::cout << "Failed GPS Update, Node ID not found!" << std::endl;
    return 0;
}

int Database::addCurrentOrientation(unsigned int NodeID,
        float horizontalOrientation, float verticalOrientation) {
    std::string sDatabaseNodeID(boost::lexical_cast<std::string>(getNodeID(NodeID)));
    std::string sHorOri(boost::lexical_cast<std::string>(horizontalOrientation));
    std::string sVerOri(boost::lexical_cast<std::string>(verticalOrientation));
    std::string sMessage("Orientation changed");

    std::string sQuery("INSERT INTO node_log (`node_id`, `hor_axis`, `ver_axis`, `message`) VALUES('" + sDatabaseNodeID + "','" +
                        sHorOri + "','" + sVerOri + "','" + sMessage + "')");
    sqlConnection->performQuery(sQuery);
    return 0;
}

int Database::addSoltrackOrientation(unsigned int NodeID,
        float horizontalOrientation, float verticalOrientation) {
    std::string sDatabaseNodeID(boost::lexical_cast<std::string>(getNodeID(NodeID)));
    std::string sHorOri(boost::lexical_cast<std::string>(horizontalOrientation));
    std::string sVerOri(boost::lexical_cast<std::string>(verticalOrientation));
    std::string sMessage("Calculated orientation: " + sHorOri + " " + sVerOri);

    std::string sQuery("INSERT INTO node_log (`node_id`, `hor_axis`, `ver_axis`, `message`) VALUES('" + sDatabaseNodeID + "','" +
                        sHorOri + "','" + sVerOri + "','" + sMessage + "')");
    sqlConnection->performQuery(sQuery);
    return 0;
}

int Database::addSoltrackError(unsigned int NodeID, float horizontalOrientation, float verticalOrientation) {
    std::string sDatabaseNodeID(boost::lexical_cast<std::string>(getNodeID(NodeID)));
    std::string sHorOri(boost::lexical_cast<std::string>(horizontalOrientation));
    std::string sVerOri(boost::lexical_cast<std::string>(verticalOrientation));
    std::string sMessage("Orientation error: " + sHorOri + " " + sVerOri);

    std::string sQuery("INSERT INTO node_log (`node_id`, `hor_axis`, `ver_axis`, `message`) VALUES('" + sDatabaseNodeID + "','" +
                        sHorOri + "','" + sVerOri + "','" + sMessage + "')");
    sqlConnection->performQuery(sQuery);
    return 0;
}

int Database::logGatewayMessage(unsigned int NodeID, std::string logMessage) {
    int iDbNodeID = getNodeID(NodeID);
    if(iDbNodeID != -1) {
        std::string sQuery("INSERT INTO `gateway_message_log` (`node_id`, `message`) VALUES ('");
        sQuery += boost::lexical_cast<std::string>(iDbNodeID);
        sQuery += std::string("','");
        sQuery += logMessage;
        sQuery += std::string("')");
        sqlConnection->performQuery(sQuery);

        return 0;
    }

    return 1;
}

int Database::getNodeID(unsigned int nodeNameId) {
    std::string ssQuery = std::string("SELECT `id` FROM nodes WHERE `node_name`='Node ");
    ssQuery += boost::lexical_cast<std::string>(nodeNameId);
    ssQuery += std::string("'");

    MYSQL_RES * sqlResult;
    MYSQL_ROW row;
    sqlResult = sqlConnection->performQuery(ssQuery);

    if(sqlResult == nullptr)
        return -1;

    if(mysql_num_rows(sqlResult)) {
        row =  mysql_fetch_row(sqlResult);
        return boost::lexical_cast<int>(row[0]);
    }

    return -1;
}

int Database::addNodeToField(unsigned int databaseNodeID, unsigned int field) {
    std::string sDatabaseNodeID {boost::lexical_cast<std::string>(databaseNodeID)};
    std::string sFieldID(boost::lexical_cast<std::string>(field));
    std::string sQuery("SELECT `id` FROM node_config WHERE `node_id`='" + sDatabaseNodeID + "'");

    MYSQL_RES * sqlResult;
    sqlResult = sqlConnection->performQuery(sQuery);

    if(sqlResult == nullptr) {
        return -1;
    }

    if(mysql_num_rows(sqlResult)) {
        std::cout << "Updating record in node_config" << std::endl;
        std::string sUpdateQuery("UPDATE `node_config` SET `field_id`='" + sFieldID + "' WHERE `node_id`='" + sDatabaseNodeID + "'");
        sqlConnection->performQuery(sUpdateQuery);
    } else {
        std::cout << "Inserting into node_config " << std::endl;
        std::string sInsertQuery("INSERT INTO node_config (`node_id`, `field_id`) VALUES('" + sDatabaseNodeID + "','" + sFieldID + "')");
        sqlConnection->performQuery(sInsertQuery);
    }

    return 0;
}

int Database::getNodeID(std::vector<uint8_t> xbeeAddress) {
    uint64_t uiXBeeAddress = 0;
    auto iVecIndex = xbeeAddress.begin();
    for(int i = 56; i >= 0; i -= 8) {
        uiXBeeAddress |= (uint64_t) *iVecIndex++ << i;
    }
    std::string ssQuery = std::string("SELECT `id` FROM nodes WHERE `address`='");
    ssQuery += boost::lexical_cast<std::string>(uiXBeeAddress);
    ssQuery += std::string("'");

    MYSQL_RES * sqlResult;
    MYSQL_ROW row;
    sqlResult = sqlConnection->performQuery(ssQuery);

    if(sqlResult == nullptr)
        return -1;

    if(mysql_num_rows(sqlResult)) {
        row =  mysql_fetch_row(sqlResult);
        return boost::lexical_cast<int>(row[0]);
    }

    return -1;
}
