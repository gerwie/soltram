#ifndef MQTTCLIENT_H
#define MQTTCLIENT_H

#include <iostream>

#include "config.h"
#include "libs/gateway_class/gateway_class.h"
#include "libs/gateway_message/gateway_message.h"

#include "mosquittopp.h"
#include "semaphore.h"
#include <queue>

const std::string COMMAND_TOPIC("Command");

class MQTTClient : public mosqpp::mosquittopp {
public:
    MQTTClient(const std::string &, const std::string &, std::queue<Gateway::MQTTMessage> &, sem_t &);
    ~MQTTClient();

    void publishMqttMessage(Gateway::MQTTMessage &msg);
    std::string MQTT_Base_Topic;

protected:
    virtual void on_connect(int) override;
    virtual void on_disconnect(int) override;
    virtual void on_publish(int) override;
    virtual void on_message(const struct mosquitto_message *) override;
    virtual void on_subscribe(int, int, const int *) override;
    virtual void on_unsubscribe(int) override;
    virtual void on_log(int, const char *) override;
    virtual void on_error() override;

private:
    sem_t &SEM_TO_GW;
    std::queue<Gateway::MQTTMessage> &Q_TO_GW;
};

#endif // MQTTCLIENT_H
