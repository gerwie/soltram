#include "mqtt_client.h"

MQTTClient::MQTTClient(const std::string & name,
                       const std::string & mqtt_base_topic,
                       std::queue<Gateway::MQTTMessage> &queue,
                       sem_t &semaphore)
                     : mosquittopp{(name).c_str()},
                                    MQTT_Base_Topic(mqtt_base_topic),
                                    SEM_TO_GW(semaphore),
                                    Q_TO_GW(queue)
{}

MQTTClient::~MQTTClient() {
    disconnect();
}

void MQTTClient::on_connect(int rc) {
    std::cout << "[MQTT]    ";
    switch (rc) {
        case 0: {
            std::cout << "MQTTClient connected." << std::endl;
            std::string subTopic(MQTT_Base_Topic);
            subTopic += ("/" + COMMAND_TOPIC + "/#");
            subscribe(NULL, subTopic.c_str(), MQTT_QoS_1);
            std::cout << "[MQTT]    Subscribed to topic: " << subTopic << std::endl;
            break;
        }
        case 1:
        std::cout << "MQTTClient failed to connect..." << std::endl;
        std::cout << "Connection refused: Unacceptable protocol version." << std::endl;
        break;
        case 2:
        std::cout << "MQTTClient failed to connect..." << std::endl;
        std::cout << "Connection refused: Identifier rejected." << std::endl;
        break;
        case 3:
        std::cout << "MQTTClient failed to connect..." << std::endl;
        std::cout << "Connection refused: Broker not available." << std::endl;
        break;
        default:
        std::cout << "MQTTClient failed to connect..." << std::endl;
        std::cout << "Undefined error (" << rc << ")" << std::endl;
    }
}

void MQTTClient::on_disconnect(int rc) {
    if (0 == rc) {
        std::cout << "MQTTClient disconnected." << std::endl;
    } else {
        std::cout << "MQTTClient disconnected unexpectedly. (" << rc << ")" << std::endl;
    }
}

void MQTTClient::on_publish(int mid) {
    std::cout << "MQTTClient published a message (ID: " << mid << ")." << std::endl;
}

void MQTTClient::on_message(const struct mosquitto_message * message) {
    std::string m;
    if (message->payload == NULL) {
        m = "(null)";
    } else {
        m = (char*) message->payload;
    }
    std::cout << "[MQTT]    MQTTClient received a message: " << message->topic << " " << m << std::endl;
    //std::cout << "QoS: " << message->qos << ", Retained: " << message->retain << std::endl;
    const std::string msgTopic((char*) message->topic);
    const std::string msgPayload((char*) message->payload);
    sem_wait(&SEM_TO_GW);
    Q_TO_GW.push(Gateway::MQTTMessage(msgTopic, msgPayload));
    sem_post(&SEM_TO_GW);
}

void MQTTClient::on_subscribe(int mid, int qos_count, const int * granted_qos) {
    std::cout << "MQTTClient subscribed (ID: " << mid << ")." << std::endl;
}

void MQTTClient::on_unsubscribe(int mid) {
    std::cout << "MQTTClient unsubscribed (ID: " << mid << ")." << std::endl;
}

void MQTTClient::on_log(int level, const char * message) {
    std::string loglevel;
    std::string m;
    switch (level) {
        case MOSQ_LOG_INFO:
        loglevel = "[INFO]";
        break;
        case MOSQ_LOG_NOTICE:
        loglevel = "[NOTICE]";
        break;
        case MOSQ_LOG_WARNING:
        loglevel = "[WARNING]";
        break;
        case MOSQ_LOG_ERR:
        loglevel = "[ERROR]";
        break;
        case MOSQ_LOG_DEBUG:
        loglevel = "[DEBUG]";
        break;
        default:
        loglevel = "[LOG]";
    }

    if (message == NULL) {
        m = "(null)";
    } else {
        m = message;
    }
    //std::cout << loglevel << " MQTTClient log: " << message << std::endl;
}

void MQTTClient::on_error() {
}

void MQTTClient::publishMqttMessage(Gateway::MQTTMessage &msg) {
    std::cout << "Publishing to: " << msg.getTopic() << std::endl;
    std::cout << "Message: " << msg.getMessage() << std::endl;
    publish(nullptr, msg.getTopic().c_str(), msg.getMessage().size(), msg.getMessage().c_str(), 1, true);
}
