#include "uart.h"

namespace UART {

int UART::getData (void* buffer, const unsigned int bytes) {
    //std::cout << "[UART] getData()" << std::endl;
    unsigned int bytesRemaining = bytes;
    unsigned int byteOffset = 0;

    //mtx.lock();
    //usleep(250 * bytes);
    while (bytesRemaining > 0) {
        int bytesRead = 0;
        //std::cout << "Reading bytes!" << std::endl;
        bytesRead = read(tty_fd, ((unsigned int*)buffer + byteOffset), bytesRemaining);
        if (bytesRead > 0) {
            //std::cout << "Bytes read = " << bytesRead << std::endl;
            bytesRemaining -= bytesRead;
            byteOffset += bytesRead;
        } else if(bytesRead == 0) {
            //std::cout << "Bytes read = 0; returning -1" << std::endl;
            return -1;
        } else if(bytesRead < 0) {
            //std::cout << "Bytes read = -1; returning -1" << std::endl;
            return -1;
        } else {
            if (11 != errno) {
                std::cout << "Read error: " << errno << std::endl;
                //mtx.unlock();
                std::cout << "getData exit on read error" << std::endl;
                return -1;
            }
        }
        //usleep(250 * bytesRemaining);
    }
    //mtx.unlock();

    return 0;
    std::cout << "getData succeeded" << std::endl;
}

bool UART::is_empty(int fileDescriptor)
{
    if (lseek(fileDescriptor, 0, SEEK_END) == -1) {
        std::cout << "UART FILE NOT EMPTY, ERROR: " << std::endl;
        lseek(fileDescriptor, 0, SEEK_SET);
        return true;
    } else if (lseek(fileDescriptor, 0, SEEK_END)) {
        // file is not empty, go back to the beginning:
        return false;
    } else {
        std::cout << "UART FILE EMPTY" << std::endl;
        return true;
    }
}

UART::UART( std::string& tty ) {
    initSuccess = true;
    c='D';
    tty_fd = open(tty.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (tty_fd == -1) {
        std::cout << "UART: Failed to open device " << tty << std::endl;
        std::cout << "Errno: " << errno << std::endl;
        initSuccess = false;
        return;
    }
    tcgetattr(tty_fd, &config);
    config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
    config.c_oflag = 0;
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
    config.c_cflag &= ~(CSIZE | PARENB);
    config.c_cflag |= CS8;
    config.c_cc[VMIN] = 1;
    config.c_cc[VTIME] = 0;
    cfsetispeed(&config,B38400);
    cfsetospeed(&config,B38400);
    tcsetattr(tty_fd, TCSAFLUSH, &config);
}

UART::~UART() {
    close(tty_fd);
}

int UART::UARTReceive (void* extbuf, unsigned int bufSize) {
    //std::cout << "[UART] UARTReceive()" << std::endl;
    // Layout
    // [0]   Delimiter
    // [1]   Size MSB
    // [2]   Size LSB
    // [3]   Frame Type (0x90)
    // [4]   Source Address MSB
    // ...
    // [11]  Source Address LSB
    // [12]  Reserved
    // [13]  Reserved
    // [14]  Receive options
    // [15]  Frame Data start
    // ...
    // [n]   Frame Data end
    // [n+1] Checksum

    // XBee Data frame
    uint8_t startByte = 0;
    uint16_t frameDataSize = 0;
    unsigned int frameSize = 0;
    //unsigned int messageSize = 0;
    //uint8_t frameType = 0;
    //uint8_t srcAddr[8];
    //uint8_t recvOpts = 0;
    //uint8_t* frameRxData;
    //uint8_t checksum = 0;

    if (getData(&startByte, 1)) {
        return 0;
    }

    if (startByte != XBEE_START_DELIMITER) {
        // This is not an XBee message
        // Also set the 4th element to the received byte
#warning this functions changed so i must be tested
        memcpy((extbuf+4),&startByte,1);
        return -1;
    }
    // We have an XBee message

    // Get the packet sizes
    uint8_t size[2];
    getData(size, 2);
    frameDataSize = (size[0] << 8) + (size[1]);
    frameSize = frameDataSize + 1;
    //messageSize = frameDataSize - 12;
    if (frameSize > bufSize) {
        // Destination buffer size is too small
        return -2;
    }

    // Copy everything to the local buffer
    uint8_t* buf = (uint8_t*) malloc(frameSize + 3);
    memset(buf,0,frameSize);
    buf[0] = startByte;
    memcpy(&buf[1],size,2);
    getData(&buf[3], frameSize);

    // Copy the local buffer to the external buffer
    memset(extbuf,0,frameSize + 3);
    memcpy(extbuf,buf,frameSize + 3);

    // Clean up, and return the frame size
    free(buf);
    return frameSize + 3;
}

int UART::UARTSend (void* buf, unsigned int size) {
    std::cout << "[UART] UARTSend()" << std::endl;
    /*
    char* tmpbuf = (char*) calloc(size,1);
    memset(tmpbuf,0,size);
    memcpy(tmpbuf,buf,size);
    */
    unsigned int bytesRemaining = size;
    unsigned int bytesWritten = 0;

    while (bytesRemaining > 0) {
        int t = 0;
        t = write(tty_fd, ((unsigned int*)buf + bytesWritten), bytesRemaining);
        if (t >= 0) {
            bytesRemaining -= t;
            bytesWritten += t;
        } else {
            std::cout << "Error writing to serial port." << std::endl;
            mtx.unlock();
            return -1;
        }
        //usleep(t * bytesWritten);
    }
    return bytesWritten;
}

int UART::UARTSend (std::string msg, uint8_t* dst_addr) {
    uint8_t dst[ADDRESS_SIZE];
    memset(dst,0,ADDRESS_SIZE);
    memcpy(dst,dst_addr,ADDRESS_SIZE);

    uint8_t dummy[ADDRESS_SIZE];
    memset(dummy,0,ADDRESS_SIZE);

    XBeeTxMessage m ((char*)msg.c_str(),nullptr, dst);

    UARTSend(m);
    return 0;
}

int UART::UARTSend (XBeeTxMessage &msg) {
    write(tty_fd, msg.XBeePacket, msg.XBeePacketSize);
    return 0;
}

}
