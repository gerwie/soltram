#ifndef UART_H
#define UART_H

#include <iostream>
#include <fcntl.h>
#include <cstdio>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <mutex>

#include "config.h"
#include "libs/xbee_dm/xbee_dm.h"

#define UART_ERR_NOTREADY 0x01
#define UART_ERR_WRITEFAIL 0x02
#define UART_ERR_READFAIL 0x03

namespace UART {

class UART {
public:
    UART  ( std::string& );
    ~UART ();

    int UARTReceive (void*, unsigned int);
    int UARTSend (void*, unsigned int);
    int UARTSend (std::string, uint8_t*);
    int UARTSend (XBeeTxMessage &);

    bool initSuccess;

private:
    int getData (void*, const unsigned int);
    bool is_empty(int fileDescriptor);

    struct termios config;
    int tty_fd;
    unsigned char c;

    std::mutex mtx;
};

}

#endif // UART_H
