#ifndef GATEWAYMESSAGE_H
#define GATEWAYMESSAGE_H


#include <string>
#include <cstring>

#define XBEE_ADDRESS_SIZE (unsigned int) 8

namespace Gateway {

class MQTTMessage {
    std::string topic;
    std::string content;

    public:
    MQTTMessage();
    MQTTMessage(const std::string&, const std::string&);
    MQTTMessage(const MQTTMessage &cpInstance);
    ~MQTTMessage();

    std::string getTopic (void);
    std::string getMessage (void);
};

class XBeeMessage {
    std::string content;
    uint8_t src_addr[XBEE_ADDRESS_SIZE];
    uint8_t dest_addr[XBEE_ADDRESS_SIZE];

    public:
    XBeeMessage (const char*, const uint8_t*, const uint8_t*);
    ~XBeeMessage ();

    std::string getMessage (void) const;
    int getSourceAddress (uint8_t*) const;
    int getDestinationAddress (uint8_t*) const;
};

}

#endif // GATEWAYMESSAGE_H
