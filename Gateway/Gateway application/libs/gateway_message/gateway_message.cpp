#include "gateway_message.h"

namespace Gateway {

// MQTTMessage class
MQTTMessage::MQTTMessage ()
    : topic(""), content("")
{
}

MQTTMessage::MQTTMessage (const std::string& top, const std::string& msg)
    : topic(std::string(top)), content(std::string(msg))
{
}

MQTTMessage::MQTTMessage(const MQTTMessage &cpInstance) {
    topic = cpInstance.topic;
    content = cpInstance.content;
}

MQTTMessage::~MQTTMessage () {
}

std::string MQTTMessage::getTopic (void) {
    return topic;
}

std::string MQTTMessage::getMessage (void) {
    return content;
}

// XBeeMessage class
XBeeMessage::XBeeMessage(const char* msg, const uint8_t* src, const uint8_t* dest)
    : content(std::string(msg))
{
    memset(src_addr,0,XBEE_ADDRESS_SIZE);
    memset(dest_addr,0,XBEE_ADDRESS_SIZE);
    memcpy(src_addr, src, XBEE_ADDRESS_SIZE);
    memcpy(dest_addr, dest, XBEE_ADDRESS_SIZE);
}

XBeeMessage::~XBeeMessage() {
}

std::string XBeeMessage::getMessage (void) const {
    return content;
}

int XBeeMessage::getSourceAddress (uint8_t* srcptr) const {
    memcpy(srcptr, src_addr, XBEE_ADDRESS_SIZE);
    return 0;
}

int XBeeMessage::getDestinationAddress (uint8_t* dstptr) const {
    memcpy(dstptr, dest_addr, XBEE_ADDRESS_SIZE);
    return 0;
}

}
