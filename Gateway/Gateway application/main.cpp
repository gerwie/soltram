#include "main.h"
#include "libs/gateway_message/gateway_message.h"
#include "libs/xbee_dm/xbee_dm.h"
#include "libs/database/database.h"

#include "semaphore.h"
#include <queue>
#include <pthread.h>


int MQTT_Thread_State = THREAD_STATE_UNINIT;
int UART_Thread_State = THREAD_STATE_UNINIT;
int Gateway_Thread_State = THREAD_STATE_UNINIT;

int MQTT_Thread_Signal = THREAD_SIG_NONE;
int UART_Thread_Signal = THREAD_SIG_NONE;
int Gateway_Thread_Signal = THREAD_SIG_NONE;

sem_t SEM_MQTT_TO_GW, SEM_GW_TO_UART, SEM_GW_TO_MQTT, SEM_UART_TO_GW;
std::queue<Gateway::MQTTMessage> Q_MQTT_TO_GW, Q_GW_TO_MQTT;
std::queue<XBeeRxMessage> Q_UART_TO_GW;
std::queue<XBeeTxMessage> Q_GW_TO_UART;

void (*old_sigint) (int);

int main (int argc, char *argv[], char *env[])
{
    std::cout << "Starting Soltram Gateway" << std::endl;

    if ((old_sigint = signal(SIGINT, sigint_handler)) == SIG_ERR) {
        std::cout << "Error while setting interrupt handler." << std::endl;
        return 1;
    }

    std::string client {"micromesh SOLTRAM Gateway"};
    std::string host;
    int port;
    std::string topic;


    if (argc >= 4) {
        host = std::string(argv[1]);
        port = atoi(argv[2]);
        topic = std::string(argv[3]);
    } else {
        std::cout << "Usage: Gateway <host> <port> <MQTT Topic>." << std::endl << std::endl;
        return 1;
    }

    // Initialize Mosquitto library
    mosqpp::lib_init();
    if(sem_init(&SEM_GW_TO_MQTT, 1, 1) == -1) {
        std::cerr << "Failed to initialize semaphore!" << std::endl;
        return 1;
    }
    if(sem_init(&SEM_GW_TO_UART, 1, 1) == -1) {
        std::cerr << "Failed to initialize semaphore!" << std::endl;
        return 1;
    }
    if(sem_init(&SEM_MQTT_TO_GW, 1, 1) == -1) {
        std::cerr << "Failed to initialize semaphore!" << std::endl;
        return 1;
    }
    if(sem_init(&SEM_UART_TO_GW, 1, 1) == -1) {
        std::cerr << "Failed to initialize semaphore!" << std::endl;
        return 1;
    }

    // Display version information
    std::cout << "[APP INFO] Intelligent Solutions - " << VERSION_NAME << std::endl << std::endl;
    std::cout << "[APP INFO] MySQL library version: " << mysql_get_client_info() << std::endl;

    // Start the threads
    std::thread MQTT_thread (	MQTTThread,
                                std::ref(MQTT_Thread_State),
                                std::ref(MQTT_Thread_Signal),
                                client,
                                host,
                                port,
                                (topic));
    while(MQTT_Thread_State != THREAD_STATE_RUNNING)
    {

    }

    std::thread UART_thread ( UARTThread,
                              std::ref(UART_Thread_State),
                              std::ref(UART_Thread_Signal));

    std::thread Gateway_thread (	GatewayThread,
                                    std::ref(Gateway_Thread_State),
                                    std::ref(Gateway_Thread_Signal),
                                    std::ref(topic) );

    MQTT_thread.detach();
    UART_thread.detach();
    Gateway_thread.detach();

    // Go into an endless loop
    while(1) {

        usleep(MAIN_USLEEP);
    }

    // Display end of program
    std::cout << "[APP INFO] --- END ---" << std::endl;

    return 0;
} // main

void sigint_handler (int sig) {
    std::cout << "[APP INFO] Gateway caught interrupt signal (" << sig << ")." << std::endl;
    std::cout << "[APP INFO] Shutting down threads..." << std::endl;
    signal(SIGINT, old_sigint);
    int i = 0;
    bool cleanStop = false;
    MQTT_Thread_Signal = THREAD_SIG_STOP;
    UART_Thread_Signal = THREAD_SIG_STOP;
    Gateway_Thread_Signal = THREAD_SIG_STOP;
    while (i<THREAD_STOP_WAITTIME) {
        if (MQTT_Thread_State == THREAD_STATE_STOPPED &&
                UART_Thread_State == THREAD_STATE_STOPPED &&
                Gateway_Thread_State == THREAD_STATE_STOPPED) {
            cleanStop = true;
            i = THREAD_STOP_WAITTIME + 1;
        } else {
            sleep(1);
            ++i;
        }
    }
    if (!cleanStop) {
        std::cout << "Could not stop threads in " << THREAD_STOP_WAITTIME << " seconds. Exitting forcefully." << std::endl;
    }
    exit(0);
}

void MQTTThread (int & state,
                 int & sig,
                 std::string clientName,
                 std::string hostName,
                 int port,
                 std::string topic)
{
    state = THREAD_STATE_CREATED;

    std::cout << "[MQTT]    Connecting to MQTT broker." << std::endl;
    MQTTClient client{clientName.c_str(), topic, Q_MQTT_TO_GW, SEM_MQTT_TO_GW};
    client.threaded_set(true);
    client.connect(hostName.c_str(), port, 60);

    client.publish(nullptr, "SOLTRAM/", 15, "testing gateway",0);

    // Loop
    state = THREAD_STATE_RUNNING;
    int run = 1;
    std::cout << "[MQTT]    MQTT Thread listening for messages..." << std::endl;

    while(run) {
        if (sig != THREAD_SIG_NONE) {
            run = 0;
        } else {
            int rc = client.loop();
            if(rc) {
                client.reconnect();
            }

            sem_wait(&SEM_GW_TO_MQTT);
            if(!Q_GW_TO_MQTT.empty())
            {
                std::cout << "[MQTT]    Publishing message to mqtt!" << std::endl;
                client.publishMqttMessage(Q_GW_TO_MQTT.front());
                Q_GW_TO_MQTT.pop();
            }
            sem_post(&SEM_GW_TO_MQTT);

            usleep(MQTT_THREAD_USLEEP);
        }
    }

    client.disconnect();
    std::cout << "[MQTT]    Thread stopped." << std::endl;
    state = THREAD_STATE_STOPPED;

    return;
}

void UARTThread (int & state, int & sig)
{
    state = THREAD_STATE_CREATED;
    std::cout << "[UART]    Thread started." << std::endl;

    state = THREAD_STATE_RUNNING;
    int run = 1;
    std::string p{"/dev/ttyUSB0"};
    UART::UART ser(p);
    if(!ser.initSuccess) {
        std::cout << "[UART]    Serial interface initialisation failed!" << std::endl;
        return;
    }

    while (run) {
        if (sig == THREAD_SIG_NONE) {

            unsigned int buffersize = (65535 + 4);
            uint8_t* buffer;
            int recvSize = 0;

            buffer = (uint8_t*) calloc(buffersize,1);
            recvSize = ser.UARTReceive((void*) buffer, buffersize);

            if (recvSize == -1) {
                std::cout << "[UART]     Received data, but not an XBee message. (" << std::hex << ((uint8_t*) (buffer+4)) << std::dec << ")" << std::endl << std::endl;
            } else if (recvSize == 0) {
                // Do nothing
            } else {
                XBeeRxMessage rxm(buffer, recvSize);

                if(rxm.bValid) {
                    std::cout << "[UART]    Received a message" << std::endl;
                    sem_wait(&SEM_UART_TO_GW);
                    Q_UART_TO_GW.push(rxm);
                    sem_post(&SEM_UART_TO_GW);
                } else {
                    //std::cout << "[UART]    ERROR: Received an invalid XBEE Message" << std::endl;
                }
            }

            if(!sem_trywait(&SEM_GW_TO_UART)) {
                if(!Q_GW_TO_UART.empty()) {

                    if(Q_GW_TO_UART.front().bValid) {
                        std::cout << "[UART]    Sending message to XBEE!" << std::endl;
                        ser.UARTSend(Q_GW_TO_UART.front());
                    }
                    Q_GW_TO_UART.pop();
                }
                sem_post(&SEM_GW_TO_UART);
            }


            free(buffer);
            usleep(UART_THREAD_USLEEP);
        } else {
            run = 0;
        }
    }
    std::cout << "[UART]    Thread stopped." << std::endl;
    state = THREAD_STATE_STOPPED;

    return;
}

void GatewayThread (int & state, int & sig, std::string &mqttBaseTopic)
{
    state = THREAD_STATE_CREATED;
    std::cout << "[Gateway] Thread started." << std::endl;

    Gateway::Gateway gw(mqttBaseTopic);
    std::queue<XBeeTxMessage> Q_INTERNAL_TO_UART;

    std::cout << "[Gateway] Initialized and running." << std::endl;

    state = THREAD_STATE_RUNNING;
    int run = 1;
    while (run) {
        if (sig == THREAD_SIG_NONE) {

            if(!sem_trywait(&SEM_UART_TO_GW)) {
                if(!Q_UART_TO_GW.empty()) {
                    unsigned int registeredNodeId = gw.handleRegisterCommand(Q_UART_TO_GW.front());
                    if(!registeredNodeId) {
                        if(!sem_trywait(&SEM_GW_TO_MQTT)) {
                            std::cout << "[GATEWAY] Routing message from UART to MQTT" << std::endl;
                            Q_GW_TO_MQTT.push(gw.ParseXBeeRxMessage(Q_UART_TO_GW.front()));
                            Q_UART_TO_GW.pop();
                            sem_post(&SEM_GW_TO_MQTT);
                        }
                    } else {
                        Q_INTERNAL_TO_UART.push(gw.createRegisterAckMessage(registeredNodeId));
                        Q_INTERNAL_TO_UART.push(gw.createTimeSyncMessage(registeredNodeId));
                        Q_INTERNAL_TO_UART.push(gw.createDateSyncMessage(registeredNodeId));
                        Q_UART_TO_GW.pop();
                    }
                }
                sem_post(&SEM_UART_TO_GW);
            }

            if(!Q_INTERNAL_TO_UART.empty()) {
                if(!sem_trywait(&SEM_GW_TO_UART)) {
                    Q_GW_TO_UART.push(Q_INTERNAL_TO_UART.front());
                    Q_INTERNAL_TO_UART.pop();
                    sem_post(&SEM_GW_TO_UART);
                }
            }

            if(!sem_trywait(&SEM_MQTT_TO_GW)) {
                if(!Q_MQTT_TO_GW.empty()) {
                    if(!sem_trywait(&SEM_GW_TO_UART)) {
                        std::cout << "[GATEWAY] Routing message from MQTT to UART" << std::endl;
                        Q_GW_TO_UART.push(gw.ParseMQTTMessage(Q_MQTT_TO_GW.front()));
                        Q_MQTT_TO_GW.pop();
                        sem_post(&SEM_GW_TO_UART);
                    }
                }
                sem_post(&SEM_MQTT_TO_GW);
            }

            usleep(GATEWAY_THREAD_USLEEP);
        } else {
            run = 0;
        }
    }
    std::cout << "[Gateway] Thread stopped." << std::endl;
    state = THREAD_STATE_STOPPED;

    return;
}

