#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>
#include <mutex>
#include <unistd.h>
#include <csignal>

#include "app_info.h"
#include "config.h"
#include "libs/mqtt_client/mqtt_client.h"
#include "libs/uart/uart.h"
#include "libs/gateway_class/gateway_class.h"

#define THREAD_STOP_WAITTIME 5

#define THREAD_STATE_UNINIT 0
#define THREAD_STATE_CREATED 1
#define THREAD_STATE_RUNNING 2
#define THREAD_STATE_STOPPED 3

#define THREAD_SIG_NONE 0
#define THREAD_SIG_STOP 1

void sigint_handler (int);

void MQTTThread (int &,
                 int &,
                 const std::string,
                 const std::string,
                 const int,
                 const std::string);
void UARTThread (int &,
                 int &);
void GatewayThread (int &,
                    int &,
                    std::string &);



#endif // MAIN_H
