#ifndef CONFIG_H
#define CONFIG_H

#include <string>

#define MAIN_FREQUENCY 20
#define MQTT_THREAD_FREQUENCY 20
#define UART_THREAD_FREQUENCY 20
#define GATEWAY_THREAD_FREQUENCY 20

#define MAIN_USLEEP (unsigned int) (1000000/MAIN_FREQUENCY)
#define MQTT_THREAD_USLEEP (unsigned int) (1000000/MQTT_THREAD_FREQUENCY)
#define UART_THREAD_USLEEP (unsigned int) (1000000/UART_THREAD_FREQUENCY)
#define GATEWAY_THREAD_USLEEP (unsigned int) (1000000/GATEWAY_THREAD_FREQUENCY)

//const char* getBBBid();

//const std::string BBB_ID {getBBBid()};

const int MQTT_KEEP_ALIVE {60};
const int MQTT_QoS_0 {0};
const int MQTT_QoS_1 {1};
const int MQTT_QoS_2 {2};
const bool MQTT_RETAIN_OFF {false};
const bool MQTT_RETAIN_ON {true};
const std::string MQTT_BROKER {"mqtt.soltram.info"};
const std::string MQTT_TOPIC_ROOT {"SOLTRAM/Field 01/Node 000002"};
const std::string UART_FILE {"/dev/ttyS0"};
const int UART_PORT {0};

#endif // CONFIG_H
