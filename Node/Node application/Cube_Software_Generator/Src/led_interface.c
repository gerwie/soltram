/**
 * @file led_interface.c
 * @brief Implementation of led_interface.h
 * @author Brian van Zwam + using existing code made by Tim van der Staaij
 */
 
#include <stdbool.h>
#include "led_interface.h"
#include "stm32l1xx_hal.h"

volatile led_indicator_t activeIndicator;
//volatile static bool errorLedActive = false;

static void Led_OutputColor(led_indicator_t indicator);

/*use this function for setting the LEDS*/
void Led_SetIndicator(led_indicator_t indicator)
{
    if (indicator != LED_HARDFAULT) {
					Led_OutputColor(indicator);
					activeIndicator = indicator;}
		else {
					indicator = LED_HARDFAULT;
					Led_OutputColor(indicator);
					activeIndicator = indicator;
		}
}

void Led_OutputColor(led_indicator_t indicator)
{
	uint16_t enabledPins = 0, disabledPins = 0;
	switch(indicator)	{
		case LED_DISABLED:
				disabledPins =( LED_RED_PIN |LED_BLUE_PIN |LED_GREEN_PIN);
			break;
		case LED_RUNNING_FINE:
				disabledPins =( LED_RED_PIN |LED_BLUE_PIN);
				enabledPins = LED_GREEN_PIN;
			break;
		
		case LED_HARDFAULT:
				disabledPins =(LED_BLUE_PIN |LED_GREEN_PIN);
				enabledPins = LED_RED_PIN;
			break;
		case LED_BLUE:
			disabledPins =( LED_RED_PIN|LED_GREEN_PIN);
			enabledPins = LED_BLUE_PIN;
			break;
		default:
			disabledPins =( LED_RED_PIN |LED_BLUE_PIN |LED_GREEN_PIN);
			break; 
	}
	/*write to the pins to set or rest them*/
//		HAL_GPIO_WritePin(LED_COMMON_PORT, LED_COMMON_PIN, GPIO_PIN_SET);
//		HAL_GPIO_WritePin(LED_COLOR_PORT, LED_GREEN_PIN, GPIO_PIN_SET);
//		HAL_GPIO_WritePin(LED_COLOR_PORT, LED_RED_PIN, GPIO_PIN_SET);	
//		HAL_GPIO_WritePin(LED_COLOR_PORT, LED_BLUE, GPIO_PIN_RESET);
	
		
		HAL_GPIO_WritePin(LED_COMMON_PORT, LED_COMMON_PIN, 	GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED_COLOR_PORT, disabledPins, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED_COLOR_PORT, enabledPins, GPIO_PIN_RESET);
}


