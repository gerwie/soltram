/**
* @file main.cpp
* @brief Entry point and RTOS resources
*        for SOLTRAM project
* @author �aron Beentjes
* @author Richard Berkvens
* @author Lucas Groenen
* @author Cas Peters
* @author Jan Roorda
* @author Brian van Zwam
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"
#include "XBeeDM.h"
#include "NodeCom.h"
#include "cmsis_os.h"                   // ARM::CMSIS:RTOS:Keil RTX
#include "threads.h"
#include "os_resource.h"
#include "hardware_init.h"
#include "EngineControl.h"
#include "led_interface.h"
#include "RTC_TimeConverter.h"

extern bool bNodeIsRegistered;

int main(void) 
{
	HAL_Init();
  SystemClock_Config();
	
	MX_GPIO_Init();									/* Initialize GPIO pins	             */
	MX_GPIO_Led_Init(); 		  			/* Initialize RGB led                */

	Led_SetIndicator(LED_DISABLED);
		
  //MX_DMA_Init();									/* Initialize DMA                		 */
  MX_ADC_Init();									/* Initialize ADC	(LIGHT)            */
  //MX_CRC_Init();									/* Initialize CRC			               */
  MX_I2C1_Init();									/* Initialize I2C1 (TEMP)            */
  //MX_I2C2_Init();								/* Initialize I2C2 (EXTERN)          */
  MX_RTC_Init();									/* Initialize RTC		                 */
  //MX_SPI1_Init();								/* Initialize SPI1 (XBEE)            */
  //MX_SPI2_Init();								/* Initialize SPI2 (EXTERN)    			 */
  //MX_TIM2_Init();								/* Initialize TIM2_CH1 (EXTERN)      */
  //MX_TIM3_Init();								/* Initialize TIM3_CH1 (EXTERN)      */
	/*MX_TIM4_Init(); Conflicts with PB6: LED output on discovery board*/
	// MX_TIM4_Init();  						/* Initialize TIM_4          */
  //MX_USART2_UART_Init();				/* Initialize UART_2 (EXTERN)        */
  //MX_WWDG_Init();								/* Initialize WWDG		               */
	TMP421_Init();									/* Initialize Temp. sensor           */
	//MX_EEPROM_Init();							/* Initialize RGB EEPROM             */
	MX_GPIO_Power_Init();
	
	msg_XbeeTx = osMessageCreate(osMessageQ(msg_XbeeTx), NULL);
	msg_XbeeRx = osMessageCreate(osMessageQ(msg_XbeeRx), NULL);
	msg_CommandHandler = osMessageCreate(osMessageQ(msg_CommandHandler), NULL);
	
	xbeeTxMemPoolId = osPoolCreate(osPool(mplXbeeTxMemPool));
	xbeeRxMemPoolId = osPoolCreate(osPool(mplXbeeRxMemPool));
	commandHandlerMemPoolId = osPoolCreate(osPool(mplCommandHanlerMemPool));
	
	/* Initialize communication */
	uint8_t centralAddress[ADDRESS_SIZE] = {0x00, 0x13, 0xA2, 0x00, 0x40, 0xF9, 0x1F, 0x9E};
	xbeeCommunication.setCentralAddress(centralAddress);
	
	Led_SetIndicator(LED_BLUE);
	enginecontrol.EngineControlInit();
	Led_SetIndicator(LED_DISABLED);
	
	MX_USART1_UART_Init();					/* Initialize UART_1 (XBEE)          */
	
	/* Init kernel, start scheduler */
	osKernelInitialize();
	osKernelStart();
	osDelay(1000);
	
  /* Initialize threads */
	osThreadCreate(osThread(thrCommandHandler), NULL);
	osThreadCreate(osThread(thrXbeeTransmit), NULL);
	osThreadCreate(osThread(thrProcessRxMessage), NULL);
	
	osDelay(1000);
	
	xbeeCommunication.registerAtGateway();
	int i = 0;
	
	while(!bNodeIsRegistered){	
		if(++i%10 == 0)
				xbeeCommunication.registerAtGateway();
		osDelay(500);
		Led_SetIndicator(LED_RUNNING_FINE);
		osDelay(500);
		Led_SetIndicator(LED_DISABLED);
	}
	
	osThreadCreate(osThread(thrStatusLED), NULL);
	osThreadCreate(osThread(thrOrientation), NULL);
	//osThreadCreate(osThread(thrPhotoMeasurement), NULL);
	osThreadCreate(osThread(thrYield), NULL);
	osThreadCreate(osThread(thrRequestTemperature), NULL);
	
	//osThreadCreate(osThread(thrPowerStatus), NULL);

	osThreadTerminate(osThreadGetId());
	while(1)
	{;}
}


#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
