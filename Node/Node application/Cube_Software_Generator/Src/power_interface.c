/**
* @file power_interface.c
* @brief Implementation of power_interface.h
* @author Brian van Zwam 
*/
 
#include <stdbool.h>
#include "power_interface.h"
#include "stm32l1xx_hal.h"


int LDO_getPin()
{
	return HAL_GPIO_ReadPin(LDO_PORT, LDO_PIN); 
}

void LDO_setPin(bool enable)
{
	 enable ?	HAL_GPIO_WritePin(LDO_PORT, LDO_PIN, GPIO_PIN_SET)
					: HAL_GPIO_WritePin(LDO_PORT, LDO_PIN, GPIO_PIN_RESET);
}

int LTC_getStatus()
{
	return HAL_GPIO_ReadPin(LTC_PORT, LTC_PIN);
}