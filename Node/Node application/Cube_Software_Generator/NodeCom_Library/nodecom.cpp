/**
* @file NodeCom.cpp
*	@brief 
* @author Jan Roorda
*/

#include "nodecom.h"
#include "NodeInformation.h"
#include "NodeSettings.h"
#include "Soltrack.h"
#include "RTC_TimeConverter.h"
#include "EngineControl.h"

extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart1;
extern RTC_HandleTypeDef hrtc;
extern osMessageQId msg_XbeeTx;
extern osPoolId xbeeTxMemPoolId;

extern float currentHarvest;
extern float currentHorAngle;
extern float currentVerAngle;
extern float soltrackHorAngle;
extern float soltrackVerAngle;
extern float currentExternTemperature;
extern float currentInternTemperature;

extern bool bNodeIsRegistered;
extern unsigned int nodeId;

extern EngineControl enginecontrol;

NodeCom::NodeCom()
    : centralAddress(NULL)
{
	
}

NodeCom::~NodeCom()
{
	
}

void NodeCom::publishCurrentHarvest(float curHarvest)
{
    MEM_BLOCK *acCurrentHarvest;
		acCurrentHarvest = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
    sprintf(acCurrentHarvest->acMessage, "curhar %.1f", curHarvest);
		
		if(acCurrentHarvest != NULL) {
			osMessagePut(msg_XbeeTx, (uint32_t) acCurrentHarvest, osWaitForever);
		}
}

void NodeCom::publishCurrentOrientation(float horizontalAngle, float verticalAngle)
{
		MEM_BLOCK *acCurrentOrientation;
    acCurrentOrientation = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
		sprintf(acCurrentOrientation->acMessage, "curori %.1f %.1f", horizontalAngle, verticalAngle);
		
		if(acCurrentOrientation != NULL) {
			osMessagePut(msg_XbeeTx, (uint32_t) acCurrentOrientation, osWaitForever);
		}
}

void NodeCom::publishSoltrackOrientation(float horizontalAngle, float verticalAngle)
{
	MEM_BLOCK *acSolOrientation;
		acSolOrientation = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
    sprintf(acSolOrientation->acMessage, "solori %.1f %.1f", horizontalAngle, verticalAngle);

		if(acSolOrientation != NULL) {
			osMessagePut(msg_XbeeTx, (uint32_t) acSolOrientation, osWaitForever);
		}
}

void NodeCom::publishSolarTemperature(float temperature)
{
    MEM_BLOCK *acCurrentTemp;
		acCurrentTemp = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
    sprintf(acCurrentTemp->acMessage, "curtmp %.1f", temperature);

		if(acCurrentTemp != NULL) {
			osMessagePut(msg_XbeeTx, (uint32_t) acCurrentTemp, osWaitForever);
		}
}

void NodeCom::publishGPS(float latitude, float longtitude)
{
    MEM_BLOCK *acGPS;
		acGPS = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
    sprintf(acGPS->acMessage, "curgps %.5f %.5f", latitude, longtitude);

		if(acGPS != NULL) {
			osMessagePut(msg_XbeeTx, (uint32_t) acGPS, osWaitForever);
		}
}


void NodeCom::publishSoltrackError(float horizontalAngle, float verticalAngle)
{
		MEM_BLOCK *acSoltrackError;
		acSoltrackError = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
    sprintf(acSoltrackError->acMessage, "solerr %.1f %.1f", horizontalAngle, verticalAngle);
		
		osMessagePut(msg_XbeeTx, (uint32_t) acSoltrackError, osWaitForever);
}

void NodeCom::publishCurrentDate(void)
{
	RTC_ConvertedTime &RealtimeClock = RTC_ConvertedTime::getInstance();
	RealtimeClock.GetCurrentTimeAndDate();
	
	MEM_BLOCK *acCurrentDate;
	acCurrentDate = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
	sprintf(acCurrentDate->acMessage, "cudate %02d%02d%02d", RealtimeClock.GetDay(), RealtimeClock.GetMonth(), RealtimeClock.GetYear());
	
	if(acCurrentDate != NULL)
	{
		osMessagePut(msg_XbeeTx, (uint32_t) acCurrentDate, osWaitForever);
	}
}

void NodeCom::registerAtGateway(void) {
	MEM_BLOCK *acRegister;
	acRegister = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
	sprintf(acRegister->acMessage, "register %d", nodeId);
	
	if(acRegister != NULL)
	{
		osMessagePut(msg_XbeeTx, (uint32_t) acRegister, osWaitForever);
	}
}

void NodeCom::publishCurrentTime(void)
{
	RTC_ConvertedTime &RealtimeClock = RTC_ConvertedTime::getInstance();
	RealtimeClock.GetCurrentTimeAndDate();
	
	MEM_BLOCK *acCurrentTime;
	acCurrentTime = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
	sprintf(acCurrentTime->acMessage, "cutime %02d%02d%02d", (unsigned int) RealtimeClock.GetHour(), 
					(unsigned int) RealtimeClock.GetMinute(), (unsigned int) RealtimeClock.GetSecond() );
	//sprintf(acCurrentTime->acMessage, "cutime %d ", time);
	if(acCurrentTime != NULL)	
		osMessagePut(msg_XbeeTx, (uint32_t) acCurrentTime, osWaitForever);
}

void NodeCom::publishString(char * sMessage) 
{
		MEM_BLOCK *acSoltrackError;
		acSoltrackError = (MEM_BLOCK*) osPoolAlloc(xbeeTxMemPoolId);
    sprintf(acSoltrackError->acMessage, "%s", sMessage);
		
		osMessagePut(msg_XbeeTx, (uint32_t) acSoltrackError, osWaitForever);
}

void NodeCom::setCentralAddress(uint8_t *centralAddr)
{
    centralAddress = centralAddr;
}

uint8_t * NodeCom::getCentralAddress()
{
    return(centralAddress);
}

void NodeCom::handleCommand(NodeCom::RX_COMMAND* rxCommand)
{
	unsigned int iTemp = 0;
	switch(rxCommand->cmdName) {
		case curhar:
			switch(rxCommand->cmdType) {
				case set:
					iTemp = atoi(rxCommand->cmdArguments);
					if(iTemp >= CUR_YIELD_UPINT_MIN && iTemp <= CUR_YIELD_UPINT_MAX) {
						curYieldUpInt = iTemp;
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
					}
					break;
				case req:
					publishCurrentHarvest(currentHarvest);
					break;
				default:
					break;
			}
			break;
			
		case curori:
			switch(rxCommand->cmdType){
				case set:
					iTemp = atoi(rxCommand->cmdArguments);
					if(iTemp >= CUR_ORIENTATION_UPINT_MIN && iTemp <= CUR_ORIENTATION_UPINT_MAX)
						curOrientationUpInt = iTemp;
					break;
				case req:
					publishCurrentOrientation(currentHorAngle, currentVerAngle);
					break;
				default:
					break;
			}
			break;
			
		case solori:
			switch(rxCommand->cmdType){
				case req:
					publishSoltrackOrientation(soltrackHorAngle, soltrackVerAngle);
					break;
				default:
					break;
		}
			
		case curtmp:
			switch(rxCommand->cmdType){
				case set:
					iTemp = atoi(rxCommand->cmdArguments);
					if(iTemp >= CUR_TEMP_UPINT_MIN && iTemp <= CUR_TEMP_UPINT_MAX)
						curTempUpInt = iTemp;
					break;
				case req:
					publishSolarTemperature(currentExternTemperature);
					break;
				default:
					break;
			}
			break;
		
		case curgps:
			switch(rxCommand->cmdType){
				case set:
					setGpsLocation(rxCommand->cmdArguments);		
					break;
				case req:
					publishGPS(GPS_Latitude, GPS_Longtitude);
					break;
				default:
					break;
			}
			break;
			
		case solerr:
			switch(rxCommand->cmdType){
				case req:
					
					break;
				default:
					break;
			}
			break;
			
		case cudate:
			switch(rxCommand->cmdType){
				case set:
					setDate(rxCommand->cmdArguments);
					break;
				case req:
					publishCurrentDate();
					break;
				default:
					break;
			}
			break;
			
		case cutime:
			switch(rxCommand->cmdType){
				case set:
					setTime(rxCommand->cmdArguments);
					break;
				case req:
					publishCurrentTime();
					break;
				default:
					break;
			}
			break;
			
			case regist:
			switch(rxCommand->cmdType){
				case ack:
					if(atoi(rxCommand->cmdArguments) == nodeId) {
						bNodeIsRegistered = true;
					}
					break;
				default:
					break;
			}
			break;
			
		default:
			return;
	}
}

NodeCom::RX_COMMAND NodeCom::interpretCommand(char *pCommand)
{
    RX_COMMAND cmdStruct;
    char cmdType[4] = "\0";
    char cmdName[7] = "\0";
    char cmdArgs[25] = "\0";
    memcpy(cmdType, pCommand, 3);
    memcpy(cmdName, pCommand + 4, 6);
    strcpy(cmdArgs, pCommand + 11);

    cmdStruct.cmdType = getCommandType(cmdType);
    cmdStruct.cmdName = getCommandName(cmdName);
    strcpy(cmdStruct.cmdArguments, cmdArgs);

    return cmdStruct;
}

NodeCom::CommandInfo NodeCom::getCommandType(char *cmdType)
{
    if(strcmp(cmdType, "set") == 0) {
        return NodeCom::set;
    }
    else if(strcmp(cmdType, "req") == 0) {
        return NodeCom::req;
    }
		else if(strcmp(cmdType, "ack") == 0) {
        return NodeCom::ack;
    }
    else {
        return NodeCom::invalid;
    }
}

NodeCom::CommandInfo NodeCom::getCommandName(char *cmdName)
{
    if(strcmp(cmdName, "curhar") == 0) {
			return NodeCom::curhar;
		}
    else if(strcmp(cmdName, "curori") == 0) {
			return NodeCom::curori;
    }
    else if(strcmp(cmdName, "solori") == 0) {
        return NodeCom::solori;
    }
    else if(strcmp(cmdName, "curtmp") == 0) {
        return NodeCom::curtmp;
    }
    else if(strcmp(cmdName, "curgps") == 0) {
        return NodeCom::curgps;
    }
    else if(strcmp(cmdName, "solerr") == 0) {
        return NodeCom::solerr;
    }
    else if(strcmp(cmdName, "cudate") == 0) {
        return NodeCom::cudate;
    }
    else if(strcmp(cmdName, "cutime") == 0) {
        return NodeCom::cutime;
    }
		else if(strcmp(cmdName, "regist") == 0) {
        return NodeCom::regist;
    }
    else {
        return NodeCom::invalid;
    }
}

void NodeCom::setGpsLocation(char * cmdArguments) 
{
	int iIndex = 0;
	int iSecondArgOffset = 0;
	char cLatitude[10] = "\0";
	char cLongtitude[10] = "\0";
	
	for(iIndex = 0; iIndex < strlen(cmdArguments); iIndex++)
	{
		if(cmdArguments[iIndex] == ' ') {
			iIndex++;
			break;
		}
		else
			cLatitude[iIndex] = cmdArguments[iIndex];
	}
	for(iSecondArgOffset = iIndex; iIndex < strlen(cmdArguments); iIndex++)
	{
		cLongtitude[iIndex - iSecondArgOffset] = cmdArguments[iIndex];
	}
	GPS_Latitude = atof(cLatitude);
	GPS_Longtitude = atof(cLongtitude);
	
	//Settings->writeGpsCoordinates(GPS_Latitude, GPS_Longtitude);
	enginecontrol.SetLocation((double)GPS_Longtitude/R2D, (double)GPS_Latitude/R2D);
	publishGPS(GPS_Latitude, GPS_Longtitude);
}

void NodeCom::setDate(char * date)
{	
	RTC_ConvertedTime &RealtimeClock = RTC_ConvertedTime::getInstance();
	
	char day[3] = "\0";
	char month[3] = "\0";
	char year[3] = "\0";
	
	memcpy(day, date, 2);
	memcpy(month, date + 2, 2);
	memcpy(year, date + 4, 2);	
	
	RealtimeClock.SetDate(atoi(day), atoi(month), atoi(year), MONDAY); 	
	publishCurrentDate();
}

void NodeCom::setTime(char * time)
{
	RTC_ConvertedTime &RealtimeClock = RTC_ConvertedTime::getInstance();
	
	char hours[3] = "\0";
	char minutes[3] = "\0";
	char seconds[3] = "\0";
	
	memcpy(hours, time, 2);
	memcpy(minutes, time + 2, 2);
	memcpy(seconds, time + 4, 2);	
	
	RealtimeClock.SetTime(atoi(hours), atoi(minutes), atoi(seconds), AM);
	publishCurrentTime();
}

