/**
* @file NodeCom.h
*	@brief 
* @author Jan Roorda
*/

#ifndef NODECOM_H
#define NODECOM_H

#include <stdio.h>
#include "XBeeDM.h"
#include "stm32l1xx_hal.h"
#include "cmsis_os.h"

#define TX_MAILBOX_COUNT 10
#define RX_MAILBOX_COUNT 10

/**
* @brief Class NodeCom
*/
class NodeCom
{
public:
    NodeCom();
    virtual ~NodeCom();

        enum CommandInfo {
						invalid,
						set,
						req,
						ack, 
						curhar,
            curori,
            solori,
            curtmp,
            curgps,
            solerr,
            cudate,
            cutime,
						regist
        };

		typedef struct {
			char acMessage[50];
			uint16_t uiIndex;
		} MEM_BLOCK;
		
		typedef struct {
			uint8_t* rxMessage;
			uint16_t uiLength;
		} RX_MESSAGE;

		typedef struct {
				unsigned int cmdType;
				unsigned int cmdName;
				char cmdArguments[25];
		} RX_COMMAND;

    void publishCurrentHarvest(float curHarvest);
		void publishCurrentOrientation(float horizontalAngle, float verticalAngle);
		void publishSoltrackOrientation(float horizontalAngle, float verticalAngle);
    void publishSolarTemperature(float temperature);
    void publishGPS(float latitude, float longtitude);
		void publishSoltrackError(float horizontalAngle, float verticalAngle);
		void publishCurrentDate(void);
		void publishCurrentTime(void);
		
		void registerAtGateway(void);
		
		void publishString(char * sMessage);

    void setCentralAddress(uint8_t * centralAddr);
    uint8_t * getCentralAddress();

    void handleCommand(NodeCom::RX_COMMAND* rxCommand);
    RX_COMMAND interpretCommand(char * pCommand);
		
		void setGpsLocation(char * cmdArguments);
		void setDate(char * date);
		void setTime(char * time);

private:
    uint8_t * centralAddress;
    CommandInfo getCommandType(char * cmdType);
    CommandInfo getCommandName(char * cmdName);
};

#endif // NODECOM_H
