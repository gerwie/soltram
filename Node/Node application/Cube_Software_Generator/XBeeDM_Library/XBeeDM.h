/**
 * @file XBeeDM.h
 * @Author Jan Roorda
 * @Created Mar 30, 2016
 * @brief
 *      
 */

#ifndef XBEEDM_H_
#define XBEEDM_H_

#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef COMPILE_ARM
#define QT_DEBUG
#endif


#define XBEE_START_DELIMITER 0x7E
#define XBEE_TX_FRAME_TYPE 0x10
#define XBEE_RX_FRAME_TYPE 0x90
#define XBEE_DEFAULT_TX_FRAME_ID 0x01
#define ADDRESS_SIZE 8


class XBeeTxMessage {
public:
    XBeeTxMessage(char * message, uint8_t * sourceAddr, uint8_t * destinationAddr);
    XBeeTxMessage(uint8_t * receivedXBeePacket, unsigned int uiPacketLength);
    virtual ~XBeeTxMessage();

    uint8_t destAddr[ADDRESS_SIZE];
    uint8_t srcAddr[ADDRESS_SIZE];
    char * sMessage;
    uint8_t * XBeePacket;
    unsigned int XBeePacketSize;
    bool bValid;
};

class XBeeRxMessage {
public:
		
	XBeeRxMessage(uint8_t * receivedXBeePacket, uint16_t uiPacketLength);
	XBeeRxMessage(const XBeeRxMessage &cpInstance);

	virtual ~XBeeRxMessage();
	
    uint8_t srcAddr[ADDRESS_SIZE];
    char * sMessage;
	uint8_t receiveOptions;
	
    uint8_t * XBeePacket;
    unsigned int XBeePacketSize;
    bool bValid;
};

class XBeeDM {
public:
    XBeeDM();
    virtual ~XBeeDM();
};



#endif /* XBEEDM_H_ */
