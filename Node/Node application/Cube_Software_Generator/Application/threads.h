/**
* @file Treads.h
*	@brief 
* @author 
*/

/* Threads.h */ 
#ifndef __THREADS_H
#define __THREADS_H

/* User Threads */
void thrStartUp(void const *arg);
void thrYield(void const *arg);
void thrOrientation(void const *arg);
void thrPhotoMeasurement(void const *arg);
void thrCommandHandler(void const *arg);
void thrXbeeTransmit(void const *arg);
void thrProcessRxMessage(void const *arg);
void thrResetWatchDogTimer(void const *arg);
void thrRequestTemperature(void const *arg);

void thrStatusLED(void const *arg);
void thrPowerStatus(void const *arg);




#endif
