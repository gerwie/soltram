/**
* @file Treads.cpp
*	@brief 
* @author 
*/

#include "threads.h"
#include "cmsis_os.h"
#include "stm32l1xx_hal.h"
#include "NodeCom.h"
#include "XBeeDM.h"
#include "TMP421.h"
#include "OrientationADC.h"
#include "led_interface.h"
#include "power_interface.h"
#include "EngineControl.h"
#include "RTC_TimeConverter.h"

/* Hardware handles */
extern ADC_HandleTypeDef hadc;
extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim5;
extern UART_HandleTypeDef huart1;
extern WWDG_HandleTypeDef hwwdg;
extern RTC_HandleTypeDef hrtc;

/* Global class instantions */
extern NodeCom xbeeCommunication;
extern OrientationADC photoADC;
extern EngineControl enginecontrol;

/* Message queue / mailbox IDs */
extern osMessageQId msg_XbeeTx;
extern osMessageQId msg_XbeeRx;
extern osMessageQId msg_CommandHandler;

/* Memory Pool ID's */
extern osPoolId xbeeTxMemPoolId;
extern osPoolId xbeeRxMemPoolId;
extern osPoolId commandHandlerMemPoolId;

/* Global vars */
extern unsigned int curYieldUpInt;						
extern unsigned int curOrientationUpInt;
extern unsigned int curTempUpInt;

/** test task used for thread testing 
*/
void thrYield(void const *arg){
	float curHar = 0.0;
	while(1){
		osDelay(curYieldUpInt);
		xbeeCommunication.publishCurrentHarvest(curHar++);
	}
}

void thrOrientation(void const *arg){
	bool firstBoot = true;							//Different code sequence with first boot of device
	bool daytime = false;								//Remember if it is daytime of nighttime
	StatusPin horizontale_engine;				//Status of the horizontal engine (on = fine, off = while turning hit end switch, error = need to turn but stand agains a end switch)
	StatusPin verticale_engine;					//Status of the horizontal engine (on = fine, off = while turning hit end switch, error = need to turn but stand agains a end switch)
	int deltaSoltrackHorizontaal = 0;		//Calc delta of the soltrack to the solar sensor
	int deltaSoltrackVerticaal = 0;			//Calc delta of the soltrack to the solar sensor
	
	/* ----------------- Get current time --------------------------- */
	RTC_ConvertedTime &RealtimeClock = RTC_ConvertedTime::getInstance();
	RealtimeClock.GetCurrentTimeAndDate();
	enginecontrol.SetTime(RealtimeClock.GetYear(), RealtimeClock.GetMonth(), RealtimeClock.GetDay(),
												RealtimeClock.GetHour(), RealtimeClock.GetMinute(), RealtimeClock.GetSecond());
	
	/* ----------------- Get current time published --------------------------- */
	xbeeCommunication.publishCurrentTime();
	
	/* ----------------- Set Location to HAN University --------------------------- */
	enginecontrol.SetLocation((double)5.97769/R2D, (double)51.9943/R2D);
	
	/* ----------------- Calc sun rise and sun set --------------------------- */
	enginecontrol.CalcSunRiseSet(30);
	
	while(1){
		osDelay(curOrientationUpInt);
	
		/* ----------------- Run Soltrack Algorithm --------------------------- */
		RealtimeClock.GetCurrentTimeAndDate();
		enginecontrol.SetTime(RealtimeClock.GetYear(), RealtimeClock.GetMonth(), RealtimeClock.GetDay(),
													RealtimeClock.GetHour(), RealtimeClock.GetMinute(), RealtimeClock.GetSecond());
		enginecontrol.StartSolTrack();
		xbeeCommunication.publishCurrentTime();
		osDelay(100);
		float horizontalAngle_Soltrack = (float)(enginecontrol.GetDegreesAzimuth());
		float verticalAngle_Soltrack =  (float)(enginecontrol.GetDegreesAltitude());
		xbeeCommunication.publishSoltrackOrientation(horizontalAngle_Soltrack,verticalAngle_Soltrack);
		
		if(firstBoot) {
			if(daytime) {
				//if daytime set engine to soltrack returned value
				horizontale_engine = enginecontrol.SetEngineLocationSetPoint(hor, degrees, (double) horizontalAngle_Soltrack); 
			  //verticale_engine = enginecontrol.SetEngineLocationSetPoint(ver, degrees, (double) verticalAngle_Soltrack);
			}else{
				//if nighttime set engine to Sun rise
				horizontale_engine = enginecontrol.SetEngineLocationSetPoint(hor, degrees, enginecontrol.GetDegreesEngineSunRise()); 
			  //verticale_engine = enginecontrol.SetEngineLocationSetPoint(ver, degrees, 0);
			}
			firstBoot = false;
		} else {
		/* ----------------- Normal operation  motor control --------------------------- */	
			photoADC.runMeasurements();
		
			if(photoADC.isLightIntensityLow()) {
				horizontale_engine = enginecontrol.SetEngineLocationSetPoint(hor, degrees, (double) horizontalAngle_Soltrack); 
				verticale_engine = enginecontrol.SetEngineLocationSetPoint(ver, degrees, (double) verticalAngle_Soltrack);
			} else {
				char stringetje[50];
				sprintf(stringetje, "N: %d, S: %d, W: %d, E: %d, Ver dev: %d", photoADC.nLightValue, photoADC.sLightValue, photoADC.wLightValue, photoADC.eLightValue, photoADC.getAzimutDeviation());
				xbeeCommunication.publishString(stringetje);
				osDelay(10);
				/* ------------ Horizontal control from ADC ----------------- */
				while(photoADC.getAzimutDeviation() != OrientationADC::noDeviation) {
					if(photoADC.getAzimutDeviation() == OrientationADC::leftOfSensor) {
					
						horizontale_engine = enginecontrol.SetEngine(hor,left,AMOUNT_PULSE_AFTER_SENSOR_ERROR);	// Turn engine left
					} else {
					
						horizontale_engine = enginecontrol.SetEngine(hor, right, AMOUNT_PULSE_AFTER_SENSOR_ERROR); // Turn engine right
					}
					photoADC.runMeasurements();
					if(photoADC.isLightIntensityLow()) 
						break;
				}
			
			
			/* ------------ Vertical control from ADC ----------------- */
				while(photoADC.getAltitudeDeviation() != OrientationADC::noDeviation) {
					if(photoADC.getAltitudeDeviation() == OrientationADC::aboveSensor) {
						Led_SetIndicator(LED_BLUE);
						verticale_engine = enginecontrol.SetEngine(ver, up, AMOUNT_PULSE_AFTER_SENSOR_ERROR);	// Turn engine up
					} else {
						Led_SetIndicator(LED_RUNNING_FINE);
						verticale_engine = enginecontrol.SetEngine(ver, down, AMOUNT_PULSE_AFTER_SENSOR_ERROR); // Turn engine down
					}
					photoADC.runMeasurements();
					if(photoADC.isLightIntensityLow()) 
						break;
				}
				Led_SetIndicator(LED_DISABLED);
			}
		}
		
	/* ------------ calc delta currentvalue vs setpoint ----------------- */
	deltaSoltrackHorizontaal = enginecontrol.GetEngineHorLocationCurrentValue() - enginecontrol.GetEngineHorLocationSetPointValue();
	deltaSoltrackVerticaal = enginecontrol.GetEngineVerLocationCurrentValue() - enginecontrol.GetEngineVerLocationSetPointValue();
	xbeeCommunication.publishCurrentOrientation(enginecontrol.GetEngineHorLocationCurrentDegrees(),
																							enginecontrol.GetEngineVerLocationCurrentDegrees());
	}
}

void thrPhotoMeasurement(void const *arg) {
	osThreadTerminate(osThreadGetId());
}

void thrCommandHandler(void const *arg) 
{
	osEvent event;
	while(1) {
		event = osMessageGet(msg_CommandHandler, osWaitForever);
		if(event.status == osEventMessage)
		{
			NodeCom::RX_COMMAND * rxCommand;
			rxCommand = (NodeCom::RX_COMMAND*) event.value.p;		
			xbeeCommunication.handleCommand(rxCommand);
			osPoolFree(commandHandlerMemPoolId, rxCommand);
		}
	}
}

void thrXbeeTransmit(void const *arg)
{
	osEvent event;
	while(1) {
		event = osMessageGet(msg_XbeeTx, osWaitForever);
		if(event.status == osEventMessage)
		{
			NodeCom::MEM_BLOCK * txMemBlock;
			txMemBlock = (NodeCom::MEM_BLOCK*) event.value.p;
			XBeeTxMessage xbMsg((char*) txMemBlock->acMessage, NULL, xbeeCommunication.getCentralAddress());
			HAL_UART_Transmit_IT(&huart1, xbMsg.XBeePacket, xbMsg.XBeePacketSize);
			osPoolFree(xbeeTxMemPoolId, event.value.p);
		}
	}
} 

void thrProcessRxMessage(void const *arg) {
	uint8_t pData;
	HAL_UART_Receive_IT(&huart1, &pData, 1);
	
	osEvent event;
	while(1) {
		event = osMessageGet(msg_XbeeRx, osWaitForever);
		if(event.status == osEventMessage) {
			NodeCom::RX_MESSAGE * rxStruct;
			
			rxStruct = (NodeCom::RX_MESSAGE*) event.value.p;
			XBeeRxMessage xbMsg((uint8_t*) rxStruct->rxMessage, rxStruct->uiLength);
			
			NodeCom::RX_COMMAND * rxCommand;
			rxCommand = (NodeCom::RX_COMMAND*) osPoolCAlloc(commandHandlerMemPoolId);
			*rxCommand = xbeeCommunication.interpretCommand(xbMsg.sMessage);
			osMessagePut(msg_CommandHandler, (uint32_t) rxCommand, osWaitForever);
			
			if(xbMsg.bValid == true) {
				
			}
			osPoolFree(xbeeTxMemPoolId, rxStruct->rxMessage);
			osPoolFree(xbeeRxMemPoolId, rxStruct);
		}
	}
}

void thrResetWatchDogTimer(void const *arg)
{
	// reset watchdog timer
	//HAL_WWDG_Refresh(&hwwdg,500);
}

void thrRequestTemperature(void const *arg)
{
	// request temperature from TMP421
	float sensorTemperature = 0.0;
	while(1){
		osDelay(curTempUpInt);
		sensorTemperature = TMP421_GetCurrentValue();
		xbeeCommunication.publishSolarTemperature(sensorTemperature);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	static uint8_t firstRxByte = 0;
	static uint8_t rxState = 0;
	static uint16_t rxPacketSize = 0;
	static uint8_t * pData;
	
	switch(rxState) {
		case 0:
			static uint8_t * pRxData;
			pRxData = huart->pRxBuffPtr-1;
			if(*pRxData == XBEE_START_DELIMITER) {
				
				pData = (uint8_t*) osPoolAlloc(xbeeTxMemPoolId);
				if(pData == NULL)
					return;
				*pData = XBEE_START_DELIMITER;
				HAL_UART_Receive_IT(&huart1, pData + 1, 2);	// Receive rxPacketSize
				rxState = 1;
			} else {
				HAL_UART_Receive_IT(&huart1, &firstRxByte, 1);
			}
			break;
		case 1:
			rxPacketSize = (*(huart->pRxBuffPtr - 2) << 8);
			rxPacketSize |= *(huart->pRxBuffPtr - 1);
			HAL_UART_Receive_IT(&huart1, pData + 3, rxPacketSize + 1);	// Receive rxPacketSize
			rxState = 2;
			break;
		case 2:
			if(pData[3] == XBEE_RX_FRAME_TYPE) {  // Check for TX frame type
				NodeCom::RX_MESSAGE *xbeeMessage;
				xbeeMessage = (NodeCom::RX_MESSAGE*) osPoolAlloc(xbeeRxMemPoolId);
				xbeeMessage->rxMessage = pData;
				xbeeMessage->uiLength = rxPacketSize + 4;	/* +4 includes start delimiter, two length bytes and checksum byte*/
				
				osMessagePut(msg_XbeeRx, (uint32_t)xbeeMessage, 0);
			} else {
				osPoolFree(xbeeTxMemPoolId, pData);
			}
			
			rxState = 0;
			HAL_UART_Receive_IT(&huart1, &firstRxByte, 1);
			break;
		default:
			rxState = 0;
		break;
	}
}

void thrStatusLED(void const *arg)
{
	while(1){
		osDelay(1950);
		if(photoADC.isLightIntensityLow())
			Led_SetIndicator(LED_BLUE);
		else
			Led_SetIndicator(LED_RUNNING_FINE);
		osDelay(50);
		Led_SetIndicator(LED_DISABLED);
	}
}

void thrPowerStatus(void const *arg)
{
	osThreadTerminate(osThreadGetId());
	while(1) {
		
	}
}




