/**
* @file NodeInformation.h
*	@brief 
* @author 
*/

#ifndef NODEINFORMATION_H
#define NODEINFORMATION_H


#define CUR_YIELD_UPINT_MAX 3600000
#define CUR_YIELD_UPINT_MIN 1000
#define CUR_ORIENTATION_UPINT_MAX 3600000
#define CUR_ORIENTATION_UPINT_MIN 1000
#define CUR_TEMP_UPINT_MAX 3600000
#define CUR_TEMP_UPINT_MIN 1000

unsigned int nodeId = 2;
bool bNodeIsRegistered = false;

/* Global variables declarations */
unsigned int curYieldUpInt = 1000;					/* Update interval, Miliseconds. 1000 < x < 3600000 */
unsigned int curOrientationUpInt = 1000; 			/* Update interval, Miliseconds. 1000 < x < 3600000 */
unsigned int curTempUpInt = 1000;					/* Update interval, Miliseconds. 1000 < x < 3600000 */

/* Runtime variable parameters */
float GPS_Latitude = 0.0;
float GPS_Longtitude = 0.0;

float currentHarvest = 0.0;
float currentHorAngle = 0.0;
float currentVerAngle = 0.0;
float soltrackHorAngle = 0.0;
float soltrackVerAngle = 0.0;
float currentExternTemperature = 0.0;
float currentInternTemperature = 0.0;



#endif
