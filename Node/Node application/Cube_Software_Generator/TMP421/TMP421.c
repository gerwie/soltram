/**
 * @file TMP421.c
 * @brief Source driver file for temperature sensor (TMP421)
 * @author Richard Berkvens
 */

#include "TMP421.h"

uint8_t SensorStatus = CONVERSION_NOT_BUSY;
HAL_StatusTypeDef TMP421Status = HAL_ERROR;
static uint8_t HighByte = 0,LowByte = 0;
static uint8_t SensorConversionBusy = 0;

void TMP421_Init()
{
	/* 	this function initialzes the temperature sensor according to below specs
			-- Configuration register 1
					-- temperature range - 40C to 127C	(bit 2 == 0)
					-- Shutdown mode - One shot conversion mode (bit 6 == 1)
					-- remaining bits are reserved and must be set to 0 (bit 0,1,3,4,5,7)
					-- current register value (0b01000000)
			-- Configuration register 2
					-- series resitance correction bit - series restistance correction is disabled (bit 2 = 0)
					-- Local Enable Bit - Local temperature sensor channel disabled (bit 3 = 1)
					-- Enable external temp channel 1 - External temp channel 1 enabled (bit 4 = 0)
					-- Enable external temp channel 2 - External temp channel 2 disabled (bit 5 = 0)
					-- Enable external temp channel 3 - External temp channel 3 disabled (bit 6 = 0)
					-- remaining bits are reserved and must be set to 0 (bit 0,1,7)
					-- -- current register value (0b00011000)
			-- Conversion rate register
					-- Conversion rate is not used, only single shot is used in this application - all bits 0 
					-- current register value (0b00000000)
	*/

		uint8_t CONFIGURATION_1_VALUE[2] = {TMP421_CONFIG_1_REG,0x40};
		uint8_t CONFIGURATION_2_VALUE[2] = {TMP421_CONFIG_2_REG,0x18};
		uint8_t CONVERSION_VALUE[2] = {TMP421_CONVERSION_REG,0x00};
		
		TMP421Status = HAL_I2C_IsDeviceReady(&hi2c1,TMP421_I2C_ID_WRITE,I2C_TRIALS,I2C_TIMEOUT);		
		if (TMP421Status != HAL_OK){
			// Sensor error
			HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);
			return;
		}

		// I2C Configuration register 1 message
		while (HAL_I2C_Master_Transmit_IT(&hi2c1,TMP421_I2C_ID_WRITE,CONFIGURATION_1_VALUE,2) == HAL_BUSY){;}
				
		// I2C Configuration register 2 message
		while (HAL_I2C_Master_Transmit_IT(&hi2c1,TMP421_I2C_ID_WRITE,CONFIGURATION_2_VALUE,2) == HAL_BUSY){;}
		
		// I2C Conversion register message
		while (HAL_I2C_Master_Transmit_IT(&hi2c1,TMP421_I2C_ID_WRITE,CONVERSION_VALUE,2) == HAL_BUSY){;}
}


float TMP421_GetCurrentValue()
{
	uint8_t dummyValue = 0x01;
	uint8_t REQUEST_ONE_SHOT_VALUE[2] = {TMP421_ONE_SHOT_START_REG,dummyValue};
	
	hi2c1.pBuffPtr = NULL;
	TMP421_CheckIfBusy();
	
	if (SensorStatus == CONVERSION_NOT_BUSY)
	{
		// I2C Conversion register message
		while (HAL_I2C_Master_Transmit_IT(&hi2c1,TMP421_I2C_ID_WRITE,REQUEST_ONE_SHOT_VALUE,2) == HAL_BUSY){}
		TMP421_CheckIfBusy();
		while (SensorStatus == CONVERSION_BUSY)
		{
			TMP421_CheckIfBusy();
		}
		// request high byte temp conversion
		while(HAL_I2C_Mem_Read(&hi2c1, TMP421_I2C_ID_READ, TMP421_REM_TEMP_1_HIGH_BYTE_REG, 1,&HighByte, 1, 100) != HAL_OK){}
			
		// request low byte temp conversion
		while(HAL_I2C_Mem_Read(&hi2c1, TMP421_I2C_ID_READ, TMP421_REM_TEMP_1_LOW_BYTE_REG, 1,&LowByte, 1, 100) != HAL_OK){}
	}else
	{
		// conversion is busy, wait till done then read bytes
		while (SensorStatus == CONVERSION_BUSY)
		{
			TMP421_CheckIfBusy();
		}
		// request high byte temp conversion
		while(HAL_I2C_Mem_Read(&hi2c1, TMP421_I2C_ID_READ, TMP421_REM_TEMP_1_HIGH_BYTE_REG, 1,&HighByte, 1, 100) != HAL_OK){}
				
		// request low byte temp conversion
		while(HAL_I2C_Mem_Read(&hi2c1, TMP421_I2C_ID_READ, TMP421_REM_TEMP_1_LOW_BYTE_REG, 1,&LowByte, 1, 100) != HAL_OK){}
	}	
	
	// convert retrieved value's to degree celcius.
	float RetrievedTemperature =  CalculateTemperature(HighByte,LowByte);
	return RetrievedTemperature;
}

void TMP421_CheckIfBusy()
{
	SensorConversionBusy = 0;
	
	hi2c1.pBuffPtr = NULL;
	while(HAL_I2C_Mem_Read(&hi2c1, TMP421_I2C_ID_READ, TMP421_STATUS_REG, 1,&SensorConversionBusy, 1, 1000) != HAL_OK){}
	
	if (SensorConversionBusy == 0x80)
	{
		SensorStatus = CONVERSION_BUSY;
	}else
	{
		SensorStatus = CONVERSION_NOT_BUSY;
	}
}

void TMP421_Reset()
{
	uint8_t REQUEST_RESET[2] = {TMP421_SOFT_RST_REG,0x01};
	
	while (HAL_I2C_Master_Transmit_IT(&hi2c1,TMP421_I2C_ID_WRITE,REQUEST_RESET,2) == HAL_BUSY){}
	SensorStatus = SENSOR_RESET;
}

uint8_t TMP421_RequestManufacturerID()
{
	uint8_t ManufacturerID = 0;
	
	while(HAL_I2C_Mem_Read_IT(&hi2c1, TMP421_I2C_ID_READ, TMP421_MANUF_ID_REG, 1,&ManufacturerID, 1) != HAL_OK){}

	return ManufacturerID;
}

uint8_t TMP421_RequestDeviceID()
{
	uint8_t DeviceID = 0;
	
	while(HAL_I2C_Mem_Read_IT(&hi2c1, TMP421_I2C_ID_READ, TMP421_DEVICE_ID_REG, 1,&DeviceID, 1) != HAL_OK);

	return DeviceID;
}

float CalculateTemperature(const uint8_t High, const uint8_t Low)
{
	float temperature = 0.0;
	// value before comma
	if (High & 128)
	{
		// negative number 
		int8_t High_Byte_Converted = (int8_t)High;
		temperature = (float)High_Byte_Converted;
	}else
	{
		temperature = (float)High;
	}
	
	// value after comma
	uint8_t Low_Byte_Converted = Low;
	Low_Byte_Converted = Low_Byte_Converted >> 4;
	float Low_Byte_Calculated = (float)Low_Byte_Converted * 0.0625;
	
	// final temperature value
	temperature = temperature + Low_Byte_Calculated;
	return temperature;
}

// CALLBACK FUNCTIONS FROM HAL DRIVER
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
	SensorStatus = SENSOR_ERROR;
	// this function is executed whenever an mesage has caused an error
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);
}
