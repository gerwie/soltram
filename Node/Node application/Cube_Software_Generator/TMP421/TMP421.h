/**
 * @file TMP421.h
 * @brief Header driver file for temperature sensor (TMP421)
 * @author Richard Berkvens
 */

#ifndef TMP421
#define TMP421

#include "stm32l1xx_hal.h"
#include "nodecom.h"

#define 	TMP421_I2C_ID_READ 								0x99 
#define 	TMP421_I2C_ID_WRITE 							0x98 

#define 	TMP421_POWER_ON_POINTER_REG				0x00
#define 	TMP421_LOC_TEMP_HIGH_BYTE_REG 		0x00
#define 	TMP421_REM_TEMP_1_HIGH_BYTE_REG 	0x01
#define 	TMP421_REM_TEMP_2_HIGH_BYTE_REG 	0x02
#define 	TMP421_REM_TEMP_3_HIGH_BYTE_REG 	0x03

#define 	TMP421_STATUS_REG 								0x08
#define 	TMP421_CONFIG_1_REG 							0x09
#define 	TMP421_CONFIG_2_REG 							0x0A
#define 	TMP421_CONVERSION_REG 						0x0B
#define 	TMP421_ONE_SHOT_START_REG 				0x0F

#define 	TMP421_LOC_TEMP_LOW_BYTE_REG 			0x10
#define 	TMP421_REM_TEMP_1_LOW_BYTE_REG 		0x11
#define 	TMP421_REM_TEMP_2_LOW_BYTE_REG 		0x12
#define 	TMP421_REM_TEMP_3_LOW_BYTE_REG 		0x13

#define 	TMP421_N_CORR_1_REG 							0x21
#define 	TMP421_N_CORR_2_REG 							0x22
#define 	TMP421_N_CORR_3_REG 							0x23

#define 	TMP421_SOFT_RST_REG 							0xFC
#define 	TMP421_MANUF_ID_REG 							0xFE

#define 	TMP421_DEVICE_ID_REG 							0xFF
#define 	TMP422_DEVICE_ID_REG 							0xFF
#define 	TMP423_DEVICE_ID_REG 							0xFF

#define I2C_TIMEOUT													100
#define I2C_TRIALS													100

typedef enum{
	SENSOR_RESET = 0,
	CONVERSION_BUSY,
	CONVERSION_NOT_BUSY,
	SENSOR_ERROR
}TMP421_Status;

extern I2C_HandleTypeDef hi2c1;
extern NodeCom xbeeCommunication;

/**
 * @brief TMP421 sensor init.
 * @retval No return value (void)
 * 
 *	Sensor is initialized in following order,
 *		- Single shot conversion mode
 *		- External temperature channel 1 enabled
 *		- No conversion rate set (single shot)
 */
void TMP421_Init();																

/**
 * @brief TMP421 get current temperature .
 * @retval float (celcius)
 * 
 *	This function requests sensor data from the temperature sensor.
 *	the following I2C calls are executed:
 *		- Check conversion status
 *		- Request single shot conversion
 *		- Read high and Low byte
 *
 * 	Finally the bytes are converted to a temperature using function:  CalculateTemperature().
 */
float TMP421_GetCurrentValue();		

/**
 * @brief TMP421 check if busy .
 * @retval no return value (void)
 * 
 *	This function requests conversion status from the temperature sensor.
 *	the following I2C calls are executed:
 *		- Check conversion status
 *
 * 	If a conversion is busy, the global SensorStatus (enum TMP421_Status) is set to CONVERSION_BUSY, if not it is set to CONVERSION_NOT_BUSY
 */
void TMP421_CheckIfBusy();												

/**
 * @brief TMP421 reset sensor.
 * @retval no return value (void)
 * 
 *	This function resets the sensor to factory specs.
 *	the following I2C calls are executed:
 *		- Request_reset
 *
 * 	After a reset the all registers will have been initilazed to power on state, additionally any conversion running wil be stopped!
 */
void TMP421_Reset();															

/**
 * @brief TMP421 request manufacturer ID.
 * @retval uint8_t (Manufacturer ID)
 * 
 *	This function requests the manufacturer ID from the sensor
 *	the following I2C calls are executed:
 *		- Request_manufacturer_id
 *
 * 	This function will ALWAYS return the following hex value: 0x55
 */
uint8_t TMP421_RequestManufacturerID();						

/**
 * @brief TMP421 request device ID.
 * @retval uint8_t (Device ID)
 * 
 *	This function requests the device ID from the sensor
 *	the following I2C calls are executed:
 *		- Request_device_id
 *
 * 	Depending on the sensor used the sensor will return one of three value's:
 *		- 0x21	(TMP421)
 *		- 0x22	(TMP422)
 *		- 0x23	(TMP423)
 */
uint8_t TMP421_RequestDeviceID();									

/**
 * @brief Calculate temperature.
 * @retval uint8_t (Device ID)
 * @param const uint8_t High (High byte from sensor)
 * @param	const uint8_t Low (Low byte from sensor)
 * 
 *	This function calculates the actual temperature value by using the data received from the temperature sensor
 */
float CalculateTemperature(const uint8_t High, const uint8_t Low);		

/**
 * @brief HAL I2C driver error callback.
 * @retval no return value (void)
 * @param I2C_HandleTypeDef* hi2c
 * 
 *	This function is called whenever an error has occured in the I2C peripheral
 */
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c);

#ifdef __cplusplus

#endif

#endif
