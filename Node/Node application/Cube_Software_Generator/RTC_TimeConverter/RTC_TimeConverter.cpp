/**
* @file 	RTC_TimeConverter.cpp
*	@brief 	This file holds the class implementation needed for the real time clock data
* @author Richard Berkvens
* @date		26-05-2016
*/

#include "RTC_TimeConverter.h"

RTC_ConvertedTime::RTC_ConvertedTime()
{
	Hour = 0;
	Minute = 0;
	Second = 0;
	TimeNotation = 0;
	
	Day = 0;
	Month = 0;
	Year = 0;
	Weekday = 0;
}

void RTC_ConvertedTime::GetCurrentTimeAndDate(void)
{
	uint32_t Time = 0,Date = 0;
	int HourTens = 0,HourOnes = 0;
	int MinuteTens = 0,MinuteOnes = 0;
	int SecondTens = 0,SecondOnes = 0;
	
	int YearTens = 0,YearOnes = 0;
	int MonthTens = 0,MonthOnes = 0;
	int DayTens = 0,DayOnes = 0;
	int WeekdayOnes = 0;
	
	Time = hrtc.Instance->TR;
	Date = hrtc.Instance->DR;
	
	// 0b0000 0000 0011 0000 0000 0000 0000 0000 Hour tens
	// 0b0000 0000 0000 1111 0000 0000 0000 0000 Hour ones
	// BCD coded
	HourTens = (Time & 0x00300000);
	HourTens = (HourTens >> 20);
	HourOnes = (Time & 0x000F0000);
	HourOnes = (HourOnes >> 16);
	Hour = (HourTens * 10) + HourOnes;
	
	// 0b0000 0000 0000 0000 0111 0000 0000 0000 Minute tens
	// 0b0000 0000 0000 0000 0000 1111 0000 0000 Minute ones
	// BCD coded
	MinuteTens = (Time & 0x00007000);
	MinuteTens = (MinuteTens >> 12);
	MinuteOnes = (Time & 0x00000F00);
	MinuteOnes = (MinuteOnes >> 8);
	Minute = (MinuteTens * 10) + MinuteOnes;
	
	// 0b0000 0000 0000 0000 0000 0000 0111 0000 Second tens
	// 0b0000 0000 0000 0000 0000 0000 0000 1111 Second ones
	// BCD coded
	SecondTens = (Time & 0x00000070);
	SecondTens = (SecondTens >> 4);
	SecondOnes = (Time & 0x0000000F);
	Second = (SecondTens * 10) + SecondOnes;
	
	// 0b0000 0000 0100 0000 0000 0000 0000 0000 Am/PM
	// BCD coded
	if (Time & 0x00400000){
		TimeNotation = PM;
	}else{
		TimeNotation = AM;
	}
	
	// 0b0000 0000 0000 0000 0000 0000 0011 0000 Day tens	
	// 0b0000 0000 0000 0000 0000 0000 0000 1111 Day ones
	// BCD coded
	DayTens = (Date & 0x00000030);
	DayTens = (DayTens >> 4);
	DayOnes = (Date & 0x0000000F);
	Day = (DayTens * 10) + DayOnes;

	// 0b0000 0000 0000 0000 0001 0000 0000 0000 Month tens	
	// 0b0000 0000 0000 0000 0000 1111 0000 0000 Month ones
	// BCD coded
	MonthTens = (Date & 0x00001000);
	MonthTens = (MonthTens >> 12);
	MonthOnes = (Date & 0x00000F00);
	MonthOnes = (MonthOnes >> 8);	
	Month = (MonthTens * 10) + MonthOnes;
	
	// 0b0000 0000 0000 0000 1110 0000 0000 0000 Weekday ones
	// BCD coded
	WeekdayOnes = (Date & 0x0000E000);
	WeekdayOnes = (WeekdayOnes >> 13);
	switch (WeekdayOnes){
		case MONDAY:
			Weekday = MONDAY;
			break;
		case TUESDAY:
			Weekday = TUESDAY;
			break;
		case WEDNESDAY:
			Weekday = WEDNESDAY;
			break;
		case THURSDAY:
			Weekday = THURSDAY;
			break;
		case FRIDAY:
			Weekday = FRIDAY;
			break;
		case SATERDAY:
			Weekday = SATERDAY;
			break;
		case SUNDAY:
			Weekday = SUNDAY;
			break;		
		default:
			Weekday = ERROR_WRONG_DAY;
	}
	
	// 0b0000 0000 1111 0000 0000 0000 0000 0000 Year tens	
	// 0b0000 0000 0000 1111 0000 0000 0000 0000 Year ones
	// BCD coded
	YearTens = (Date & 0x00F00000);
	YearTens = (YearTens >> 20);
	YearOnes = (Date & 0x000F0000);
	YearOnes = (YearOnes >> 16);	
	Year = 2000 + (YearTens * 10) + YearOnes;
}

void RTC_ConvertedTime::SetTime(const int hour,const int minute,const int second,const int timenotation)
{
	uint32_t Time = 0;
	int HourTens = 0,HourOnes = 0;
	int MinuteTens = 0,MinuteOnes = 0;
	int SecondTens = 0,SecondOnes = 0;
	
	Hour = hour;
	Minute = minute;
	Second = second;
	TimeNotation = timenotation;
	
	HourTens = hour / 10;
	HourOnes = hour % 10;
	
	MinuteTens = minute / 10;
	MinuteOnes = minute % 10;
	
	SecondTens = second / 10;
	SecondOnes = second % 10;
	
	Time = (TimeNotation << 22) + (HourTens << 20) + (HourOnes << 16) + (MinuteTens << 12) + (MinuteOnes << 8) + (SecondTens << 4) + SecondOnes;
	
	// change RTC registers
	// Unlock RTC regsiters
	PWR->CR |= (1 << 8);	
	hrtc.Instance->WPR = 0xCA;
	hrtc.Instance->WPR = 0x53;	
	hrtc.Instance->ISR |= (1 << 7);
	// poll INITF bit in RTC_ISR reg, untill 1
	while ((hrtc.Instance->ISR & (1 << 6)) == 0) {;}
	// write value in RTC_TR 		
	hrtc.Instance->TR = Time;	
	// Lock RTC registers	
	hrtc.Instance->ISR = 	hrtc.Instance->ISR & (0 << 7);	
}

void RTC_ConvertedTime::SetDate(const int day,const int month,const int year,const int weekday)
{
	uint32_t Date = 0;
	int YearTens = 0,YearOnes = 0;
	int MonthTens = 0,MonthOnes = 0;
	int DayTens = 0,DayOnes = 0;
	int WeekdayOnes = 0;
	
	Day = day;
	Month = month;
	Year = 2000 + year;
	Weekday = weekday;
	
	YearTens = year / 10;
	YearOnes = year % 10;
	
	MonthTens = month / 10;
	MonthOnes = month % 10;
	
	DayTens = day / 10;
	DayOnes = day % 10;
	
	WeekdayOnes = weekday;
	
	Date = (YearTens << 20) + (YearOnes << 16) + (WeekdayOnes << 13) + (MonthTens << 12) + (MonthOnes << 8) + (DayTens << 4) + DayOnes;
	
	// set RTC registers
	// Unlock RTC registers
	PWR->CR |= (1 << 8);	
	hrtc.Instance->WPR = 0xCA;
	hrtc.Instance->WPR = 0x53;	
	hrtc.Instance->ISR |= (1 << 7);
	// poll INITF bit in RTC_ISR reg, untill 1
	while ((hrtc.Instance->ISR & (1 << 6)) == 0) {;}
	// write value in RTC_DR 		
	hrtc.Instance->DR = Date;	
	// Lock RTC regsiters
	hrtc.Instance->ISR = 	hrtc.Instance->ISR & (0 << 7);	
}
