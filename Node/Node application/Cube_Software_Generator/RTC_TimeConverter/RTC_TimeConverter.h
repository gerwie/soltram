/**
* @file 	RTC_TimeConverter.h
*	@brief 	This file holds the class needed for the real time clock data
* @author Richard Berkvens
* @date		26-05-2016
*/

#include "stm32l1xx_hal.h"

#ifndef RTC_TIMECONVERTER_H
#define RTC_TIMECONVERTER_H

#define 	AM 0
#define 	PM 1

#define MONDAY 						1
#define TUESDAY 					2
#define WEDNESDAY 				3
#define THURSDAY 					4
#define FRIDAY 						5
#define SATERDAY 					6
#define SUNDAY 						7
#define ERROR_WRONG_DAY 	0

extern RTC_HandleTypeDef hrtc;


	/**
	* @brief RTC_ConvertedTime class 
	*
	* this class holds the data needed to set and get the RTC timer values
	*/ 
class RTC_ConvertedTime{
	
public:
		static RTC_ConvertedTime& getInstance() {
			static RTC_ConvertedTime instance;
			return instance;
		}
		
		/**
		* @brief SetTime.
		* @retval No return value (void)
		* @param const int hour (new hour value for RTC timer) 
		* @param const int minute (new minute value for RTC timer) 
		* @param const int second (new second value for RTC timer) 
		* @param const int timenotation (new timenotation value for RTC timer) 		
		*
		*	This function initializes the RTC time
		*/
		void SetTime(const int hour,const int minute,const int second,const int timenotation);
		
		/**
		* @brief SetDate.
		* @retval No return value (void)
		* @param const int month (new month value for RTC timer) 
		* @param const int year (new year value for RTC timer) 
		* @param const int weekday (new weekdasy value for class variable Weekday) 
		*
		*	This function initializes the RTC date
		*/		
		void SetDate(const int day,const int month,const int year,const int weekday);
		
		/**
		* @brief GetCurrentTimeAndDate.
		* @retval No return value (void)
		*
		*	This function retrieves the current RTC value and stores is in the class variables
		*/		
		void GetCurrentTimeAndDate(void);
		
		int GetHour() 				{return Hour;}
		int GetMinute() 			{return Minute;}
		int GetSecond() 			{return Second;}
		int GetDay() 					{return Day;}
		int GetMonth() 				{return Month;}
		int GetYear() 				{return Year;}
		int GetTimeNotation() {return TimeNotation;}
		int GetWeekday()			{return Weekday;}
		
private:
		RTC_ConvertedTime(void);
		int Hour,Minute,Second;
		int Year,Month,Day;
		int Weekday;
		int TimeNotation;
};

#endif //RTC_TIMECONVERTER_H
