#include "NodeSettings.h"
#include "stm32l1xx_hal.h"

NodeSettings::NodeSettings()
{
	
}

NodeSettings::~NodeSettings() {
	
}

void NodeSettings::writeGpsCoordinates(double latitude, double longtitude) {
	uint32_t writeAddr = FLASH_EEPROM_BASE + GPS_MEM_ADDRESS;
	uint32_t tmp;
	
	tmp = 0xffffffff & ((uint64_t) latitude >> 32);
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD, writeAddr++, tmp);
	
	tmp = 0xffffffff & ((uint64_t) latitude);
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD, writeAddr++, tmp);
	
	tmp = 0xffffffff & ((uint64_t) longtitude >> 32);
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD, writeAddr++, tmp);
	
	tmp = 0xffffffff & ((uint64_t) longtitude);
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTWORD, writeAddr++, tmp);
}

void NodeSettings::writeCentralAddress(uint8_t *centralAddress) {
	uint32_t writeAddr = FLASH_EEPROM_BASE + CENTRAL_ADDR_MEM_ADDRESS;
	
	for(int i = 0; i < 8; i++) {
		HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE, writeAddr++, centralAddress[i]);
	}
}

void NodeSettings::readGpsCoordinates(double * latitude, double * longtitude) {
	uint32_t *readAddr = (uint32_t*) FLASH_EEPROM_BASE + (uint32_t) GPS_MEM_ADDRESS;
	uint64_t tmplatitude = 0;
	uint64_t tmplongtitude = 0;
	uint32_t tmpValue;
	
	tmpValue = *readAddr++;
	tmplatitude = (uint64_t) tmpValue << 32; 
	
	tmpValue = *readAddr++;
	tmplatitude |= tmpValue;
	
	tmpValue = *readAddr++;
	tmplongtitude = (uint64_t) tmpValue << 32; 
	
	tmpValue = *readAddr++;
	tmplongtitude |= tmpValue;
	
	*latitude = (double) tmplatitude;
	*longtitude = (double) tmplongtitude;
}

void NodeSettings::readCentralAddress(uint8_t *centralAddress) {
	
}

