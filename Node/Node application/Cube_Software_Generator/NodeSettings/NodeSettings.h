#ifndef NODESETTINGS_H
#define NODESETTINGS_H

#include <stdio.h>
#include "cmsis_os.h"

#define GPS_MEM_ADDRESS 0					// two doubles: 16 bytes 
#define CENTRAL_ADDR_MEM_ADDRESS 4	// 8 words

class NodeSettings 
{
public:
	NodeSettings();
	virtual ~NodeSettings();
	
	void writeGpsCoordinates(double latitude, double longtitude);
	void writeCentralAddress(uint8_t *centralAddress);
	void readGpsCoordinates(double * latitude, double * longtitude);
	void readCentralAddress(uint8_t *centralAddress);

private:
		
};


#endif
