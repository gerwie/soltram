/**
* @file orientationADC.ccp
*	@brief 
* @author Lucas Groenen 
*/

#include "orientationADC.h"

extern ADC_HandleTypeDef hadc;

OrientationADC::OrientationADC() {
	
	 measurementIndex				=	0;

	 nightTime							= false;
	 lowLightLevels 				= false;
	 sufficientMeasurements = false;
	 lowLightLevelCounter 	= 0;
}

void OrientationADC::runMeasurements() {
	uint16_t iADCValues[4];
	for(int i = 0; i < 4; i++) {
		switchChannel(i);
		HAL_ADC_Start(&hadc);
		HAL_ADC_PollForConversion(&hadc, 1);
		iADCValues[i] = HAL_ADC_GetValue(&hadc);
		HAL_ADC_Stop(&hadc);
		
		//photoADC.setMeasurementMean(i, valueBuffer);
		//char str[20] = "\n";
		//sprintf(str, "ADC channel %d = %d", i, iADCValues[i]);
		//xbeeCommunication.publishString(str);
		//osDelay(100);
	}
	nLightValue = iADCValues[0];
	eLightValue = iADCValues[1];
	sLightValue = iADCValues[2];
	wLightValue = iADCValues[3];
	
	if( (nLightValue < LOW_LIGHT_INTENSITY) ||
			(eLightValue < LOW_LIGHT_INTENSITY) ||
			(sLightValue < LOW_LIGHT_INTENSITY) ||
			(wLightValue < LOW_LIGHT_INTENSITY) 		) {
		lowLightLevels = true;
		lowLightLevelCounter++;
				
		if(lowLightLevelCounter >= NIGHT_COUNTDOWN) {
			setNightTime(false);	// Should be set to true, is set to false for debugging purposes
			lowLightLevelCounter = NIGHT_COUNTDOWN; 	// To prevent lowLightLevelCounter to overflow
		}
		
	} else {
		setNightTime(false);
		lowLightLevels = false;
		lowLightLevelCounter = 0;
		calculateDeviations();
	}
}

void OrientationADC::calculateDeviations() {
		OrientationADC::horPosition();
		OrientationADC::verPosition();
}

void OrientationADC::horPosition(){
	
    float horPosBuffer;

		horPosBuffer = (float) eLightValue + wLightValue;

		if(horPosBuffer == 0.0) {
			horDeviation = 0.0;
			return;
		}
	
    horDeviation = (float) ((eLightValue - wLightValue)/ horPosBuffer);
}

void OrientationADC::verPosition(){
	
    float verPosBuffer;
	
    verPosBuffer = (float) nLightValue + sLightValue;

		if(verPosBuffer == 0.0) {
			horDeviation = 0.0;
			return;
		}
	
	  verDeviation = (float) ((nLightValue - sLightValue)/ verPosBuffer);
}

int	OrientationADC::getCardinalValue(int cardinalDirection){
	
	switch(cardinalDirection){
	
		case NORTH : 
			return OrientationADC::nLightValue;
		case SOUTH : 
			return OrientationADC::sLightValue;
		case EAST : 
			return OrientationADC::eLightValue;
		case WEST : 
			return OrientationADC::wLightValue;
		default:
			break;
	}
	return 0;
}

float OrientationADC::getHorDeviation(){
	
    return OrientationADC::horDeviation;
}

float OrientationADC::getVerDeviation(){
	
    return OrientationADC::verDeviation;
}

unsigned int OrientationADC::getAzimutDeviation() {
	if((horDeviation < SENSOR_ANGLE_TRESHOLD) && (horDeviation > -SENSOR_ANGLE_TRESHOLD)) 
		return noDeviation;
	else if (horDeviation <= -SENSOR_ANGLE_TRESHOLD) 
		return rightOfSensor;
	else if (horDeviation >= SENSOR_ANGLE_TRESHOLD)
		return leftOfSensor;
	else
		return 0;
}

unsigned int OrientationADC::getAltitudeDeviation() {
	if((verDeviation < SENSOR_ANGLE_TRESHOLD) && (verDeviation > -SENSOR_ANGLE_TRESHOLD)) 
		return noDeviation;
	else if (verDeviation <= -SENSOR_ANGLE_TRESHOLD) 
		return belowSensor;
	else if (verDeviation >= SENSOR_ANGLE_TRESHOLD)
		return aboveSensor;
	else
		return 0;
}

void OrientationADC::switchChannel(int cardinalDirection){
	
	switch(cardinalDirection){
	
		case NORTH : 
			adcConfig.Channel = ADC_CHANNEL_10;
			break;
		case EAST : 
			adcConfig.Channel  = ADC_CHANNEL_11;
			break;
		case SOUTH : 
			adcConfig.Channel = ADC_CHANNEL_12;
			break;
		case WEST : 
			adcConfig.Channel  = ADC_CHANNEL_13;
			break;
		default:
			break;
	}
	
	adcConfig.Rank = 1;
	adcConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES;
	HAL_ADC_ConfigChannel(&hadc, &adcConfig);
}

void	OrientationADC::setLightValue(int value, int cardinalDirection){
	
	if(value <= LOW_LIGHT_INTENSITY) {
		
		OrientationADC::lowLightLevels = true;
	} else if(value > LOW_LIGHT_INTENSITY ){
		
		OrientationADC::lowLightLevels = false;
	}
	
	switch(cardinalDirection){
		
		case NORTH : 
			OrientationADC::nLightValue = value;
			break;
		case SOUTH : 
			OrientationADC::sLightValue = value;
			break;
		case EAST  : 
			OrientationADC::eLightValue = value;
			break;
		case WEST  : 
			OrientationADC::wLightValue = value;;
			break; 
		default:
			break;
	}			
}

void    OrientationADC::runningMean(int cardinalDirection, int value) {

	if(value <= LOW_LIGHT_INTENSITY) {
		
		value = LOW_LIGHT_INTENSITY; 
		++value;
		OrientationADC::lowLightLevels = true;
		
	} 
	else if (value > LOW_LIGHT_INTENSITY ) {
		
		OrientationADC::lowLightLevels = false;
	}
	
	switch(cardinalDirection) {

			case NORTH :
				OrientationADC::nLightMean[OrientationADC::measurementIndex] = value;
				break;
			case SOUTH :
				OrientationADC::sLightMean[OrientationADC::measurementIndex] = value;
				break;
			case EAST :
				OrientationADC::eLightMean[OrientationADC::measurementIndex] = value;
				break;
			case WEST :
				OrientationADC::wLightMean[OrientationADC::measurementIndex] = value;
				break;
			default:
				break;
	}

	OrientationADC::measurementIndex++;

	if(OrientationADC::measurementIndex == NR_OF_MEASUREMENTS &&
		 OrientationADC::sufficientMeasurements == false){
			OrientationADC::sufficientMeasurements = true;
	}

	if(OrientationADC::measurementIndex == NR_OF_MEASUREMENTS){
			OrientationADC::measurementIndex = 0;
	}
}

void    OrientationADC::calculateMean(int cardinalDirection) {

	float measurementMean;

	if(OrientationADC::sufficientMeasurements == true) {
	
    measurementMean = 0;

    for(int i; i < NR_OF_MEASUREMENTS; i++) {

        switch(cardinalDirection) {

            case NORTH : 
							measurementMean += (OrientationADC::nLightMean[i] / NR_OF_MEASUREMENTS);
							break;
            case SOUTH : 
							measurementMean += (OrientationADC::sLightMean[i] / NR_OF_MEASUREMENTS);
							break;
            case EAST  : 
							measurementMean += (OrientationADC::eLightMean[i] / NR_OF_MEASUREMENTS);
							break;
            case WEST  : 
							measurementMean += (OrientationADC::wLightMean[i] / NR_OF_MEASUREMENTS);
							break;
						default:
							break;
        }
    }
		
		OrientationADC::setMeasurementMean(cardinalDirection, measurementMean);
	}
}

void		OrientationADC::setMeasurementMean(int cardinalDirection, float measurementMean) {
	
		switch(cardinalDirection) {

				case NORTH : 
					OrientationADC::nLightValue = measurementMean;
					break;
				case SOUTH : 
					OrientationADC::sLightValue = measurementMean;
					break;
				case EAST  : 
					OrientationADC::eLightValue = measurementMean;
					break;
				case WEST  : 
					OrientationADC::wLightValue = measurementMean;
					break;
				default:
					break;
		}
}

bool		OrientationADC::getNightTime() {
	return OrientationADC::nightTime;
}

void		OrientationADC::setNightTime(bool nightTime) {

	OrientationADC::nightTime = nightTime;
}

void		OrientationADC::detectLightIntensity(int measurement) {
	
	if(measurement < LOW_LIGHT_INTENSITY){
		OrientationADC::lowLightLevels = true;
	}else{
		
		OrientationADC::lowLightLevels = false;
	}
}

bool OrientationADC::isLightIntensityLow() {
	return lowLightLevels;
}
	
bool OrientationADC::getLightIntensity() {
	return lowLightLevels;
}
