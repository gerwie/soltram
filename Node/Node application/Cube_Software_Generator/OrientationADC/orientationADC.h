/**
* @file 	OrienationADC.h
*	@brief 	This class performs calculations on the data from the ADC and stores it.
* @author Lucas Groenen
* @date		18-05-2016
*/

#ifndef ORIENTATIONADC_H
#define ORIENTATIONADC_H

#include "stm32l1xx_hal.h"

#define NORTH 0
#define EAST  1
#define SOUTH 2
#define WEST  3

#define SENSOR_ANGLE_TRESHOLD 0.1
#define COMPASS_DIRECTIONS	 4
#define NR_OF_MEASUREMENTS   5
#define LOW_LIGHT_INTENSITY  200		//Value between 0-4095 which the ADC is tested against to see if the measurement is sufficient. 3/4096 =  0.7 mV per bit
#define NIGHT_COUNTDOWN			 3600	//amount of times the photoThread will consecutivly return lowLight before night mode is enabled


extern ADC_HandleTypeDef hadc;

class OrientationADC{
	
public:
	
		enum { noDeviation, leftOfSensor, rightOfSensor, aboveSensor, belowSensor };
    OrientationADC();

		uint16_t nLightValue;
		uint16_t sLightValue;
		uint16_t eLightValue;
		uint16_t wLightValue;
		float horDeviation;
   float verDeviation;

		/** 
		* @brief	Runs the ADC measurements
		* @author J. Roorda
		*/
		void runMeasurements();
		
		unsigned int getAzimutDeviation();
		unsigned int getAltitudeDeviation();
		
		/** 
		* @brief	returns the horizontal deviation of the photodiode
		* @author Lucas Groenen
		* @param [out] horDeviation
		*/
    float getHorDeviation();
			
		/** 
		* @brief	returns the vertical deviation of the photodiode
		* @author Lucas Groenen
		* @param [out] verDeviation
		*/
    float getVerDeviation();
		
		/** 
		* @brief	returns the measured value for the cardinal direction that is requested
		* @author Lucas Groenen
		* @param [in] cardinalDirection, tells the functions which cardinal direction should be returned (NORTH, SOUTH, EAST, WEST)
		*/
		int getCardinalValue(int cardinalDirection);
			
			/** 
		* @brief	stores the light intensity for the cardinal directions
		* @author Lucas Groenen
		* @param [in] valueBuffer contains the ADC value
		* @param [in] cardinalDirection, tells the functions which cardinal direction the ADC should measure (NORTH, SOUTH, EAST, WEST)
		*/
		void setLightValue(int valueBuffer, int cardinalDirection);	
		
		/** 
	  * @brief	calls horPosition and verPosition functions to calculate the devations in both of these axis.
 	  * @author Lucas Groenen
	  */
		void calculateDeviations();
		
		/** 
		* @brief	switches the channel the ADC is measuring on
		* @author Lucas Groenen
		* @param 	[in] cardinalDirection, tells the functions which cardinal direction the ADC is measuring
		*/
		void  switchChannel(int cardinalDirection);
		
		/** 
		* @brief	calculates the mean of the measured values for each cardinal direction
		* @author Lucas Groenen
		* @param [in] cardinalDirection
		*/
    void calculateMean(int cardinalDirection);
		
		/** 
	 * @brief	writes the measured value into the array linked to the cardinal direction that was measured
	 * @author Lucas Groenen
	 * @param [in] cardinalDirection
	 */
	 void	setMeasurementMean(int cardinalDirection, float measurementMean);
		
		void runningMean(int cardinalDirection, int value);
		bool getNightTime();
		void setNightTime(bool nightTime);
		bool getLightIntensity();
		bool isLightIntensityLow();

private:
	
	 /** 
	 * @brief	calculates the horizontal deviation and stores it in horDeviation
	 * @author Lucas Groenen
	 */
   void  horPosition();

	 /** 
	 * @brief	calculates the horizontal deviation and stores it in horDeviation
	 * @author Lucas Groenen
	 */
   void  verPosition();

   
	 
	/** 
	 * @brief	checks if the ADC value is high enough for the soltrack to react to the measurement
	 * @author Lucas Groenen
	 * @param [in] cardinalDirection
	 */
	 void detectLightIntensity(int measurement);
	
	 ADC_ChannelConfTypeDef  adcConfig;
	
   

   int nLightMean[NR_OF_MEASUREMENTS];
   int sLightMean[NR_OF_MEASUREMENTS];
   int eLightMean[NR_OF_MEASUREMENTS];
   int wLightMean[NR_OF_MEASUREMENTS];

   int  measurementIndex;

   
	 
	 bool lowLightLevels;
	 unsigned int lowLightLevelCounter;
	 bool sufficientMeasurements;
	 bool nightTime;
};

#endif //ORIENTATIONADC_H
