/**
* @file led_interface.h
* @brief Multi-led indicator controller
*        with variable blinking frequency 
*	@author Brian van Zwam + using existing code made by Tim van der Staaij
*/

#ifndef LED_INTERFACE_H
#define LED_INTERFACE_H

#include <stdint.h>
#include "stm32l1xx_hal.h"
/**
* @name LED_PERIPH
* @brief The GPIO and TIM peripherals
*        assigned to led functionality.
*/
/**@{*/
#define LED_COLOR_PORT GPIOB
#define LED_COMMON_PORT GPIOC
#define LED_COMMON_TIMER TIM14
/**@}*/

/**
* @name LED_PIN
* @brief The specific GPIO pins
*        assigned to led functionality.
*				the commom pin is an output and led pins are input	
*/
/**@{*/
#define LED_COMMON_PIN      GPIO_PIN_13
#define LED_RED_PIN		    	GPIO_PIN_3
#define LED_BLUE_PIN        GPIO_PIN_4
#define LED_GREEN_PIN       GPIO_PIN_5
/**@}*/

#ifdef __cplusplus
extern "C" {
#endif
	
/** @brief Available indicators */
typedef enum {
    LED_DISABLED = 0,        		/**< All leds off */
    LED_RUNNING_FINE,       /**< Running stable LED_GREEN every 1sec*/
		LED_HARDFAULT, 	/**< HARDFAULT LED_RED*/
		LED_BLUE									
} 	led_indicator_t;	

/** 
 * @brief Set the active indicator led;
 *        others will be disabled
 * @param [in] indicator Led to light up
 * @pre MX_GPIO_Led_Init() has been called
 */
void Led_SetIndicator(led_indicator_t indicator);

///**
// * @brief Disco effect for testing and amusement purposes
// * @pre GPIO_Driver_Led_Init()
// *
// * Run this in an (in)finite loop to get a color frenzy.
// * Locks the thread for about 45ms.
// */
//void Led_DoDisco(void);


#ifdef __cplusplus
}
#endif

#endif
