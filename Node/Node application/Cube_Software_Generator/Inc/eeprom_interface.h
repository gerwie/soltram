/**
* @file 	eeprom_interface.h
*	@brief 	This class handles reading and writing the EEPROM
* @author Lucas Groenen
* @date		10-06-2016
*/

#ifndef EEPROM_INTERFACE_H
#define EEPROM_INTERFACE_H

#include "stm32l1xx_hal.h"
#include "stm32l1xx_hal_flash_ex.h" 

#define EEPROM_BASE_ADDRESS  0x008080000
#define EEPROM_SECOND_ADRESS 0x100807FFF
#define EEPROM_THIRD_ADRESS  0x200807FFE


class EEPROMInterface{
	
public:
		
		void  writeNodeID(uint32_t nodeID);
		void 	writeTime(uint32_t time);
		void  writeOrientationUpInt(uint32_t orientationUpInt);
		void  writeTempUpInt(uint32_t tempUpInt);
		void  curYieldUpInt(uint32_t yieldUpInt);
		void	writeLongtitude(uint32_t longtitude);
		void	writeLatitude(uint32_t latitude);
		
		uint32_t getNodeID();
		uint32_t getTime();
		uint32_t getOrientationUpInt();
		uint32_t getTempUpInt();
		uint32_t getCurYieldUpInt();
		
		float getLongtitude();
		float getLatitude();
		void 		 writeEEPROM(uint32_t data);

private:

		uint32_t readEEPROM();
		uint32_t convertFloatToInt(float data);
};


#endif //EEPROM_INTERFACE_H