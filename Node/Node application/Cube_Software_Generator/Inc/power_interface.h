/**
* @file power_interface.h
* @brief interface to the LDO enable pin and get an indication from the LTC3106 
*	@author Brian van Zwam
*/

#ifndef POWER_INTERFACE_H
#define POWER_INTERFACE_H


#define LDO_PORT	GPIOA
#define LDO_PIN		GPIO_PIN_0

#define LTC_PORT	GPIOA
#define LTC_PIN		GPIO_PIN_1


/**
* @brief Get the LDO_EN_PIN, The LDO is enable when the pin is high and disabled when the pin is low.
*/
int LDO_getPin();

/**
* @brief Set the LDO_EN_PIN, The LDO is enable when the pin is high and disabled when the pin is low.
* @param [in] bool if the pin must be enabled
*/
void LDO_setPin(bool enable);


/**
* @brief Get the PGOOD indicator of the LTC3106. PGOOD is an open-drain ouput that is pulled to ground if V_out falls 8% below its programmed voltage (3.3V)
*/
int LTC_getStatus();

#endif
