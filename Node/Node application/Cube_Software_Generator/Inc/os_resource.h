/**
 * @file os_resource.h
 * @brief Application-global definitions
 * @author Intelligent Solutions
 */

#ifndef __OS_RESOURCE_H
#define __OS_RESOURCE_H

#include "cmsis_os.h" // ARM::CMSIS:RTOS:Keil RTX
#include "nodecom.h"
#include "threads.h"
#include "stm32l1xx_hal.h"
#include "XBeeDM.h"
#include "TMP421.h"
#include "OrientationADC.h"
#include "led_interface.h"
#include "power_interface.h"
#include "EngineControl.h"

#define EEPROM_BASE_ADDRESS 0x08080000

/* Global class instantions */
NodeCom xbeeCommunication;
OrientationADC photoADC;
EngineControl enginecontrol;

/* Message queue / mailbox IDs */
osMessageQId msg_XbeeTx;
osMessageQId msg_XbeeRx;
osMessageQId msg_CommandHandler;

/* Memory Pool ID's */
osPoolId xbeeTxMemPoolId;
osPoolId xbeeRxMemPoolId;
osPoolId commandHandlerMemPoolId;
                
/* Thread definitions */
osThreadDef(thrYield, osPriorityNormal, 1, 0);
osThreadDef(thrOrientation, osPriorityHigh, 1, 1200);
osThreadDef(thrCommandHandler, osPriorityNormal, 1, 0);
osThreadDef(thrXbeeTransmit, osPriorityHigh, 1, 0);
osThreadDef(thrProcessRxMessage, osPriorityHigh, 1, 0);
osThreadDef(thrResetWatchDogTimer, osPriorityNormal, 1, 0);
osThreadDef(thrPhotoMeasurement, osPriorityNormal, 1, 0);

osThreadDef(thrStatusLED, osPriorityNormal, 1, 0);
osThreadDef(thrPowerStatus, osPriorityNormal, 1, 0);
osThreadDef(thrRequestTemperature, osPriorityNormal, 1, 0);


/* Message Queue definitions */
osMessageQDef(msg_XbeeTx, TX_MAILBOX_COUNT, NodeCom::MEM_BLOCK*);
osMessageQDef(msg_XbeeRx, RX_MAILBOX_COUNT, NodeCom::RX_MESSAGE*);
osMessageQDef(msg_CommandHandler, RX_MAILBOX_COUNT, NodeCom::RX_COMMAND*);

/* Memory pool definitions */
osPoolDef(mplXbeeTxMemPool, 8, NodeCom::MEM_BLOCK);
osPoolDef(mplXbeeRxMemPool, RX_MAILBOX_COUNT, NodeCom::RX_MESSAGE);
osPoolDef(mplCommandHanlerMemPool, RX_MAILBOX_COUNT, NodeCom::RX_COMMAND);

#endif
