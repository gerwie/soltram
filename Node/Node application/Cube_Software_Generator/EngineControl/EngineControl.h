#ifndef ENGINE_CONTROL_H_
#define ENGINE_CONTROL_H_
/**
* @file EngineControl.h
*	@brief This file is used to set engine angles with the Soltrack algortime.
* @author Cas Peters
*/

#include "OrientationADC.h"
#include "cmsis_os.h"
#include "SolTrack.h"

#define DEGREES_END_SWITCH_HOR_LEFT 45
#define DEGREES_END_SWITCH_HOR_RIGHT 360
#define DEGREES_END_SWITCH_VER_DOWN 0
#define DEGREES_END_SWITCH_VER_UP 90
#define SENSOR_DEVIATION_VALUE 0.3
#define AMOUNT_PULSE_AFTER_SENSOR_ERROR 10
#define DELAY_PULSE_UP 1
#define DELAY_PULSE_DOWN 1

extern OrientationADC photoADC;

/**
 *	@brief enum rotate
 *	
 *	left horizontal engine moves left.
 *	right horizontal engine moves right.
 *	up vertical engine moves up.
 *	down vertical engine moves down.
 */
enum Rotate { left, right ,up ,down};

/**
 *	@brief enum StatusPin
 *	
 *	on pin is set.
 *	off pin is reset.
 *	error pin called doesn't exist.
 */
enum StatusPin { on, off, error };

/**
 *	@brief enum Switch
 *	
 *	switchleft location switch left.
 *	switchright location switch right.
 *	switchup location switch up.
 *	switchdown location switch down.
 */
enum Switch { switchleft, switchright , switchup , switchdown };

/**
 *	@brief enum Engine
 *	
 *	hor selection engine horizontal.
 *	ver selection engine vertical.
 */
enum Engine { hor, ver };

/**
 *	@brief enum LocationSun
 *	
 *	infrontofsensor the sensor points to the sun.
 *	leftofsenor the sensor points to the right of the sun.
 *	rightofsenor the sensor points to the left of the sun.
 *	aboveofsensor the sensor points below the sun.
 *	belowofsensor the sensor points above the sun.
 */
enum LocationSun { infrontofsensor, leftofsenor, rightofsenor, aboveofsensor, belowofsensor };

/**
 *	@brief enum InputUnit
 *	
 *	degrees set the engine with degrees as input value.
 *	pulses set the engine with pulse as input value.
 */
enum InputUnit { degrees, pulses};

class EngineControl{
	private:
		Time time;
		Location location;
		Position position;
		int computeEquatorial;
		double EngineHorLocationCurrentValue;
		double EngineVerLocationCurrentValue;
		double EngineHorLocationSetPoint;
		double EngineVerLocationSetPoint;
		double EngineDegreesSwitchsHorLeft;
		double EngineDegreesSwitchsHorRight;
		double EngineDegreesSwitchsVerUp;
		double EngineDegreesSwitchsVerDown;
		double EngineEndSwitchReachHor;
		double EngineEndSwitchReachVer;
		double EngineDegreesPerPulseHor;
		double EngineDegreesPerPulseVer;
		double DegreesEngineSunRise;
		double DegreesEngineSunSet;
	public:
	/**
 * @brief EngineControl
 * @retval Constructor has no return value (void)
 */
	EngineControl();
	
	/**
 * @brief EngineControl
 * @retval No return value (void)
 *  
 * 	1.	Calibration engines.
 * 	2.	Calculate Degree Per Pulse.
 * 	3.	Set location node.
 * 	4.	Set time for calc sun rise/set.
 * 	5.	Calc sun rise/set.
 * 	6.	Set EngineSunRise.
 *	(Init).
 */
		void EngineControlInit(void);
	
	/**
 * @brief StartSolTrack
 * @retval No return value (void)
 *  
 *	Start Soltrack algoritme.
 *	Time need to be set.
 *	Location need to be set.
 *	Position will be set.
 *	(settings of Soltrack).
 */
		void StartSolTrack();

/**
 * @brief SetTime
 * @retval No return value (void).
 * @param int iYear (current year).
 * @param int iMonth (current month).
 * @param int iHour (current hour).
 * @param int iMinute (current minute).
 * @param double dSecond (current second).
 *  
 *	Set values time.
 *	(Settings of Soltrack).
 */
		void SetTime(int iYear,int iMonth,int iDay,int iHour,int iMinute,double dSecond);

/**
 * @brief SetLocation
 * @retval No return value (void)
 * @param double dLongitude (set Longitude)
 * @param double dLatitude (set Latitude)
 *  
 *	Set values location.
 *	(Settings of Soltrack).
 */
		void SetLocation(double dLongitude,double dLatitude);

/**
 * @brief SetComputeEquatorial
 * @retval No return value (void)
 * @param bool bEquatorial (true set, false reset)
 *  
 *	Set value ComputeEquatorial.
 *	(Settings of Soltrack).
 */
		void SetComputeEquatorial(bool bEquatorial);

/**
 * @brief CalcSunRiseSet
 * @retval No return value (void)
 * @param int MinTimeElapsed (Interval between time between calculations)
 *  
 *	Calc sunRise / sunSet.
 *	(Settings of Soltrack).
 */
		void CalcSunRiseSet(int MinTimeElapsed);

/**
 * @brief GetDegreesAltitude
 * @retval double altitude
 *  
 *	returns the altitude calculated by soltrack.
 *	(Settings of Soltrack).
 */
		double GetDegreesAltitude(void);

/**
 * @brief GetDegreesAzimuth
 * @retval double azimuth
 *  
 *	returns the azimuth calculated by soltrack.
 *	(Settings of Soltrack).
 */
		double GetDegreesAzimuth(void);
		
/**
 * @brief GetDegreesEngineSunRise
 * @retval double degrees
 *  
 *	returns the degrees of sun rise calculated by soltrack.
 *	(Settings of Soltrack).
 */
		double GetDegreesEngineSunRise(void);
		
		/**
 * @brief GetDegreesEngineSunSet
 * @retval double degrees
 *  
 *	returns the degrees of sun set calculated by soltrack.
 *	(Settings of Soltrack).
 */
		double GetDegreesEngineSunSet(void);
		
/**
 * @brief GetEngineHorLocationCurrentValue
 * @retval double altitude
 *  
 *	returns the horizontale location of the engine in pulses.
 *	(Settings of Soltrack).
 */
		double GetEngineHorLocationCurrentValue(void);
	
/**
 * @brief GetEngineHorLocationCurrentDegrees
 * @retval double altitude
 *  
 *	returns the horizontale location of the engine in degrees.
 *	(Settings of Soltrack).
 */
		double GetEngineHorLocationCurrentDegrees(void);
		
/**
 * @brief GetEngineVerLocationCurrentValue
 * @retval double altitude
 *  
 *	returns the verticale location of the engine in pulses.
 *	(Settings of Soltrack).
 */
		double GetEngineVerLocationCurrentValue(void);

/**
 * @brief GetEngineVerLocationCurrentDegrees
 * @retval double altitude
 *  
 *	returns the verticale location of the engine in Degrees.
 *	(Settings of Soltrack).
 */
		double GetEngineVerLocationCurrentDegrees(void);
		
/**
 * @brief GetEngineHorLocationSetPointValue
 * @retval double altitude
 *  
 *	returns the horizontale location of the set point of the engine in pulses.
 *	(Settings of Soltrack).
 */
		double GetEngineHorLocationSetPointValue(void);
		
/**
 * @brief GetEngineHorLocationSetPointDegrees
 * @retval double altitude
 *  
 *	returns the horizontale location of the set point of the engine in Degrees.
 *	(Settings of Soltrack).
 */
		double GetEngineHorLocationSetPointDegrees(void);
		
/**
 * @brief GetEngineVerLocationCurrentValue
 * @retval double altitude
 *  
 *	returns the verticale location of the set point of the engine in pulses.
 *	(Settings of Soltrack).
 */
		double GetEngineVerLocationSetPointValue(void);
		
/**
 * @brief GetEngineVerLocationCurrentDegrees
 * @retval double altitude
 *  
 *	returns the verticale location of the set point of the engine in Degrees.
 *	(Settings of Soltrack).
 */
		double GetEngineVerLocationSetPointDegrees(void);

/**
 * @brief GetEngineDegreesPerPulseHor
 * @retval double altitude
 *  
 *	returns the horizontale degrees per pulse.
 *	(Settings of Soltrack).
 */
		double GetEngineDegreesPerPulseHor(void);
		
/**
 * @brief GetEngineDegreesPerPulseHor
 * @retval double altitude
 *  
 *	returns the verticale degrees per pulse.
 *	(Settings of Soltrack).
 */		
		double GetEngineDegreesPerPulseVer(void);
/**
 * @brief CalibrationEnginesSwitch
 * @retval No return value (void)
 *  
 *	1.	Rotate engines hor right till end switch.
 *	2.	Rotate engines hor left till end switch.
 *	3.	Save amount pulses hor.
 *	4.	Rotate engines ver up till end switch.
 *	5.	Rotate engines ver down till end switch.
 *	6.	Save amount pulses ver.
 *	(Settings of the Engines).
 */
		void CalibrationEnginesSwitch(void);

/**
 * @brief CalibrationEngineSetPoint.
 * @retval No return value (void).
 * @param Engine engine (enum; select engine "hor"(horizontal) or "ver"(vertical)).
 * @param double dDegrees_switchleft (Degrees of the left end-switch seen from north 0�).
 * @param double dDegrees_switchright (Degrees of the right end-switch seen from north 0�).
 *  
 *	Set private values EngineDegreesSwitchsHorLeft , EngineDegreesSwitchsHorRight or 
 *	Set private values EngineDegreesSwitchsVerUp, EngineDegreesSwitchsVerDown ;
 *	(Settings of the Engines).
 */
		void SetEngineSetPoint(Engine engine, double dDegrees_switchleft,double dDegrees_switchright);

/**
 * @brief CalculateDegreePerPulse
 * @retval No return value (void)
 *  
 *	Set private values DeltaDegreesHor , DeltaDegreesVer , EngineDegreesPerPulseHor , EngineDegreesPerPulseVer.
 *	(Settings of the Engines).
 */
		void CalculateDegreePerPulse(void);

/**
 * @brief GetLocationSun
 * @retval LocationSun (enum; infrontofsensor, leftofsenor, rightofsenor, aboveofsensor, belowofsensor)
 * @param Engine engine (enum; select engine "hor"(horizontal) or "ver"(vertical))
 *	 
 *	Get Location Sun seen relative to the light sensor.
 *	(Settings of the Engines).
 */
		LocationSun GetLocationSun(Engine engine);

/**
 * @brief SetEngineLocationSetPoint
 * @retval StatusPin (enum; on(fine),off(hit endswitch),error(already on endswitch))
 * @param Engine engine (enum; select engine "hor"(horizontal) or "ver"(vertical))
 * @param InputUnit unit (enum; input unit "degrees" or "pulse")
 * @param double dValue (select engine "hor"(horizontal) or "ver"(vertical))
 *	 
 *	Get engines to location.
 *	(Settings of the Engines)..
 */
		StatusPin SetEngineLocationSetPoint(Engine engine, InputUnit unit, double dValue);

/**
 * @brief RotateTillEndSwitch
 * @retval int (amount of pulse needed for hit to a end switch)
 * @param Engine engine (enum; select engine "hor"(horizontal) or "ver"(vertical))
 * @param Rotate rotate (enum; rotation direction  "left" or "right" or "up" or "down")
 *	 
 *	Rotate engine till it hits a end switch.
 *	(Hardware Control).
 */
		int RotateTillEndSwitch(Engine engine, Rotate rotate);

/**
 * @brief SetEngine
 * @retval StatusPin (enum; on(fine),off(hit endswitch),error(already on endswitch))
 * @param Engine engine (enum; select engine "hor"(horizontal) or "ver"(vertical))
 * @param Rotate rotate (enum; rotation direction  "left" or "right" or "up" or "down")
 * @param int iPulseAmount (amount of pulses)
 *	 
 *	Rotate engine amount of pulses.
 *	(Hardware Control).
 */
		StatusPin SetEngine(Engine engine, Rotate rotate, int iPulseAmount);

/**
 * @brief EngineSwitchStatus
 * @retval StatusPin (enum; on, off, error)
 * @param Engine engine (enum; select engine "hor"(horizontal) or "ver"(vertical))
 * @param Switch switchnumber (enum; witch switch  "left" or "right" or "up" or "down")
 *	 
 *	Get switch status, return error if switch does not exist.
 *	(Hardware Control).
 */
		StatusPin EngineSwitchStatus(Engine engine, Switch switchnumber);

/**
 * @brief delay
 * @retval No return value (void)
 * @param int d (wait d * 8 system clk cycles)
 *	 
 *	software delay.
 *	(Software delay).
 */
		void delay(int d);
};

#endif
