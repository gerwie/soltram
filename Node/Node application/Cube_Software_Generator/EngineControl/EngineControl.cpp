/**
* @file EngineControl.cpp
*	@brief This file is used to set engine angles with the Soltrack algortime.
* @author Cas Peters
*/

#include "EngineControl.h"

EngineControl::EngineControl(){

};

void EngineControl::EngineControlInit(){
		SetEngineSetPoint(hor,DEGREES_END_SWITCH_HOR_LEFT,DEGREES_END_SWITCH_HOR_RIGHT);
		SetEngineSetPoint(ver,DEGREES_END_SWITCH_VER_DOWN,DEGREES_END_SWITCH_VER_UP);
		CalibrationEnginesSwitch();
		CalculateDegreePerPulse();
};

void EngineControl::StartSolTrack(){
		SolTrack(time,location,&position,computeEquatorial);
		return;
};

void EngineControl::SetTime(int iYear,int iMonth,int iDay,int iHour,int iMinute,double dSecond){
		time.year = iYear;
		time.month = iMonth;
		time.day = iDay;
		time.hour = iHour;
		time.minute = iMinute;
		time.second = dSecond;
		return;
};

void EngineControl::SetLocation(double dLongitude,double dLatitude){
		location.longitude = dLongitude;
		location.latitude = dLatitude;
		return;
};

void EngineControl::SetComputeEquatorial(bool equatorial){
		computeEquatorial = (equatorial == true) ? 1 : 0 ;
		return;
};

void EngineControl::CalcSunRiseSet(int MinTimeElapsed){
		int Min = 0;
		int Hour = 0;
		bool daytime = false;
		SetTime(time.year,time.month,time.day,Hour,Min,0);
		StartSolTrack();
		while(daytime == false || GetDegreesAltitude() > 0){
			Min = Min + MinTimeElapsed;
			while(Min >= 60){
				Min = Min - 60;
				Hour++;
			}
			SetTime(time.year,time.month,time.day,Hour,Min,0);
			StartSolTrack();
			if(daytime == false && GetDegreesAltitude() > 0){
				daytime = true;
				DegreesEngineSunRise = GetDegreesAzimuth();
			}
		}
		DegreesEngineSunSet = GetDegreesAzimuth();
		return;
};

double EngineControl::GetDegreesAltitude(){
		return position.altitudeRefract;
};

double EngineControl::GetDegreesAzimuth(){
		return position.azimuthRefract;
};

double EngineControl::GetDegreesEngineSunSet(void){
	return DegreesEngineSunSet;
};

double EngineControl::GetDegreesEngineSunRise(void){
		return DegreesEngineSunRise;
}

double EngineControl::GetEngineHorLocationCurrentValue(void){
		return EngineHorLocationCurrentValue;
};

double EngineControl::GetEngineHorLocationCurrentDegrees(void){
		return ((EngineHorLocationCurrentValue * EngineDegreesPerPulseHor) + EngineDegreesSwitchsHorLeft);
};

double EngineControl::GetEngineVerLocationCurrentValue(void){
		return EngineVerLocationCurrentValue;
};

double EngineControl::GetEngineVerLocationCurrentDegrees(void){
	return ((EngineVerLocationCurrentValue * EngineDegreesPerPulseVer) + EngineDegreesSwitchsVerDown);
};

double EngineControl::GetEngineHorLocationSetPointValue(void){
		return EngineHorLocationSetPoint;
};

double EngineControl::GetEngineHorLocationSetPointDegrees(void){
		return ((EngineHorLocationSetPoint * EngineDegreesPerPulseVer) + EngineDegreesSwitchsHorLeft);
};
		
double EngineControl::GetEngineVerLocationSetPointValue(void){
		return EngineVerLocationSetPoint;
};

double EngineControl::GetEngineVerLocationSetPointDegrees(void){
		return ((EngineVerLocationSetPoint * EngineDegreesPerPulseVer) + EngineDegreesSwitchsVerDown);
};

double EngineControl::GetEngineDegreesPerPulseHor(void){
		return EngineDegreesPerPulseHor;
};

double EngineControl::GetEngineDegreesPerPulseVer(void){
		return EngineDegreesPerPulseVer;
};
		
void EngineControl::CalibrationEnginesSwitch(){
		//Cali eng 1 end switch
		RotateTillEndSwitch(hor,right);
		EngineEndSwitchReachHor = RotateTillEndSwitch(hor,left);
		EngineHorLocationCurrentValue = 0;
		//Cali eng 2 end switch
		RotateTillEndSwitch(ver,up);
		EngineEndSwitchReachVer = RotateTillEndSwitch(ver,down);
		EngineVerLocationCurrentValue = 0;
		return;
};

void EngineControl::SetEngineSetPoint(Engine engine, double dDegrees_switch_left_or_down,double dDegrees_switch_right_or_up){
		if(engine == hor){
			EngineDegreesSwitchsHorLeft = dDegrees_switch_left_or_down;
			EngineDegreesSwitchsHorRight = dDegrees_switch_right_or_up;
		}else{
			EngineDegreesSwitchsVerDown = dDegrees_switch_left_or_down;
			EngineDegreesSwitchsVerUp = dDegrees_switch_right_or_up;
		}
		return;
};

void EngineControl::CalculateDegreePerPulse(){
		double DeltaDegreesHor;
		double DeltaDegreesVer;
		DeltaDegreesHor = EngineDegreesSwitchsHorLeft > EngineDegreesSwitchsHorRight ?
		(EngineDegreesSwitchsHorLeft - EngineDegreesSwitchsHorRight) :
		(EngineDegreesSwitchsHorRight - EngineDegreesSwitchsHorLeft) ;
		DeltaDegreesVer = EngineDegreesSwitchsVerUp > EngineDegreesSwitchsVerDown ?
		(EngineDegreesSwitchsVerUp - EngineDegreesSwitchsVerDown) :
		(EngineDegreesSwitchsVerDown - EngineDegreesSwitchsVerUp) ;
	
		if(EngineEndSwitchReachHor != 0 && EngineEndSwitchReachVer != 0){
			EngineDegreesPerPulseHor = DeltaDegreesHor / EngineEndSwitchReachHor;
			//EngineDegreesPerPulseHor = EngineEndSwitchReachHor / DeltaDegreesHor;
			EngineDegreesPerPulseVer = DeltaDegreesVer / EngineEndSwitchReachVer;
			//EngineDegreesPerPulseVer =  EngineEndSwitchReachVer / DeltaDegreesVer;
		}else{
			EngineEndSwitchReachHor = AMOUNT_PULSE_AFTER_SENSOR_ERROR;
			EngineEndSwitchReachVer = AMOUNT_PULSE_AFTER_SENSOR_ERROR;
		}
}

LocationSun EngineControl::GetLocationSun(Engine engine){
		float Value; 
		LocationSun locationsun;
		if(engine == hor){
			Value = photoADC.getHorDeviation();
			if(Value < SENSOR_DEVIATION_VALUE && Value > -SENSOR_DEVIATION_VALUE){
				locationsun = infrontofsensor;
			}else if(Value <= -SENSOR_DEVIATION_VALUE){
				locationsun = rightofsenor;
			}else if(Value >= SENSOR_DEVIATION_VALUE){
				locationsun = leftofsenor;
			}			
		}else{
			Value = photoADC.getVerDeviation();
			if(Value < SENSOR_DEVIATION_VALUE && Value > -SENSOR_DEVIATION_VALUE){
				locationsun = infrontofsensor;
			}else if(Value <= -SENSOR_DEVIATION_VALUE){
				locationsun = aboveofsensor;
			}else if(Value >= SENSOR_DEVIATION_VALUE){
				locationsun = belowofsensor;
			}
		}
		return locationsun;
}

StatusPin EngineControl::SetEngineLocationSetPoint(Engine engine, InputUnit unit, double dValue){
		if(engine == hor){
			if(unit == degrees){
				if(dValue < EngineDegreesSwitchsHorLeft){
					dValue = 0; // No need to calc degrees are lower than possible
				}else{
					dValue = dValue > EngineDegreesSwitchsHorRight ? EngineDegreesSwitchsHorRight : dValue; // reach not past max value
					dValue = dValue - EngineDegreesSwitchsHorLeft; // reach - minimal reach
					EngineHorLocationSetPoint = EngineDegreesPerPulseHor != 0 ? (dValue / EngineDegreesPerPulseHor) : 0; //relative distance
				}
			}

			if(EngineHorLocationSetPoint != EngineHorLocationCurrentValue){
				double compareVal = EngineHorLocationSetPoint - EngineHorLocationCurrentValue;
				return (SetEngine(hor, compareVal < 0 ? left:right, abs(compareVal)));
			}
		}else{
			if(unit == degrees){
				if(dValue < EngineDegreesSwitchsVerDown){
					dValue = 0;// No need to calc degrees are lower than possible
				}else{
					dValue = dValue > EngineDegreesSwitchsVerUp ? EngineDegreesSwitchsVerUp : dValue; // reach not past max value
					dValue = dValue - EngineDegreesSwitchsVerDown; // reach - minimal reach
					EngineVerLocationSetPoint = EngineDegreesPerPulseVer != 0 ? (dValue / EngineDegreesPerPulseVer) : 0; //relative distance
				}	
			}			
			if(EngineVerLocationSetPoint != EngineVerLocationCurrentValue){
				double compareVal = EngineVerLocationCurrentValue - EngineVerLocationSetPoint;
				return (SetEngine(ver, compareVal < 0 ? up:down, abs(compareVal)));
			}
		}
		return on;
};

int EngineControl::RotateTillEndSwitch(Engine engine, Rotate rotate){
		int AmountOfPulses = 0;
		if(engine == hor){
			if(rotate == left){
				if(on == EngineSwitchStatus(hor,switchleft)){
					return 0;
				}else{
					while(off == EngineSwitchStatus(hor,switchleft)){
						SetEngine(hor,left,1);
						AmountOfPulses++;}
					return AmountOfPulses;
				}
			}
			
		if(rotate == right){
			if(on == EngineSwitchStatus(hor,switchright)){
				return 0;
			}else{
				while(off == EngineSwitchStatus(hor,switchright)){
					SetEngine(hor,right,1);
					AmountOfPulses++;}
				return AmountOfPulses;
			}
		}
	}else{
		if(rotate == up){
			if(on == EngineSwitchStatus(ver,switchup)){
				return 0;
			}else{
				while(off == EngineSwitchStatus(ver,switchup)){
					SetEngine(ver,up,1);
					AmountOfPulses++;}
				return AmountOfPulses;
			}
		}
		
		if(rotate == down){
			if(on == EngineSwitchStatus(ver,switchdown)){
				return 0;
			}else{
				while(off == EngineSwitchStatus(ver,switchdown)){
					SetEngine(ver,down,1);
					AmountOfPulses++;}
				return AmountOfPulses;
			}
		}
	}
	return error;
};

StatusPin EngineControl::SetEngine(Engine engine, Rotate rotate, int iPulseAmount){ 
	if(engine == hor){
		Switch switchstatus = rotate == left ? switchleft : switchright;
		rotate == left ? HAL_GPIO_WritePin(GPIOC,GPIO_PIN_10,GPIO_PIN_RESET) : HAL_GPIO_WritePin(GPIOC,GPIO_PIN_10,GPIO_PIN_SET);
		if(EngineSwitchStatus(hor,switchstatus) == on){return error;}
		
		for(int i = 0 ; i < iPulseAmount ; i++){
			if(EngineSwitchStatus(hor,switchstatus) == on){return off;}
			HAL_GPIO_WritePin(GPIOC,GPIO_PIN_11,GPIO_PIN_SET);
			delay(DELAY_PULSE_UP);
			HAL_GPIO_WritePin(GPIOC,GPIO_PIN_11,GPIO_PIN_RESET);
			delay(DELAY_PULSE_DOWN);
			if(rotate == left){
				EngineHorLocationCurrentValue--;
			}else{
				EngineHorLocationCurrentValue++;
			}
		}
		return on;
	}
	
	if(engine == ver){
		Switch switchstatus = rotate == down ? switchdown : switchup;
		rotate == down ? HAL_GPIO_WritePin(GPIOC,GPIO_PIN_12,GPIO_PIN_RESET) : HAL_GPIO_WritePin(GPIOC,GPIO_PIN_12,GPIO_PIN_SET);
		if(EngineSwitchStatus(ver,switchstatus) == on){return error;}
		
		for(int i = 0 ; i < iPulseAmount ; i++){
			if(EngineSwitchStatus(ver,switchstatus) == on){return off;}
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
			delay(DELAY_PULSE_UP);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
			delay(DELAY_PULSE_DOWN);
			if(rotate == down){
				EngineVerLocationCurrentValue--;
			}else{
				EngineVerLocationCurrentValue++;
			}
		}
		return on;
	}
	return error;
}

StatusPin EngineControl::EngineSwitchStatus(Engine engine, Switch switchstatus){
	if(engine == hor){
		if(switchstatus == switchleft){
			if(GPIO_PIN_SET == HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_5)){
				return off;
			}else{
				return on;
			}
		}else{
			if(GPIO_PIN_SET == HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)){
				return off;
			}else{
				return on;
			}
		}
	}else{
		if(switchstatus == switchup){
			if(GPIO_PIN_SET == HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1)){
				return off;
			}else{
				return on;
			}
		}else{
			if(GPIO_PIN_SET == HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2)){
				return off;
			}else{
				return on;
			}
		}
	}
}

void EngineControl::delay(const int d){
		//osDelay(d);
		return;
}
