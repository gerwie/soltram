pin layout stm32L152ret6

(SWD)
SWD_DIO           =      PA13  46       IO
SWD_CLK           =      PA14  49       IO
RESET             =      NRST  7        IO

(LDO ENABLE)
LDO_EN            =      PA0   14       IO      OUTPUT

(POWER GOOD?)
PGOOD             =      PA1   15       IO      INPUT

(XBEE UART)
UART1-TX          =      PA9   42       IO
UART1-RX          =      PA10  43       IO
UART1-CTS         =      PA11  44       IO
UART1-RTS         =      PA12  45       IO

(XBEE SPI)
SPI1_CLK          =      PA4   20       IO
SPI1_MISO         =      PA5   21       IO
SPI1_MOSI         =      PA6   22       IO
SPI1_NNS          =      PA7   23       IO      INPUT
SPI1_ATTN(GPIO)   =      PB7   59       IO      INPUT

(XBEE GPIO)
XBEE_SLEEPREQUEST =      PC7   38       IO      OUTPUT
XBEE_RESET        =      PC8   39       IO      OUTPUT

(LIGHTDIODE)
NORTH             =      PC0   8        ADC_10
EAST              =      PC1   9        ADC_11
SOUTH             =      PC2   10       ADC_12
WEST              =      PC3   11       ADC_13

(TEMPERATURE SENSOR)
I2C1_SCL          =      PB8   61       IO
I2C1_SDA          =      PB9   62       IO

(MOTOR_CONTROLLER)
PWM1              =      PA15  50       TIM2_CH1
PWM2              =      PC6   37       TIM3_CH2
DIRECTION_1       =      PC10  51       IO     OUTPUT
STEP_1            =      PC11  52       IO     OUTPUT
DIRECTION_2       =      PC12  53       IO     OUTPUT
STEP_2            =      PD2   54       IO     OUTPUT

(END SWITCH POSITIONS)
EPS_1             =      PC5   25       IO     INPUT
EPS_2             =      PB0   26       IO     INPUT
EPS_3             =      PB1   27       IO     INPUT
EPS_4             =      PB2   28       IO     INPUT

(STATUS LEDS)
LED_RED			  = 	 PB3	55		IO		OUTPUT	
LED_BLUE		  = 	 PB4	56		IO		OUTPUT
LED_GREEN		  =	     PB5	57		IO		OUTPUT	
LED_COM			  = 	 PC13	2		IO		INPUT


(SOLAR POWER YIELD)
SOLAR_POWER_YIELD =      PB12  33       ADC_18


EXTRA PINOUTS

(UART2)
UART2-TX          =      PA2   16       IO
UART2-RX          =      PA3   17       IO

(SPI2)
SPI2_CLK          =      PB13  34       IO
SPI2_MISO         =      PB14  35       IO
SPI2_MOSI         =      PB15  36       IO

(I2C2)
I2C2_SCL          =      PB10  29       IO
I2C2_SDA          =      PB11  30       IO

(PWM_SIGNAL)
PWM3              =      PB6   58       TIM4_CH1

(CRYSTAL(HSE))
RCC_OSC32_IN      =      PC14  3        IO   INPUT
RCC_OSC32_OUT     =      PC15  4        IO   OUTPUT

(CRYSTAL(LSE))
RCC_IN            =      PH0   5        IO   INPUT
RCC_OUT           =      PH1   6        IO   OUTPUT






