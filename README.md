# SOLTRAM #

The aim of the SOLTRAM project is to effectuate a controller that can orient a solar panel towards the sun using the `Soltrack` algorithm. The client is the readership of durable energy at the University of Applied Science HAN. The algorithm uses the GPS-location and the coordinated universal time (UTC) to determine the angle towards the sun based on the position of the solar panel. The solar panel will also be equipped with a light sensor that will control the values determined by `SolTrack`. Both values will be sent to a central gateway through a mesh network. This will allow the user to use the website to see the results of the sensor versus the algorithm.

The project was realized using C and C++11 for the code and an STM32L-serie low energy chip. The final product consists of three main components. These components are the node, central gateway/database and a website that enables the user to interface with the nodes. Every component can communicate with each other through XBEE and the orientation controls, both the sensor and the algorithm, can reorient the solar panel.


## Folder structure
The folder structure used in this project and what to expect in each folder:

* `./Database`: Contains the databases structure
* `./Docs`    : Contains the documentation of this project. Note the documentation is in Dutch.
* `./Gateway` : Contains the gateway source files and hardware design.  
* `./Node`    : Contains the node source files and hardware design.
* `./Website` : The website of this project.

## Hardware ##
The Gateway and the Node contains a self developed Printed Circuit Board (PCB).

The hardware designs are made with [Altium designer](http://www.altium.com/). To view or edited the designs the recommended choice is Altium designer. <br \>

## Software ##
### Website ###
The [website](http://soltram.info) has support for MQTT WebSockect. The WebSockect can connect to subdomain `mqtt.soltram.info`.  

### Database ###
The data is a MySQL database that is on the same server as the website.

### Server ###
The website and database are running on the same server. This is easier to maintain.

The following tutorials are used:

* [Setting up a ubuntu 16.04 LAMP server](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04)
* [Installing mosquitto + the websockets](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-the-mosquitto-mqtt-messaging-broker-on-ubuntu-16-04)
* [installing phpMyAdmin](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-phpmyadmin-on-ubuntu-16-04)

On the server the php version has been downgraded to PHP-5.6. This because the website is using an older way to communicate with the mysql database.

### Gateway
The Gateway is developed in [Qt Creator](https://www.qt.io/) under a Linux enviroment. The source files contains a .pro file that opens the Gateway project. The Gateway is written in C++.

The Gateway must run with `sudo` premission. This is for opening the UART connection. <br \>
The following parameters must be given:

* host, `mqtt.soltram.info`
* port, `8883`
* mqtt topic,  `SOLTRAM/Field 0x`where the `x` is the field where the gateway is placed.

The command to start the gateway is that: `sudo ./gateway mqtt.soltram.info 8883 SOLTRAM/Field 0x`. The gateway will stop when `ctrl-z` is pressed.

### Node
The node is developed in [Keil uVision](http://www2.keil.com/mdk5/uvision/) under a windows enviroment. The sources files contains a .uvprojx file that opens the Node project.

On the node there is running an real-time operating system (RTOS). The RTOS that is used is [Keil RTX](http://www.keil.com/arm/rl-arm/kernel.asp). The node also contains a library [SolTrack](https://github.com/astronomy/SolTrack).
 This library compute the position of the sun based on the time and GPS coordinates.    


### Dependencies ####
The gateway has the following dependencies:

* [libMosquitto-devel](https://mosquitto.org/download/)
* [mysql-connector-c](https://dev.mysql.com/downloads/connector/c/)

## Credits
This project was started as a school project with 6 members including myself. These project members should also get the credits for this project, but for privacy reasons these members are not mentioned.

Authors:

* [Brian van Zwam](https://github.com/Gerwie)

### Libraries

**SolTrack**
> SolTrack is a free, fast and accurate C/C++ routine to compute the position of the Sun.
> SolTrack has been developed by Marc van der Sluys, Paul van Kan and Jurgen Reintjes, of the Lectorate of Sustainable Energy at the HAN University of Applied Sciences in Arnhem, the Netherlands.
> <https://github.com/astronomy/SolTrack>

**Eclipse Paho JavaScript client**  
> The Paho JavaScript Client is an MQTT browser-based client library written
> in Javascript that uses WebSockets to connect to an MQTT Broker.  
> <https://github.com/eclipse/paho.mqtt.javascript>


**jQuery**

> New Wave JavaScript  
> [jQuery is licensed](http://jquery.org/license) under [GNU GPL](http://www.gnu.org/licenses/gpl.html) and [MIT licences](http://www.opensource.org/licenses/mit-license.php), and with the MIT licence you could use it in commercial applications.

## License
> You can check out the full license [here](./LICENSE)

This project is licensed under the terms of the **MIT** license.
